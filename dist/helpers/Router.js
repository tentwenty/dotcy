"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants = __importStar(require("../constants"));
const query_string_1 = __importDefault(require("query-string"));
class Router {
    constructor(config) {
        this.config = config;
    }
    getRoute(key, params = {}, pathParams, arrayFormat = "comma") {
        for (const index in pathParams) {
            const regex = new RegExp(`{${index}}`, "g");
            key = key.replace(regex, pathParams[index]);
        }
        key += query_string_1.default.stringify(params)
            ? "?" + query_string_1.default.stringify(params, { arrayFormat })
            : "";
        return key == constants.ENDPOINT_AUTHORIZE
            ? `${this.config.baseUrl}/api/${key}`
            : `${this.config.baseUrl}/api/${this.config.apiVersion}/${this.config.channelId}/${key}`;
    }
}
exports.default = Router;
//# sourceMappingURL=Router.js.map