"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = __importDefault(require("winston"));
require("winston-daily-rotate-file");
const myFormat = winston_1.default.format.printf((_a) => {
    var { level, message, timestamp } = _a, metadata = __rest(_a, ["level", "message", "timestamp"]);
    let msg = `${timestamp} [${level}] : ${message} `;
    if (Object.keys(metadata).length)
        msg += `: ${JSON.stringify(metadata)}`;
    return msg;
});
const filePath = `logs/dotcy/`;
const Logger = winston_1.default.createLogger({
    format: winston_1.default.format.combine(
    // winston.format.colorize(),
    winston_1.default.format.splat(), winston_1.default.format.timestamp(), myFormat),
    transports: [
        new winston_1.default.transports.DailyRotateFile({
            filename: `${filePath}error_%DATE%.log`,
            datePattern: "YYYY-MM-DD",
            level: "error",
            maxSize: "1m",
        }),
        new winston_1.default.transports.DailyRotateFile({
            filename: `${filePath}combined_%DATE%.log`,
            datePattern: "YYYY-MM-DD",
            maxSize: "1m",
        }),
    ],
});
// If we're not in production then log to the `console` with the format:
// if (process.env.NODE_ENV !== "production") logger.add(new winston.transports.Console());
exports.default = Logger;
//# sourceMappingURL=Logger.js.map