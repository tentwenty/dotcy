import winston from "winston";
import "winston-daily-rotate-file";
declare const Logger: winston.Logger;
export default Logger;
