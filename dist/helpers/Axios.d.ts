import IConfig from "../interfaces/IConfig";
import { AxiosInstance } from "axios";
export default class Axios {
    axiosInstance: AxiosInstance;
    private config;
    private tokenId;
    private Router;
    constructor(dotcyConfig: IConfig);
    private getAccessToken;
    private setRequestInterceptor;
    private setResponseInterceptor;
}
