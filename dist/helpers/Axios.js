"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const db_1 = __importDefault(require("../helpers/db"));
const http_status_1 = __importDefault(require("http-status"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("./Router"));
const Logger_1 = __importDefault(require("./Logger"));
const http_1 = __importDefault(require("http"));
const https_1 = __importDefault(require("https"));
class Axios {
    constructor(dotcyConfig) {
        this.tokenId = "UWDAtVZHUc";
        this.config = dotcyConfig;
        this.Router = new Router_1.default(dotcyConfig);
        this.axiosInstance = axios_1.default.create({
            timeout: 60000,
            httpAgent: new http_1.default.Agent({ keepAlive: true }),
            httpsAgent: new https_1.default.Agent({ keepAlive: true }),
        });
        this.setRequestInterceptor();
        this.setResponseInterceptor();
    }
    getAccessToken(username, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = new URLSearchParams();
                params.append("grant_type", this.config.grantType);
                params.append("username", username);
                params.append("password", password);
                const response = yield axios_1.default.post(this.Router.getRoute(constants.ENDPOINT_AUTHORIZE), params, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                    },
                });
                const result = response.data;
                yield db_1.default.update({ _id: this.tokenId }, Object.assign({ _id: this.tokenId }, result), { upsert: true });
                return result;
            }
            catch (error) {
                Logger_1.default.error("getAccessToken", error.response.data);
                throw error;
            }
        });
    }
    setRequestInterceptor() {
        this.axiosInstance.interceptors.request.use((axiosConfig) => __awaiter(this, void 0, void 0, function* () {
            //Logger.info(`Hitting API: ${axiosConfig.url}`);
            let tokenObj = yield db_1.default.findOne({ _id: this.tokenId });
            //Logger.info(`Retrieved Token : ${tokenObj.access_token}`);
            if (!tokenObj) {
                tokenObj = yield this.getAccessToken(this.config.apiUsername, this.config.apiPassword);
                //Logger.info(`Fresh Token : ${tokenObj.access_token}`);
            }
            axiosConfig.headers = {
                Authorization: `Bearer ${tokenObj.access_token}`,
                Accept: axiosConfig.headers.Accept
                    ? axiosConfig.headers.Accept
                    : "application/json",
                "Content-Type": axiosConfig.headers["Content-Type"]
                    ? axiosConfig.headers["Content-Type"]
                    : "application/x-www-form-urlencoded",
                APIKey: this.config.apiKey,
            };
            return axiosConfig;
        }), (error) => {
            Logger_1.default.error("Axios Error :: setRequestInterceptor", error);
            Promise.reject(error);
        });
    }
    setResponseInterceptor() {
        this.axiosInstance.interceptors.response.use((response) => {
            return response;
        }, (error) => __awaiter(this, void 0, void 0, function* () {
            var _a, _b;
            Logger_1.default.error(`error interceptor: ${JSON.stringify((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data)}`);
            Logger_1.default.error(error.stack);
            const originalRequest = error.config;
            if (((_b = error.response) === null || _b === void 0 ? void 0 : _b.status) === http_status_1.default.UNAUTHORIZED &&
                !originalRequest["x-retry"]) {
                originalRequest["x-retry"] = true;
                const tokenObj = yield this.getAccessToken(this.config.apiUsername, this.config.apiPassword);
                axios_1.default.defaults.headers.common["Authorization"] = `Bearer ${tokenObj.access_token}`;
                return this.axiosInstance(originalRequest);
            }
            return Promise.reject(error);
        }));
    }
}
exports.default = Axios;
//# sourceMappingURL=Axios.js.map