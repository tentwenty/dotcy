"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
class CustomError extends Error {
    constructor(message, statusCode = http_status_1.default.INTERNAL_SERVER_ERROR) {
        super(message);
        this.statusCode = statusCode;
        this.error = http_status_1.default[statusCode];
        this.message = message;
    }
}
exports.default = CustomError;
//# sourceMappingURL=Error.js.map