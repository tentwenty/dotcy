import IConfig from "../interfaces/IConfig";
import { IArrayFormatter, IPathParams, IRouteParams } from "../interfaces/ITypes";
export default class Router {
    private config;
    constructor(config: IConfig);
    getRoute(key: string, params?: IRouteParams, pathParams?: IPathParams, arrayFormat?: IArrayFormatter): string;
}
