export default class CustomError extends Error {
    message: string;
    statusCode: number;
    error: unknown;
    constructor(message: string, statusCode?: number);
}
