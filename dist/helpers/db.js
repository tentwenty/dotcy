"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nedb_async_await_1 = require("nedb-async-await");
exports.default = nedb_async_await_1.Datastore({ filename: "tokens.db", autoload: true });
//# sourceMappingURL=db.js.map