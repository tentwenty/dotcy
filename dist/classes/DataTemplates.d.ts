import IConfig from "../interfaces/IConfig";
import IDataTemplates from "../interfaces/IDataTemplates";
import { ICaching } from "../interfaces/ITypes";
import { IReqProductFeatures } from "../interfaces/requests/datatemplates";
import { IRespProductFeatures } from "../interfaces/responses/datatemplates";
export default class DataTemplates implements IDataTemplates {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Returns the template values for the specified product
     * @param request - A fully populated request object
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getProductFeatures(request: IReqProductFeatures, caching?: ICaching): Promise<IRespProductFeatures[]>;
}
