import IConfig from "../interfaces/IConfig";
import IScheduler from "../interfaces/IScheduler";
import { ICaching } from "../interfaces/ITypes";
import { IRespVenueZone } from "../interfaces/responses/scheduler";
import { IReqVenueZone } from "../interfaces/requests/scheduler";
export default class Scheduler implements IScheduler {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /** Gets Venue Working dates
     * @param caching Caching policy to apply
     * @param startDate Start date to start the search from
     * @param endDate End date to end the search on
     * @returns The available timestamps in the form of a string[]
     */
    getVenueWorkingDate({ startDate, endDate, venueID, caching, }: {
        startDate: string;
        endDate: string;
        venueID: string;
        caching?: ICaching;
    }): Promise<string[]>;
    /**
     * Loads and returns the tickets purchased by the user
     * @param ticketReq json response
     * @returns Returns the reponse from dotcy platform
     */
    getVenueZone(veueReq: IReqVenueZone): Promise<IRespVenueZone[]>;
}
