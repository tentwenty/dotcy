"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class Profile {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /** Gets Profile Based on the Search Params
     * @param searchParams JSON Array
     * @returns Returns User Object Array
     */
    searchProfiles(searchParams) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PROFILE_SEARCH_PROFILES), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.searchProfiles()", error);
                return [];
            }
        });
    }
    /** Gets Profile Based on the Search Params
     * @param profileParam JSON Array
     * @returns Returns User Object
     */
    updateProfile(profileParam) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PROFILE_UPDATE_PROFILE), profileParam, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.updateProfile()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    getProfileById(profileId) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const AdditionalAttributes = [
                    "birthdate",
                    "emailaddress1",
                    "emailaddress2",
                    "emailaddress3",
                    // "dtk_preferredcontactlanguage",
                    "birthdate",
                    "salutation",
                    "address1_line1",
                    "address1_county",
                    "telephone1",
                    "telephone2",
                    "telephone3",
                    "address1_line2",
                    "address1_line3",
                    "address1_city",
                    "dtk_address1state",
                    "dtk_address1country",
                    "address1_postalcode",
                    "address1_postofficebox",
                    "address1_latitude",
                    "address1_longitude",
                    "address1_fax",
                    "address1_telephone1",
                    "address1_telephone3",
                    "address2_line1",
                    "address2_line2",
                    "address2_line3",
                    "address2_city",
                    "address2_county",
                    "dtk_address2state",
                    "dtk_address2country",
                    "address2_postalcode",
                    "address2_postofficebox",
                    "address2_latitude",
                    "address2_longitude",
                    "address2_fax",
                    "address2_telephone1",
                    "address2_telephone2",
                    "address2_telephone3",
                    "gendercode",
                    "preferredcontactmethodcode",
                    "donotphone",
                    "dtk_donotallowsms",
                    "donotemail",
                    "donotbulkemail",
                    "dtk_donotallowwhatsapp",
                    "lifelongmembershipnumber",
                ];
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PROFILE_GET_BY_ID, { AdditionalAttributes }, { profileId }, "none"));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.getProfileById()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Gets Profile Based on the Search Params
     * @param profileParam JSON Array
     * @returns Returns User Object
     */
    addOrUpdateTicketDeliveryAddress(ticketParam) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PROFILE_ADD_OR_UPDATE_TICKET_DETAILS), ticketParam, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.addOrUpdateTicketDeliveryAddress()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Gets Download Data Based on the Search Params
     * @param profileParam JSON Array
     * @returns string
     */
    downloadData(data) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { profileID, userIPAddress, profileType, password } = data;
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PROFILE_DOWNLOAD_DATA, { userIPAddress, profileType, password }, { profileID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.downloadData()", error);
                return "";
            }
        });
    }
    /** Initiate profile delete
     * @param profileParam JSON Array
     * @returns Returns string
     */
    deleteProfile(data) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { profileID, userIPAddress, profileType, reason, operatorID } = data;
                const response = yield this.axiosInstance.delete(this.router.getRoute(constants.ENDPOINT_PROFILE_DELETE_PROFILE, { userIPAddress, profileType, reason, operatorID }, { profileID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.deleteProfile()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Revokes profile delete request
     * @param profileParam JSON Array
     * @returns string
     */
    revokeDeleteProfile(data) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { profileID, userIPAddress, profileType, reason } = data;
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PROFILE_REVOKE_DELETE_PROFILE, { userIPAddress, profileType, reason }, { profileID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.revokeDeleteProfile()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    getPersonalInterest(profileID) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PROFILE_PERSONAL_INTEREST, {}, { profileID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Profile.getPersonalInterest()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    updatePersonalInterest(profileID, associatePersonalInterests, disassociatePersonalInterests) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_PROFILE_PERSONAL_INTEREST, { associatePersonalInterests, disassociatePersonalInterests }, { profileID }, "none"), constants.JSON_HEADER);
                return true;
            }
            catch (error) {
                Logger_1.default.error("Profile.updatePersonalInterest()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
}
exports.default = Profile;
//# sourceMappingURL=Profile.js.map