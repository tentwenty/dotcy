"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class Products {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /**
     * Fetches Product Hierarchy
     * @param productHierarchyID (Optional) The product hierarchy to load the groups from.
     * @param loadProductVariants (Optional) Loads product variants. Defaults to false.
     * @param onlyChannelPriceListSupportedProducts (Optional) Return only products for a channel. Defaults to true.
     * @param loadCapacity (Optional) Loads capacity if set to true. Defaults to false.
     * @param loadTargetGroups (Optional) Loads target groups. Defaults to false.
     * @param loadAccessAreas (Optional) Loads access areas for products if set to true. Defaults to false.
     * @param languageIDs (Optional) Loads products from specific lang. Defaults to empty Array.
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getProductHierarchy(params = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { productHierarchyID = "", loadProductVariants = false, onlyChannelPriceListSupportedProducts = true, loadCapacity = true, loadTargetGroups = true, loadAccessAreas = true, loadTags = true, loadSalesLiterature = true, languageIDs = [], caching = "Default", } = params;
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PRODUCT_HIERARCHY, {
                    productHierarchyID,
                    loadProductVariants,
                    onlyChannelPriceListSupportedProducts,
                    loadCapacity,
                    loadTargetGroups,
                    loadAccessAreas,
                    loadTags,
                    loadSalesLiterature,
                    languageIDs,
                    caching,
                }));
                Logger_1.default.info(response.data);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getProductHierarchy()", error);
                return [{}];
            }
        });
    }
    /**
     * Fetches all the product variants
     * @param productIDs - Ids of products for which variants need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getProductVariants(request, caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PRODUCT_VARIANTS, { caching }), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getProductVariants()", error);
                return {};
            }
        });
    }
    /**
     * Fetches the product capacity
     * @param searchParams JSON Array
     * @returns Returns the response from dotcy platform
     */
    getProductCapacity(searchParams) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_PRODUCT_CAPACITY), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getProductCapacity()", error);
                return {};
            }
        });
    }
    /**
     * Fetches all the product time blocks
     * @param startdate - current date
     * @param enddate - current date + sellablebymaxdaysinfuture
     * @returns Returns the response from dotcy platform
     */
    getProductTimeBlocks(productID, startDate, endDate) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_TIME_BLOCKS, { startDate, endDate }, { productID }, constants.QS_INDEX_SEPARATOR));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getProductTimeBlocks()", error);
                return [];
            }
        });
    }
    /**
     * Fetches all the variant type options
     * @returns Returns the response from dotcy platform
     */
    getVariantTypeOptions(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PRODUCT_VARIANT_TYPE_OPTIONS, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getVariantTypeOptions()", error);
                return [];
            }
        });
    }
    /** Fetches the upsell products
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    getUpsellConfiguration(request) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getUpsellConfiguration()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Fetches all the variant type options
     * @returns Returns the response from dotcy platform
     */
    addToWaitingList(params) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_WAITING_LIST), params, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.addToWaitingList()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Fetches the upsell products
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    getUpSellConfigurationDetails(upsellConfigIDs, caching = "Default") {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG_DETAILS, {
                    caching,
                }), upsellConfigIDs, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getUpSellConfigurationDetails()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Fetches all images for specified product IDs
     * @param productIDs - Ids of products for which images need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getProductImageUrls(productIDs, caching) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_IMAGE_URLS, {
                    productIDs,
                    caching,
                }, null, "index"));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getProductImageUrls()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Fetches all images for specified product IDs
     * @param salesLiteratureID - Ids of products for which images need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getproductLiterature(salesLiteratureID, caching) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_PRODUCT_LITERATURE, {
                    salesLiteratureID,
                    caching,
                }, null, "index"));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Products.getproductLiterature()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error === null || error === void 0 ? void 0 : error.response.status);
            }
        });
    }
}
exports.default = Products;
//# sourceMappingURL=Products.js.map