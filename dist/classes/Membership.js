"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class Membership {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /**
     * Fetches all the membership types
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getMembershipTypes(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_TYPES, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.getPaymentMethods()", error);
                return [];
            }
        });
    }
    /**
     * Fetch membership information
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    getSingleCustomerMembershipInfo(profileId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_SINGLE_MEM_INFO, {}, { profileId }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.getSingleCustomerMembershipInfo()", error);
                return [];
            }
        });
    }
    /**
     * Fetch membership information
     * @param reqMem - IReqMemCustInfo
     * @returns Returns the response from dotcy platform
     */
    getCustomerMembershipInfo(reqMem) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { profileIDs, profileType, retrieveOnlyActiveMembers } = reqMem;
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_CUSTOMER_MEMBERSHIP_INFO, {
                    profileIDs,
                    profileType,
                    retrieveOnlyActiveMembers,
                }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.getCustomerMembershipInfo()", error);
                return [];
            }
        });
    }
    /**
     * Fetch membership transition type
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getMembershipTransitionTypes(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_TRANSITION_TYPES, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.getMembershipTransitionTypes()", error);
                return [];
            }
        });
    }
    /**
     * Fetch linked members of a family membership
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    getLinkedMembership(profileId, caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_LINKED_MEM_INFO, {
                    caching,
                    profileId,
                }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.getLinkedMembership()", error);
                return [];
            }
        });
    }
    /**
     * Send membership invite for sub member
     * @param request - profile id of the submember to be invited
     * @param caching Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    sendMemberRegistrationInvite(request, userIPAddress) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_SEND_MEMB_REG_INVITE, {
                    userIPAddress,
                }), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.sendMemberRegistrationInvite()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Fetch the benefit consumption of membership by a member
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    getBenefitConsumption(profileId, caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_BENEFIT_CONSUMPTION, {
                    caching,
                    profileId,
                }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.getBenefitConsumption()", error);
                return [];
            }
        });
    }
    /**
     * @param request - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    addLinkedMembers(request) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_ADD_LINKED_MEMBER), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.addLinkedMember()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    calculateMembershipAddOnPrice(memAddOnReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { calculationType, request } = memAddOnReq;
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_ADD_ON_PRICE, {
                    calculationType,
                }), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.calculateMembershipAddOnPrice()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    validateInvitation(requestID, userIPAddress) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_VALIDATE_PRICE, { userIPAddress }, { requestID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.validateInvitation()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    memberInvitationUpdate(request, userIPAddress) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_INVITATION_UPDATE, {
                    userIPAddress,
                }), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.memberInvitationUpdate()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    calculateMembershipUpgradePrice(memAddOnReq) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { calculationType, request } = memAddOnReq;
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_UPGRADE_PRICE, {
                    calculationType,
                }), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Membership.calculateMembershipUpgradePrice()", error);
                return {};
            }
        });
    }
}
exports.default = Membership;
//# sourceMappingURL=Membership.js.map