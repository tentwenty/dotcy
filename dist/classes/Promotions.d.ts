import IConfig from "../interfaces/IConfig";
import IPromotions from "../interfaces/IPromotions";
import { IRespCoupons, IRespValidateCoupon, IRespValidateVoucher, IRespVouchers } from "../interfaces/responses/promotions";
import { IReqSearchCoupon, IReqValidateCoupon, IReqSearchVouchers, IReqValidateVoucher } from "../interfaces/requests/promotions";
export default class Promotions implements IPromotions {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /** Searches for a matching coupons based on coupon codes
     * @param searchParams A fully populated search params object
     * @param searchParams.PaymentMethodID The ID of the payment method
     * @param searchParams.SearchString The coupon code to search for
     * @param searchParams.ReturnInactiveCoupons (Optional) Boolean indicating whether to return inactive copons
     * @param searchParams.OnlySpecificCouponsIDs (Optional) Coupon IDs to search for
     * @param searchParams.IncludePrerequisitesAndOffers (Optional) Boolean indicating whether prerequisites and offers need to be considered
     * @returns Returns the reponse from dotcy platform
     */
    searchCoupon(searchParams: IReqSearchCoupon): Promise<IRespCoupons[]>;
    /**
     * Validates the coupon and returns null if invalid or a decimal of the discount value when valid
     * @param PaymentMethodID ID string of the payment method;
     * @param CouponCode Coupon code
     * @param BookingID ID string of the booking (For an already existing booking)
     * @param CurrencyID ID string of the currency
     * @param Booking (Optional) Fully populated booking object if you need to create a new one
     * @param EntryStartDate (Optional) Start date string for a free product
     * @param OperatorID (Optional) ID of the operator that started this transaction
     * @returns Returns boolean value indicating the completion of task
     */
    validateCoupon(couponData: IReqValidateCoupon): Promise<IRespValidateCoupon>;
    /** Searches the discount vouchers for the specified voucher code and returns a response of the matching vouchers
     * @param searchParams A fully populated search params object
     * @param searchParams.PaymentMethodID The ID of the payment method
     * @param searchParams.SearchString The voucher code to search for
     * @param searchParams.ReturnInactiveCoupons (Optional) Boolean indicating whether to return inactive vouchers
     * @param searchParams.OnlySpecificVoucherIDs (Optional) Vouchers IDs to search for
     * @param searchParams.IncludePrerequisitesAndOffers (Optional) Boolean indicating whether prerequisites and offers need to be considered
     * @returns Returns the reponse from dotcy platform
     */
    searchVoucher(searchParams: IReqSearchVouchers): Promise<IRespVouchers[]>;
    /** Searches the discount vouchers for the specified voucher code and returns a response of the matching vouchers
     * @param validateReq A fully populated search params object
     * @param validateReq.PaymentMethodID The ID of the payment method
     * @param validateReq.VoucherCode The voucher code to be validated against the booking
     * @param validateReq.EntryStartDate Entry date when the voucher will be consumed
     * @param validateReq.BookingID (Optional) ID of booking that needs to be validated against the voucher code
     * @param validateReq.Booking (Optional) Fully populated booking object if booking is not already created
     * @param validateReq.CurrencyID (Optional) ID string of the currency
     * @param validateReq.OperatorID (Optional) ID of the operator that initiated this action
     * @returns Returns the reponse from dotcy platform
     */
    validateVoucher(validateReq: IReqValidateVoucher): Promise<IRespValidateVoucher>;
}
