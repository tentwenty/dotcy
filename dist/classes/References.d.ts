import IReferences from "../interfaces/IReferences";
import { ICaching } from "../interfaces/ITypes";
import IConfig from "../interfaces/IConfig";
import { IRespChCurrencies, IRespChPricelists, IRespCountries, IRespLang, IRespPersInterests, IRespTaxProfiles, IRespStates, IRespTargetGroup, IRespVenueAccess, IRespAgeRange, IRespSchoolGrades, IRespEduLevels, IRespTags, IRespVisitorCategories } from "../interfaces/responses/references";
export default class References implements IReferences {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getCountries(caching?: ICaching): Promise<IRespCountries[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getStates(caching?: ICaching): Promise<IRespStates[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getTargetGroups(caching?: ICaching): Promise<IRespTargetGroup[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getVenueAccessAreas(venueID: string, caching?: ICaching): Promise<IRespVenueAccess[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getAgeRanges(caching?: ICaching): Promise<IRespAgeRange[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getSchoolGrades(caching?: ICaching): Promise<IRespSchoolGrades[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getEducationalLevels(caching?: ICaching): Promise<IRespEduLevels[]>;
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getTags(caching?: ICaching): Promise<IRespTags[]>;
    /**
     * Get the list of Currencies supported by the current channel
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getChannelCurrencies(caching?: ICaching): Promise<IRespChCurrencies[]>;
    /**
     * Gets the list of Personal Interests
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getPersonalInterests(caching?: ICaching): Promise<IRespPersInterests[]>;
    /**
     * Get the list of languages defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getLanguages(caching?: ICaching): Promise<IRespLang[]>;
    /**
     * Get the list of available price lists for the specified channel
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @param onlyChannelSupportedPriceLists (Optional) Fetches only pricelist supported by the channel if set to true
     * @returns Returns the reponse from dotcy platform
     */
    getChannelPriceLists(caching?: ICaching, onlyChannelSupportedPriceLists?: boolean): Promise<IRespChPricelists[]>;
    /**
     * Get the list of tax profiles - based on the specified tax profile ids
     * @param taxProfileIDs The IDs of the tax profiles to return their details
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getTaxProfiles(taxProfileIDs?: string[], caching?: ICaching): Promise<IRespTaxProfiles[]>;
    /**
     * Get the list of visitor categories
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getVisitorCategories(caching?: ICaching): Promise<IRespVisitorCategories[]>;
}
