import IConfig from "../interfaces/IConfig";
import IEventSession from "../interfaces/IEventSession";
import { IRespEventSession, IRespSessionSlot, IRespRemainingCapacity, IRespSessionProduct } from "../interfaces/responses/eventsession";
import { IReqEventSession, IReqSessionSlot, IReqSessionProduct } from "../interfaces/requests/eventsession";
export default class EventSession implements IEventSession {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /** Posts Search Event Sessions
     * @param sessionParams IReqEventSession
     * @returns Returns Session Object Array
     */
    getEventSessions(sessionParams: IReqEventSession): Promise<IRespEventSession[]>;
    /** Posts Search Event Sessions
     * @param sessionParams IReqEventSession
     * @returns Returns Session Object Array
     */
    getSessionsSlots(sessionParams: IReqSessionSlot): Promise<IRespSessionSlot[]>;
    /** Posts Search Event Sessions
     * @param capacityParams IReqRemainingCapacity
     * @returns Returns capacity Object
     */
    getRemainingCapacity(eventID: string): Promise<IRespRemainingCapacity>;
    /** Posts Search Event Sessions
     * @param sessionParams IReqEventSession
     * @returns Returns Session Object Array
     */
    getSessionProducts(sessionParams: IReqSessionProduct): Promise<IRespSessionProduct[]>;
}
