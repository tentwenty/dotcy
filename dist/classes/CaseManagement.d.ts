import IConfig from "../interfaces/IConfig";
import { ICaching } from "../interfaces/ITypes";
import ICaseManagement from "../interfaces/ICaseManagement";
import { IReqSearchCase, IReqCreateCase } from "../interfaces/requests/casemanagement";
import { IRespCaseType, IRespSearchCase, IRespCreateCase, IRespCaseDetails } from "../interfaces/responses/casemanagement";
export default class CaseManagement implements ICaseManagement {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Use this method to get case types
     * @returns Returns the reponse from dotcy platform
     */
    getCaseTypes(onlyChannelSupportedTypes?: boolean, caching?: ICaching): Promise<IRespCaseType[]>;
    /**
     * Use this method to generate OTP
     * @param req The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    searchCases(req: IReqSearchCase): Promise<IRespSearchCase[]>;
    /**
     * Use this method to generate OTP
     * @param req The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    createCase(req: IReqCreateCase): Promise<IRespCreateCase>;
    /**
     * Fetch individual case details
     * @param caseID The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    getCaseDetails(caseID: string): Promise<IRespCaseDetails>;
}
