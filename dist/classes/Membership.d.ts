import IConfig from "../interfaces/IConfig";
import { ICaching } from "../interfaces/ITypes";
import IMembership from "../interfaces/IMembership";
import { IRespMemCustInfo, IRespMemType, IRespMemTransitionType, IRespGetLinkedMem, IRespBenefitConsumption, IRespMemSendInviteMember, IRespMemCalculateMemAddOnPrice, IRespMemValidateInvite, IRespMemInvitationUpdate } from "../interfaces/responses/membership";
import { IReqMemCustInfo, IReqMemAddLinkedMember, IReqMemInviteMember, IReqMemCalculateMemAddOnPrice, IReqMemInvitationUpdate } from "../interfaces/requests/membership";
export default class Membership implements IMembership {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Fetches all the membership types
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getMembershipTypes(caching?: ICaching): Promise<IRespMemType[]>;
    /**
     * Fetch membership information
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    getSingleCustomerMembershipInfo(profileId: string): Promise<IRespMemCustInfo[]>;
    /**
     * Fetch membership information
     * @param reqMem - IReqMemCustInfo
     * @returns Returns the response from dotcy platform
     */
    getCustomerMembershipInfo(reqMem: IReqMemCustInfo): Promise<IRespMemCustInfo[]>;
    /**
     * Fetch membership transition type
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getMembershipTransitionTypes(caching?: ICaching): Promise<IRespMemTransitionType[]>;
    /**
     * Fetch linked members of a family membership
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    getLinkedMembership(profileId: string, caching?: string): Promise<IRespGetLinkedMem[]>;
    /**
     * Send membership invite for sub member
     * @param request - profile id of the submember to be invited
     * @param caching Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    sendMemberRegistrationInvite(request: IReqMemInviteMember, userIPAddress: string): Promise<IRespMemSendInviteMember>;
    /**
     * Fetch the benefit consumption of membership by a member
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    getBenefitConsumption(profileId: string, caching?: string): Promise<IRespBenefitConsumption[]>;
    /**
     * @param request - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    addLinkedMembers(request: IReqMemAddLinkedMember): Promise<string>;
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    calculateMembershipAddOnPrice(memAddOnReq: IReqMemCalculateMemAddOnPrice): Promise<IRespMemCalculateMemAddOnPrice>;
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    validateInvitation(requestID: string, userIPAddress: string): Promise<IRespMemValidateInvite>;
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    memberInvitationUpdate(request: IReqMemInvitationUpdate, userIPAddress?: string): Promise<IRespMemInvitationUpdate>;
    calculateMembershipUpgradePrice(memAddOnReq: IReqMemCalculateMemAddOnPrice): Promise<IRespMemCalculateMemAddOnPrice>;
}
