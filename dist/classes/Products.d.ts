import IProducts from "../interfaces/IProducts";
import IConfig from "../interfaces/IConfig";
import { ICaching } from "../interfaces/ITypes";
import { IRespProductHierarchy, IRespProductVariants, IRespGetProductCapacity, IRespProductTimeBlock, IRespVariantTypeOptions, IRespGetUpsellConfiguration, IRespWaitingList, IRespGetUpsellConfigurationDetails, IRespGetProductImageURLs, IRespGetProductLiterature } from "../interfaces/responses/products";
import { IReqGetProductCapacity, IReqGetUpsellConfiguration, IReqProductHierarchy, IReqWaitingList, IReqProductVariants } from "../interfaces/requests/products";
export default class Products implements IProducts {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Fetches Product Hierarchy
     * @param productHierarchyID (Optional) The product hierarchy to load the groups from.
     * @param loadProductVariants (Optional) Loads product variants. Defaults to false.
     * @param onlyChannelPriceListSupportedProducts (Optional) Return only products for a channel. Defaults to true.
     * @param loadCapacity (Optional) Loads capacity if set to true. Defaults to false.
     * @param loadTargetGroups (Optional) Loads target groups. Defaults to false.
     * @param loadAccessAreas (Optional) Loads access areas for products if set to true. Defaults to false.
     * @param languageIDs (Optional) Loads products from specific lang. Defaults to empty Array.
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getProductHierarchy(params?: IReqProductHierarchy): Promise<IRespProductHierarchy[]>;
    /**
     * Fetches all the product variants
     * @param productIDs - Ids of products for which variants need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getProductVariants(request: IReqProductVariants, caching?: ICaching): Promise<IRespProductVariants>;
    /**
     * Fetches the product capacity
     * @param searchParams JSON Array
     * @returns Returns the response from dotcy platform
     */
    getProductCapacity(searchParams: IReqGetProductCapacity): Promise<IRespGetProductCapacity>;
    /**
     * Fetches all the product time blocks
     * @param startdate - current date
     * @param enddate - current date + sellablebymaxdaysinfuture
     * @returns Returns the response from dotcy platform
     */
    getProductTimeBlocks(productID: string, startDate: string, endDate: string): Promise<IRespProductTimeBlock[]>;
    /**
     * Fetches all the variant type options
     * @returns Returns the response from dotcy platform
     */
    getVariantTypeOptions(caching?: ICaching): Promise<IRespVariantTypeOptions[]>;
    /** Fetches the upsell products
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    getUpsellConfiguration(request: IReqGetUpsellConfiguration): Promise<IRespGetUpsellConfiguration[]>;
    /**
     * Fetches all the variant type options
     * @returns Returns the response from dotcy platform
     */
    addToWaitingList(params: IReqWaitingList): Promise<IRespWaitingList>;
    /** Fetches the upsell products
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    getUpSellConfigurationDetails(upsellConfigIDs: string[], caching?: ICaching): Promise<IRespGetUpsellConfigurationDetails[]>;
    /** Fetches all images for specified product IDs
     * @param productIDs - Ids of products for which images need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getProductImageUrls(productIDs: string[], caching?: ICaching): Promise<IRespGetProductImageURLs>;
    /** Fetches all images for specified product IDs
     * @param salesLiteratureID - Ids of products for which images need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    getproductLiterature(salesLiteratureID: string, caching?: ICaching): Promise<IRespGetProductLiterature>;
}
