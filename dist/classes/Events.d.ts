import IConfig from "../interfaces/IConfig";
import IEvents from "../interfaces/IEvents";
import { IRespEvent, IRespEventSetup, IRespSearchEvent } from "../interfaces/responses/event";
import { IReqSearchEvent } from "../interfaces/requests/eventsession";
import { ICaching } from "src/interfaces/ITypes";
export default class Events implements IEvents {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    getEventById(eventId: string): Promise<IRespEvent>;
    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    getEventSetUp(): Promise<IRespEventSetup[]>;
    searchEvents(searchParams: IReqSearchEvent, caching?: ICaching): Promise<IRespSearchEvent[]>;
}
