"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class CaseManagement {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /**
     * Use this method to get case types
     * @returns Returns the reponse from dotcy platform
     */
    getCaseTypes(onlyChannelSupportedTypes = false, caching = "Default") {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_GET_CASE_TYPES, {
                    caching,
                    onlyChannelSupportedTypes,
                }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("CaseManagement.getCaseTypes()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Use this method to generate OTP
     * @param req The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    searchCases(req) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_SEARCH_CASES), req, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("CaseManagement.searchCases()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Use this method to generate OTP
     * @param req The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    createCase(req) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { operatorID } = req, params = __rest(req, ["operatorID"]);
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_CREATE_CASE, {
                    operatorID,
                }), params, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("CaseManagement.createCase()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Fetch individual case details
     * @param caseID The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    getCaseDetails(caseID) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_CASE_DETAILS, { caseID }), caseID, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("CaseManagement.getCaseDetails()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
}
exports.default = CaseManagement;
//# sourceMappingURL=CaseManagement.js.map