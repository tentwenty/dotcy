import IConfig from "../interfaces/IConfig";
import ISeating from "../interfaces/ISeating";
import { IReqSearchSeatParams, IReqSeatingSearchParams, IReqSearchSeatingSections } from "../interfaces/requests/seating";
import { ICaching } from "../interfaces/ITypes";
import { IRespSeatingAreas, IRespSearchSeats, IRespGetSeatingSection } from "../interfaces/responses/seating";
export default class Seating implements ISeating {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    searchSeatingAreas(searchParams: IReqSeatingSearchParams, caching?: ICaching): Promise<IRespSeatingAreas[]>;
    searchSeats(searchParams: IReqSearchSeatParams): Promise<IRespSearchSeats[]>;
    getSeatingSection(seatingSectionID: string): Promise<IRespGetSeatingSection>;
    searchSeatingSections(searchParams: IReqSearchSeatingSections): Promise<IRespGetSeatingSection[]>;
}
