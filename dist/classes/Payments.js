"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class Payments {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /**
     * Fetches all the product variants
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getPaymentMethods(caching = "Default", operatorID = "") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PAYMENT_METHODS, { caching, operatorID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Payments.getPaymentMethods()", error);
                return [];
            }
        });
    }
    /**
     * Fetch the payment related information
     * @param methodType The payment type
     * @param methodID The payment method id
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getPaymentConfig({ methodType, methodID, caching = "Default", }) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PAYMENT_CONFIGS, { caching }, { methodType, methodID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Payments.getPaymentConfig()", error);
                return {};
            }
        });
    }
    /**
     * Post Payment against a booking
     * @param paymentObj.operatorID Fully populated payment object
     * @param paymentObj.autoApprove Auto approve payment
     * @param paymentObj.revalidateBookingOnPayment Force re-validation of BookingRules on each payment issued using the API
     * @param paymentObj.payment Fully populated payment object
     * @param paymentObj.payment.Amount Amount being payed for
     * @param paymentObj.payment.ChannelID Channel from where the payment has been initiated
     * @param paymentObj.payment.CurrencyID Currency id of the payment
     * @param paymentObj.payment.ForceCloseBookingOnFullPayment Close booking once the full amount is payed
     * @param paymentObj.payment.MethodOfPaymentID Payment method ID
     * @param paymentObj.payment.PaymentCategory Payment category
     * @param paymentObj.payment.PaymentStatus Status of the payment. Values can be "Submitted" | "Approved"
     * @param paymentObj.payment.DiscountCoupon (Optional) Discount coupon data object
     * @param paymentObj.payment.DiscountVoucher (Optional) Discount Voucher data object
     * @param paymentObj.payment.NotesAndComments (Optional) Any additional notes
     * @returns return the ID string of the payment
     */
    addPayment(paymentObj) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID, payment, autoApprove = false, revalidateBookingOnPayment = false } = paymentObj, params = __rest(paymentObj, ["bookingID", "payment", "autoApprove", "revalidateBookingOnPayment"]);
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PAYMENT_ADD_PAYMENT, Object.assign({ autoApprove, revalidateBookingOnPayment }, params), { bookingID }), payment, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Payments.addPayment()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Change Payment Status and update bank response
     * @param paymentID Payment ID that needs to be updated
     * @param paymentStatus Payment status string
     * @param bankResponse Bank response object
     * @param operatorID (Optional) Operator ID
     * @returns Returns boolean value indicating the completion of task
     */
    updatePaymentStatus(updateReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { paymentID, paymentStatus = "Submitted", operatorID, bankResponse } = updateReq;
                yield this.axiosInstance.patch(this.router.getRoute(constants.ENDPOINT_PAYMENT_UPDATE_STATUS, { paymentStatus, operatorID }, { paymentID }), bankResponse, constants.JSON_HEADER);
                return true;
            }
            catch (error) {
                Logger_1.default.error("Payments.updatePaymentStatus()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Gets Wallet Balance Based on the Customer ID
     * @param customerID string
     * @returns Returns number
     */
    getWalletBalance(customerID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_PAYMENT_GET_WALLET_BALANCE, {}, { customerID }, "none"));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Payment.getWalletBalance()", error);
                return 0;
            }
        });
    }
    /** Gets Wallet Balance Based on the Customer ID
     * @param paymentReq A fully populated cancel payment request object
     * @param paymentReq.paymentID The ID of the payment to cancel
     * @param paymentReq.terminalID The Terminal from where the cancellation was requested (if applicable)
     * @param paymentReq.operatorID The Operator/User that requested the cancellation (if applicable)
     * @param paymentReq.sessionID The Terminal Session for which the cancellation is issued from
     * @returns Returns boolean value indicating the completion of task
     */
    cancelPayment(paymentReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { paymentID } = paymentReq, params = __rest(paymentReq, ["paymentID"]);
                yield this.axiosInstance.delete(this.router.getRoute(constants.ENDPOINT_PAYMENT_CANCEL_PAYMENT, Object.assign({}, params), { paymentID }));
                return true;
            }
            catch (error) {
                Logger_1.default.error("Payment.cancelPayment()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
}
exports.default = Payments;
//# sourceMappingURL=Payments.js.map