import IConfig from "../interfaces/IConfig";
import { IBookingType, IRespCalculatedTax, ITaxLineItems } from "../interfaces/ITypes";
import IBookings from "../interfaces/IBookings";
import { IReqAddOrUpdateBooking, IReqExtendExpiration, IReqRemoveBookingItem, IReqSaveBooking, IReqUpdateBookingAddress, IReqRemoveTicketHolder, IReqAddOrUpdateTicketholder, IReqGetBookingFromPayment, IReqSearchTicket, IReqSearchBookings, IReqFulfillBooking, IReqSetCustomer, IReqSaveGuestDetails, IReqDownloadTicket, IReqSplitBooking, IReqMergeBooking, IReqBookingValidate } from "../interfaces/requests/bookings";
import { IRespBookingByID, IRespSaveBooking, IRespSearchTicket, IRespBookingByTicketID, IRespSplitBooking, IRespAddOrUpdateBooking, IRespValidateBooking, IRespAddOrUpdateTicketholder } from "../interfaces/responses/bookings";
export default class Bookings implements IBookings {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Use this method to calculate the tax of various items based on their tax profile
     * @param taxLineItems List of items for tax calculation along with their tax information
     * @param taxLineItems.LineItemID - Line item ID
     * @param taxLineItems.ProductID - Product ID
     * @param taxLineItems.AmountBeforeTax - Total amount for a product
     * @param taxLineItems.TaxProfileID - IndividualTaxProfileID of the product
     * @returns Returns the reponse from dotcy platform
     */
    getCalculatedTax(taxLineItems: ITaxLineItems[]): Promise<IRespCalculatedTax>;
    /**
     * Use this method to create a booking
     * @param bookingReq The populated request object
     * @param bookingReq.booking The populated booking object
     * @param bookingReq.operatorID The Operator ID creating the booking
     * @param bookingReq.terminalID The terminal ID (POS or KIOSK) to which the booking is allocated to
     * @param bookingReq.sessionID The terminal session on which to assign the booking
     * @param bookingReq.generatePassword (Optional) If a password should be generated on the booking. Defaults to false.
     * @param bookingReq.createBookingItems (Optional) if set to true it will also create the items in the booking object that are not assigned a CRM ID. Default to true.
     * @returns Returns the reponse from dotcy platform
     */
    saveBooking(bookingReq: IReqSaveBooking): Promise<IRespSaveBooking>;
    /**
     * Use this method to create a booking
     * @param updateReq The populated request object
     * @param updateReq.address The populated address object
     * @param updateReq.address.IsBillToAddress Use as billing address
     * @param updateReq.address.IsDeliverToAddress Use as delivery address
     * @param updateReq.address.ContactName Shipping contact name
     * @param updateReq.address.Street1 Address string for street
     * @param updateReq.address.Street2 (Optional) Address string for street
     * @param updateReq.address.Street3 (Optional) Address string for street
     * @param updateReq.address.ZipOrPostalCode Post code number
     * @param updateReq.address.City City name
     * @param updateReq.address.County County name
     * @param updateReq.address.Country Country name
     * @param updateReq.address.State State name
     * @param updateReq.address.State State name
     * @param updateReq.address.DeliveryDate delivery date
     * @param updateReq.address.DeliveryNotes Additional notes
     * @param updateReq.bookingID Booking ID to update
     * @param updateReq.operatorID The user doing this action
     * @returns Returns the reponse from dotcy platform
     */
    updateBookingAddress(updateReq: IReqUpdateBookingAddress): Promise<boolean>;
    /**
     * Adds or Updates a product in the booking
     * @param addOrUpdateReq The populated request object
     * @param bookingID - ID of booking that needs to be updated
     * @param bookingItem - Fully populated booking item
     * @param bookingItem.BaseProductID - ID of the base product
     * @param bookingItem.ProductVariantID - ID of the product Variant
     * @param bookingItem.EntryTimeSlots - Entry slots object
     * @param bookingItem.EntryTimeSlots.Start - Start date time string
     * @param bookingItem.EntryTimeSlots.End - End date time string
     * @param bookingItem.ID - (Optional) Updated the booking item if present else creates a new booking item
     * @param terminalID The assigned terminal from where the action was entered
     * @param operatorID The user executing the action
     * @param bookingType (Optional) "Order" | "Opportunity" | "Both". Defaults to Order
     * @param priceCalculationInApi (Optional) If the API should recalculate the pricing and taxes. Defaults to false
     * @returns Returns response from dotcy platform
     */
    addOrUpdateBookingItem(addOrUpdateReq: IReqAddOrUpdateBooking): Promise<IRespAddOrUpdateBooking>;
    /**
     * Removes the specified line from the provided booking
     * @param removeItemReq Booking item details
     * @param removeItemReq.bookingID Booking item details
     * @param removeItemReq.bookingItemID Booking item details
     * @param removeItemReq.bookingType (Optional) "Order" | "Opportunity" | "Both". Defaults to Order
     * @param removeItemReq.terminalID (Optional) The temrinal from which the request was made
     * @param removeItemReq.operatorID (Optional) The user executing the action
     * @returns Returns boolean value to indicate the success of the removal of item
     */
    removeBookingItem(removeItemReq: IReqRemoveBookingItem): Promise<boolean>;
    /**
     * Get booking by ID
     * @param bookingID ID string of the booking
     * @param bookingType (Optional) Type of booking. Defaults to Order
     * @param loadCustomerInfo (Optional) Default to false
     * @returns Returns the reponse from dotcy platform
     */
    getBookingById(bookingID: string, bookingType?: IBookingType, loadCustomerInfo?: boolean): Promise<IRespBookingByID>;
    /**
     * Use this method to retrieve a booking by order-number (booking reference no) and email
     * If booking is null then the method will not cross reference the email address
     * @param bookingReference Unique booking reference no/Order ID
     * @param emailAddress (Optional) Email address of customer for additional check
     * @param loadCustomerInfo (Optional) Default to false
     * @returns Returns the reponse from dotcy platform
     */
    getBookingByRefNo(bookingReference: string, emailAddress?: string, loadCustomerInfo?: boolean): Promise<IRespBookingByID>;
    /**
     * Cancels the specified booking (opportunity or order). if the booking is an order then also update the no sale reason if one is supplied
     * @param bookingID The ID of the booking to cancel
     * @param noSaleReasonID (Optional) The ID of the no-sales reason
     * @param operatorID (Optional) The user doing this action
     * @returns Returns the reponse from dotcy platform
     */
    abandonBooking(bookingID: string, noSaleReasonID?: string, operatorID?: string): Promise<boolean>;
    /**
     * Cancels the specified booking (opportunity or order). if the booking is an order then also update the no sale reason if one is supplied
     * @param bookingID The ID of the booking to cancel
     * @param noSaleReasonID (Optional) The ID of the no-sales reason
     * @param operatorID (Optional) The user doing this action
     * @param bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @returns Returns the reponse from dotcy platform
     */
    cancelBooking(bookingID: string, noSaleReasonID?: string, operatorID?: string, bookingRecordType?: IBookingType): Promise<boolean>;
    /**
     * Extends the booking's expiration date
     * @param extendExpReq A fully populated request object
     * @param extendExpReq.bookingID The ID of the booking to cancel
     * @param extendExpReq.periodInMinutes (Optional) A period in minutes to extend the expiration of the booking - starting from the current time
     * @param extendExpReq.expirationDate (Optional) A date the booking expiration will be extended to. Date and Time
     * @param extendExpReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param extendExpReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    extendExpiration(extendExpReq: IReqExtendExpiration): Promise<boolean>;
    /**
     * Remove the specified ticket holder from the booking item
     * @param removeReq A fully populated request object
     * @param removeReq.bookingItemID The ID of the booking to cancel
     * @param removeReq.ticketHolderID The ID of the booking to cancel
     * @param removeReq.bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @param removeReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param removeReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    removeTicketholderFromBookingItem(removeReq: IReqRemoveTicketHolder): Promise<boolean>;
    /**
     * Add a new ticket holder record to the booking item or update an existing one
     * @param addOrUpdateReq A fully populated request object
     * @param addOrUpdateReq.bookingItemID The booking item ID
     * @param addOrUpdateReq.ticketHolder The ticket holder object.
     * @param removeReq.bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @param removeReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param removeReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    addOrUpdateTicketholderFromBookingItem(addOrUpdateReq: IReqAddOrUpdateTicketholder): Promise<IRespAddOrUpdateTicketholder>;
    /**
     * Loads and returns the details of the specified booking using the payment ID
     * @param bookingReq A fully populated request object
     * @param bookingReq.paymentID The ID of the payment to match against the booking
     * @param bookingReq.operatorID (Optional) The operator / user making the request
     * @param bookingReq.partnerID (Optional) The ID of the business partner (request from business portal)
     * @param bookingReq.portalUserID (Optional)The ID of the business portal user
     * @param bookingReq.loadCustomerInfo (Optional) Load additional customer info. Defaults to false
     * @returns Returns the reponse from dotcy platform
     */
    getBookingFromPaymentID(bookingReq: IReqGetBookingFromPayment): Promise<IRespBookingByID>;
    /**
     * Get bookings based on search criteria
     * @param searchParams A fully populated request object
     * @param lastXBookings The last x number of bookings that need to be fetched
     * @returns Returns the reponse from dotcy platform
     */
    searchBookings(bookingReq: IReqSearchBookings): Promise<IRespBookingByID[]>;
    /**
     * Loads and returns the tickets purchased by the user
     * @param ticketReq json response
     * @returns Returns the reponse from dotcy platform
     */
    searchTickets(ticketReq: IReqSearchTicket): Promise<IRespSearchTicket[]>;
    /**
     * Fulfill a booking
     * @param bookingReq - A fully populated booking request object
     * @returns Returns the response from dotcy platform
     */
    fulfillBooking(bookingReq: IReqFulfillBooking): Promise<boolean>;
    /**
     * Set a customer for a booking
     * @param bookingId - A fully populated booking request object
     * @returns Returns the response from dotcy platform
     */
    setCustomer(bookingReq: IReqSetCustomer): Promise<boolean>;
    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    getBookingByTicketId(ticketID: string): Promise<IRespBookingByTicketID>;
    /**
     * Sets guest details for a booking
     * @param guestDetails A fully populated guest details object
     * @returns Returns the response from dotcy platform
     */
    saveGuestDetails(guestDetails: IReqSaveGuestDetails): Promise<boolean>;
    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    downloadTicket(ticket: IReqDownloadTicket): Promise<string>;
    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    getElectronicTicket(ticketID: string, versionNumber: string): Promise<string>;
    /**
     * Split booking
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    splitBooking(bookingReq: IReqSplitBooking): Promise<IRespSplitBooking>;
    /**
     * merge booking
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    mergeBooking(bookingReq: IReqMergeBooking): Promise<boolean>;
    /**
     * Validate booking for all the mandatory fields
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    validateBooking(bookingReq: IReqBookingValidate): Promise<IRespValidateBooking>;
}
