import IConfig from "../interfaces/IConfig";
import { ICaching, ITagScope } from "../interfaces/ITypes";
import ITags from "../interfaces/ITags";
import { IRespGetTags, IRespTagsForRecords } from "../interfaces/responses/tags";
import { IReqTagsForRecords } from "../interfaces/requests/tags";
export default class Tags implements ITags {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Returns the list of tags for the provided entity type
     * @param scope - Scope of tags
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getTags(scope: ITagScope, caching?: ICaching): Promise<IRespGetTags[]>;
    /**
     * Returns the list of tags linked to a set of records specified by the search criteria
     * @param scope - Scope of tags
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getTagsForRecords(searchParams: IReqTagsForRecords): Promise<IRespTagsForRecords>;
}
