"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class Promotions {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /** Searches for a matching coupons based on coupon codes
     * @param searchParams A fully populated search params object
     * @param searchParams.PaymentMethodID The ID of the payment method
     * @param searchParams.SearchString The coupon code to search for
     * @param searchParams.ReturnInactiveCoupons (Optional) Boolean indicating whether to return inactive copons
     * @param searchParams.OnlySpecificCouponsIDs (Optional) Coupon IDs to search for
     * @param searchParams.IncludePrerequisitesAndOffers (Optional) Boolean indicating whether prerequisites and offers need to be considered
     * @returns Returns the reponse from dotcy platform
     */
    searchCoupon(searchParams) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PPROMOTIONS_SEARCH_COUPONS), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Promotions.searchCoupon()", error);
                return [];
            }
        });
    }
    /**
     * Validates the coupon and returns null if invalid or a decimal of the discount value when valid
     * @param PaymentMethodID ID string of the payment method;
     * @param CouponCode Coupon code
     * @param BookingID ID string of the booking (For an already existing booking)
     * @param CurrencyID ID string of the currency
     * @param Booking (Optional) Fully populated booking object if you need to create a new one
     * @param EntryStartDate (Optional) Start date string for a free product
     * @param OperatorID (Optional) ID of the operator that started this transaction
     * @returns Returns boolean value indicating the completion of task
     */
    validateCoupon(couponData) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PPROMOTIONS_VALIDATE_COUPON), couponData, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Promotions.validateCoupon()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /** Searches the discount vouchers for the specified voucher code and returns a response of the matching vouchers
     * @param searchParams A fully populated search params object
     * @param searchParams.PaymentMethodID The ID of the payment method
     * @param searchParams.SearchString The voucher code to search for
     * @param searchParams.ReturnInactiveCoupons (Optional) Boolean indicating whether to return inactive vouchers
     * @param searchParams.OnlySpecificVoucherIDs (Optional) Vouchers IDs to search for
     * @param searchParams.IncludePrerequisitesAndOffers (Optional) Boolean indicating whether prerequisites and offers need to be considered
     * @returns Returns the reponse from dotcy platform
     */
    searchVoucher(searchParams) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PPROMOTIONS_SEARCH_VOUCHER), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Promotions.searchVoucher()", error);
                return [];
            }
        });
    }
    /** Searches the discount vouchers for the specified voucher code and returns a response of the matching vouchers
     * @param validateReq A fully populated search params object
     * @param validateReq.PaymentMethodID The ID of the payment method
     * @param validateReq.VoucherCode The voucher code to be validated against the booking
     * @param validateReq.EntryStartDate Entry date when the voucher will be consumed
     * @param validateReq.BookingID (Optional) ID of booking that needs to be validated against the voucher code
     * @param validateReq.Booking (Optional) Fully populated booking object if booking is not already created
     * @param validateReq.CurrencyID (Optional) ID string of the currency
     * @param validateReq.OperatorID (Optional) ID of the operator that initiated this action
     * @returns Returns the reponse from dotcy platform
     */
    validateVoucher(validateReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_PPROMOTIONS_VALIDATE_VOUCHER), validateReq, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Promotions.validateVoucher()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
}
exports.default = Promotions;
//# sourceMappingURL=Promotions.js.map