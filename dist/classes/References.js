"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
class References {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getCountries(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_COUNTRIES, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getCountries()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getStates(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_STATES, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getStates()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getTargetGroups(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_TARGET_GROUPS, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getTargetGroups()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getVenueAccessAreas(venueID, caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_VENUE_ACCESS_AREAS, { caching }, { venueID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getVenueAccessAreas()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getAgeRanges(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                Logger_1.default.info(`Calling api :  ${constants.ENDPOINT_REFERENCES_AGE_RANGE}`);
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_AGE_RANGE, { caching }));
                Logger_1.default.info(`References.getAgeRange() : ${response}`);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getAgeRanges()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getSchoolGrades(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_SCHOOL_GRADES, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getSchoolGrades()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getEducationalLevels(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_EDUCATIONAL_LEVELS, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getEducationalLevels()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getTags(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_TAGS, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getTags()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of Currencies supported by the current channel
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getChannelCurrencies(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                Logger_1.default.info(`Calling api :  ${constants.ENDPOINT_REFERENCES_CH_CURRENCIES}`);
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_CH_CURRENCIES, { caching }));
                Logger_1.default.info(`References.getChannelCurrencies() : ${response}`);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getChannelCurrencies()", error);
                return [];
            }
        });
    }
    /**
     * Gets the list of Personal Interests
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getPersonalInterests(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_PER_INTERESTS, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getPersonalInterests()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of languages defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getLanguages(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_LANGUAGES, { caching }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getLanguages()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of available price lists for the specified channel
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @param onlyChannelSupportedPriceLists (Optional) Fetches only pricelist supported by the channel if set to true
     * @returns Returns the reponse from dotcy platform
     */
    getChannelPriceLists(caching = "Default", onlyChannelSupportedPriceLists = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_CH_PRICELIST, {
                    caching,
                    onlyChannelSupportedPriceLists,
                }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getChannelPriceLists()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of tax profiles - based on the specified tax profile ids
     * @param taxProfileIDs The IDs of the tax profiles to return their details
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getTaxProfiles(taxProfileIDs = [], caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_TAX_PROFILES, {
                    caching,
                    taxProfileIDs,
                }, {}, constants.QS_INDEX_SEPARATOR));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getTaxProfiles()", error);
                return [];
            }
        });
    }
    /**
     * Get the list of visitor categories
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    getVisitorCategories(caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_REFERENCES_VISITOR_CATEGORIES, {
                    caching,
                }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("References.getVisitorCategories()", error);
                return [];
            }
        });
    }
}
exports.default = References;
//# sourceMappingURL=References.js.map