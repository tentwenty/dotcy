import IConfig from "../interfaces/IConfig";
import ICommunications from "../interfaces/ICommunications";
import { IReqGenerateOTP, IReqSendActivity, IReqVerifyOTP } from "../interfaces/requests/communications";
import { IRespCustomNotifications, IRespGenerateOTP, IRespSendActivity, IRespVerifyOTP, IRespSendTicketsToTicketholder, IRespBulkNotification } from "../interfaces/responses/communications";
import { IPriority } from "../interfaces/ITypes";
export default class Communications implements ICommunications {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Use this method to generate OTP
     * @param otpReq The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    generateOTP(otpReq: IReqGenerateOTP): Promise<IRespGenerateOTP>;
    /**
     * Use this method to verify OTP
     * @param otpReq The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    verifyOTP(otpReq: IReqVerifyOTP): Promise<IRespVerifyOTP>;
    /**
     * Creates and Sends a direct activity (email, SMS, WhatsApp)
     * @param activityReq A fully populated request object
     * @returns Returns the reponse from dotcy platform
     */
    sendActivity(activityReq: IReqSendActivity): Promise<IRespSendActivity>;
    /**
     * Returns the list of custom notifications generated from the platform (such as mobile notifications)
     * @param lastActivityTimeStamp The date/time stamp from which onwards the method will return the created activities
     * @param activityPriority A filter of the priority of the messages to be retrieved
     * @returns Returns the reponse from dotcy platform
     */
    getCustomNotifications(lastActivityTimeStamp: string, activityPriority?: IPriority): Promise<IRespCustomNotifications[]>;
    /**
     * Returns bulk notification data
     * @param activityID Notification ID for which information needs to be fetched
     * @returns Returns the reponse from dotcy platform
     */
    getBulkNotification(activityID: string): Promise<IRespBulkNotification>;
    /**
     * Returns the list of custom notifications generated from the platform (such as mobile notifications)
     * @param lastActivityTimeStamp The date/time stamp from which onwards the method will return the created activities
     * @param activityPriority A filter of the priority of the messages to be retrieved
     * @returns Returns the reponse from dotcy platform
     */
    sendTicketsToTicketHolder(ticketID: string, bookingID?: string, senderProfileID?: string): Promise<IRespSendTicketsToTicketholder>;
}
