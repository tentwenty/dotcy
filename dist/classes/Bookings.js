"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
const Error_1 = __importDefault(require("../helpers/Error"));
class Bookings {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    /**
     * Use this method to calculate the tax of various items based on their tax profile
     * @param taxLineItems List of items for tax calculation along with their tax information
     * @param taxLineItems.LineItemID - Line item ID
     * @param taxLineItems.ProductID - Product ID
     * @param taxLineItems.AmountBeforeTax - Total amount for a product
     * @param taxLineItems.TaxProfileID - IndividualTaxProfileID of the product
     * @returns Returns the reponse from dotcy platform
     */
    getCalculatedTax(taxLineItems) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_CALCULATE_TAX), taxLineItems, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.getCalculatedTax()", error);
                return {};
            }
        });
    }
    /**
     * Use this method to create a booking
     * @param bookingReq The populated request object
     * @param bookingReq.booking The populated booking object
     * @param bookingReq.operatorID The Operator ID creating the booking
     * @param bookingReq.terminalID The terminal ID (POS or KIOSK) to which the booking is allocated to
     * @param bookingReq.sessionID The terminal session on which to assign the booking
     * @param bookingReq.generatePassword (Optional) If a password should be generated on the booking. Defaults to false.
     * @param bookingReq.createBookingItems (Optional) if set to true it will also create the items in the booking object that are not assigned a CRM ID. Default to true.
     * @returns Returns the reponse from dotcy platform
     */
    saveBooking(bookingReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { generatePassword = false, createBookingItems = true, booking } = bookingReq, params = __rest(bookingReq, ["generatePassword", "createBookingItems", "booking"]);
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_SAVE_BOOKING, Object.assign({ generatePassword,
                    createBookingItems }, params)), booking, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.saveBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Use this method to create a booking
     * @param updateReq The populated request object
     * @param updateReq.address The populated address object
     * @param updateReq.address.IsBillToAddress Use as billing address
     * @param updateReq.address.IsDeliverToAddress Use as delivery address
     * @param updateReq.address.ContactName Shipping contact name
     * @param updateReq.address.Street1 Address string for street
     * @param updateReq.address.Street2 (Optional) Address string for street
     * @param updateReq.address.Street3 (Optional) Address string for street
     * @param updateReq.address.ZipOrPostalCode Post code number
     * @param updateReq.address.City City name
     * @param updateReq.address.County County name
     * @param updateReq.address.Country Country name
     * @param updateReq.address.State State name
     * @param updateReq.address.State State name
     * @param updateReq.address.DeliveryDate delivery date
     * @param updateReq.address.DeliveryNotes Additional notes
     * @param updateReq.bookingID Booking ID to update
     * @param updateReq.operatorID The user doing this action
     * @returns Returns the reponse from dotcy platform
     */
    updateBookingAddress(updateReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { address, bookingID, operatorID } = updateReq;
                yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_BOOKING_UPDATE_ADDRESS, { operatorID }, { bookingID }), address, constants.JSON_HEADER);
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.updateBookingAddress()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Adds or Updates a product in the booking
     * @param addOrUpdateReq The populated request object
     * @param bookingID - ID of booking that needs to be updated
     * @param bookingItem - Fully populated booking item
     * @param bookingItem.BaseProductID - ID of the base product
     * @param bookingItem.ProductVariantID - ID of the product Variant
     * @param bookingItem.EntryTimeSlots - Entry slots object
     * @param bookingItem.EntryTimeSlots.Start - Start date time string
     * @param bookingItem.EntryTimeSlots.End - End date time string
     * @param bookingItem.ID - (Optional) Updated the booking item if present else creates a new booking item
     * @param terminalID The assigned terminal from where the action was entered
     * @param operatorID The user executing the action
     * @param bookingType (Optional) "Order" | "Opportunity" | "Both". Defaults to Order
     * @param priceCalculationInApi (Optional) If the API should recalculate the pricing and taxes. Defaults to false
     * @returns Returns response from dotcy platform
     */
    addOrUpdateBookingItem(addOrUpdateReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID, bookingItem, bookingType = "Order", priceCalculationInApi = false } = addOrUpdateReq, params = __rest(addOrUpdateReq, ["bookingID", "bookingItem", "bookingType", "priceCalculationInApi"]);
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_ADD_OR_UPDATE_BOOKING, Object.assign({ bookingType, priceCalculationInApi }, params), { bookingID }), bookingItem, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.addOrUpdateBookingItem()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Removes the specified line from the provided booking
     * @param removeItemReq Booking item details
     * @param removeItemReq.bookingID Booking item details
     * @param removeItemReq.bookingItemID Booking item details
     * @param removeItemReq.bookingType (Optional) "Order" | "Opportunity" | "Both". Defaults to Order
     * @param removeItemReq.terminalID (Optional) The temrinal from which the request was made
     * @param removeItemReq.operatorID (Optional) The user executing the action
     * @returns Returns boolean value to indicate the success of the removal of item
     */
    removeBookingItem(removeItemReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID, bookingItemID, bookingType = "Order" } = removeItemReq, params = __rest(removeItemReq, ["bookingID", "bookingItemID", "bookingType"]);
                yield this.axiosInstance.delete(this.router.getRoute(constants.ENDPOINT_BOOKING_REMOVE_ITEM, Object.assign({ bookingType }, params), { bookingID, bookingItemID }));
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.removeBookingItem()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Get booking by ID
     * @param bookingID ID string of the booking
     * @param bookingType (Optional) Type of booking. Defaults to Order
     * @param loadCustomerInfo (Optional) Default to false
     * @returns Returns the reponse from dotcy platform
     */
    getBookingById(bookingID, bookingType = "Order", loadCustomerInfo = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_BOOKING_GET_BY_ID, { bookingType, loadCustomerInfo }, { bookingID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.getBookingById()", error);
                return {};
            }
        });
    }
    /**
     * Use this method to retrieve a booking by order-number (booking reference no) and email
     * If booking is null then the method will not cross reference the email address
     * @param bookingReference Unique booking reference no/Order ID
     * @param emailAddress (Optional) Email address of customer for additional check
     * @param loadCustomerInfo (Optional) Default to false
     * @returns Returns the reponse from dotcy platform
     */
    getBookingByRefNo(bookingReference, emailAddress, loadCustomerInfo = false) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_BOOKING_GET_BY_BOOKING_REFNO, { emailAddress, loadCustomerInfo }, { bookingReference }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.getBookingByRefNo()", error);
                return {};
            }
        });
    }
    /**
     * Cancels the specified booking (opportunity or order). if the booking is an order then also update the no sale reason if one is supplied
     * @param bookingID The ID of the booking to cancel
     * @param noSaleReasonID (Optional) The ID of the no-sales reason
     * @param operatorID (Optional) The user doing this action
     * @returns Returns the reponse from dotcy platform
     */
    abandonBooking(bookingID, noSaleReasonID, operatorID) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.axiosInstance.delete(this.router.getRoute(constants.ENDPOINT_BOOKING_ABANDON_BOOKING, { noSaleReasonID, operatorID }, { bookingID }));
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.abandonBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Cancels the specified booking (opportunity or order). if the booking is an order then also update the no sale reason if one is supplied
     * @param bookingID The ID of the booking to cancel
     * @param noSaleReasonID (Optional) The ID of the no-sales reason
     * @param operatorID (Optional) The user doing this action
     * @param bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @returns Returns the reponse from dotcy platform
     */
    cancelBooking(bookingID, noSaleReasonID, operatorID, bookingRecordType = "Order") {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.axiosInstance.delete(this.router.getRoute(constants.ENDPOINT_BOOKING_CANCEL_BOOKING, { noSaleReasonID, operatorID, bookingRecordType }, { bookingID }));
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.cancelBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Extends the booking's expiration date
     * @param extendExpReq A fully populated request object
     * @param extendExpReq.bookingID The ID of the booking to cancel
     * @param extendExpReq.periodInMinutes (Optional) A period in minutes to extend the expiration of the booking - starting from the current time
     * @param extendExpReq.expirationDate (Optional) A date the booking expiration will be extended to. Date and Time
     * @param extendExpReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param extendExpReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    extendExpiration(extendExpReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID } = extendExpReq, params = __rest(extendExpReq, ["bookingID"]);
                const response = yield this.axiosInstance.patch(this.router.getRoute(constants.ENDPOINT_BOOKING_EXTEND_EXPIRATION, Object.assign({}, params), { bookingID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.extendExpiration()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Remove the specified ticket holder from the booking item
     * @param removeReq A fully populated request object
     * @param removeReq.bookingItemID The ID of the booking to cancel
     * @param removeReq.ticketHolderID The ID of the booking to cancel
     * @param removeReq.bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @param removeReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param removeReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    removeTicketholderFromBookingItem(removeReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingItemID, ticketHolderID, bookingRecordType = "Order" } = removeReq, params = __rest(removeReq, ["bookingItemID", "ticketHolderID", "bookingRecordType"]);
                yield this.axiosInstance.delete(this.router.getRoute(constants.ENDPOINT_BOOKING_REMOVE_TICKETHOLDER, Object.assign({ bookingRecordType }, params), { bookingItemID, ticketHolderID }));
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.removeTicketholderFromBookingItem()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Add a new ticket holder record to the booking item or update an existing one
     * @param addOrUpdateReq A fully populated request object
     * @param addOrUpdateReq.bookingItemID The booking item ID
     * @param addOrUpdateReq.ticketHolder The ticket holder object.
     * @param removeReq.bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @param removeReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param removeReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    addOrUpdateTicketholderFromBookingItem(addOrUpdateReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingItemID, ticketHolder, bookingRecordType = "Order" } = addOrUpdateReq, params = __rest(addOrUpdateReq, ["bookingItemID", "ticketHolder", "bookingRecordType"]);
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_ADD_OR_UPD_TICKETHOLDER, Object.assign({ bookingRecordType }, params), { bookingItemID }), ticketHolder, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.addOrUpdateTicketholderFromBookingItem()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Loads and returns the details of the specified booking using the payment ID
     * @param bookingReq A fully populated request object
     * @param bookingReq.paymentID The ID of the payment to match against the booking
     * @param bookingReq.operatorID (Optional) The operator / user making the request
     * @param bookingReq.partnerID (Optional) The ID of the business partner (request from business portal)
     * @param bookingReq.portalUserID (Optional)The ID of the business portal user
     * @param bookingReq.loadCustomerInfo (Optional) Load additional customer info. Defaults to false
     * @returns Returns the reponse from dotcy platform
     */
    getBookingFromPaymentID(bookingReq) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { paymentID, loadCustomerInfo = false } = bookingReq, params = __rest(bookingReq, ["paymentID", "loadCustomerInfo"]);
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_BOOKING_GET_BY_PAYMENT_ID, Object.assign({ loadCustomerInfo }, params), { paymentID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.getBookingFromPaymentID()", error);
                return {};
            }
        });
    }
    /**
     * Get bookings based on search criteria
     * @param searchParams A fully populated request object
     * @param lastXBookings The last x number of bookings that need to be fetched
     * @returns Returns the reponse from dotcy platform
     */
    searchBookings(bookingReq) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { searchParams, lastXBookings = 100 } = bookingReq;
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_SEARCH_BOOKINGS, { lastXBookings }), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.searchBookings()", error);
                return [];
            }
        });
    }
    /**
     * Loads and returns the tickets purchased by the user
     * @param ticketReq json response
     * @returns Returns the reponse from dotcy platform
     */
    searchTickets(ticketReq) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_SEARCH_TICKETS), ticketReq, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.searchTickets()", error);
                return [];
            }
        });
    }
    /**
     * Fulfill a booking
     * @param bookingReq - A fully populated booking request object
     * @returns Returns the response from dotcy platform
     */
    fulfillBooking(bookingReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID } = bookingReq, params = __rest(bookingReq, ["bookingID"]);
                const response = yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_BOOKING_FULFILL_BOOKING, Object.assign({}, params), { bookingID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.fulfillBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Set a customer for a booking
     * @param bookingId - A fully populated booking request object
     * @returns Returns the response from dotcy platform
     */
    setCustomer(bookingReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID } = bookingReq, params = __rest(bookingReq, ["bookingID"]);
                yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_BOOKING_SET_CUSTOMER, Object.assign({}, params), { bookingID }));
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.setCustomer()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    getBookingByTicketId(ticketID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_BOOKING_GET_BY_TICKET_ID, {}, { ticketID }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.getBookingByTicketId()", error);
                return {};
            }
        });
    }
    /**
     * Sets guest details for a booking
     * @param guestDetails A fully populated guest details object
     * @returns Returns the response from dotcy platform
     */
    saveGuestDetails(guestDetails) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID, GuestDetails, operatorID, bookingType = "Order" } = guestDetails;
                const response = yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_BOOKING_SAVE_GUEST_DETAILS, {
                    operatorID,
                    bookingType,
                }, { bookingID }), GuestDetails, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.saveGuestDetails()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    downloadTicket(ticket) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const bookingID = ticket.bookingID;
                const ticketIDs = ticket.ticketIDs;
                const returnInBased64 = ticket.returnInBased64;
                const returnOnlyAvailableTickets = ticket.returnOnlyAvailableTickets;
                const limitHandling = ticket.limitHandling;
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_BOOKING_DOWNLOAD_TICKET, { ticketIDs, returnInBased64, returnOnlyAvailableTickets, limitHandling }, { bookingID }, "none"));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.downloadTicket()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    getElectronicTicket(ticketID, versionNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_BOOKING_ELECTRONIC_TICKET, {}, { ticketID, versionNumber }));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.getElectronicTicket()", error);
                return "";
            }
        });
    }
    /**
     * Split booking
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    splitBooking(bookingReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID, request } = bookingReq, params = __rest(bookingReq, ["bookingID", "request"]);
                const response = yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_BOOKING_SPLIT, Object.assign({}, params), { bookingID }), request, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.splitBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * merge booking
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    mergeBooking(bookingReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bookingID, request } = bookingReq, params = __rest(bookingReq, ["bookingID", "request"]);
                yield this.axiosInstance.put(this.router.getRoute(constants.ENDPOINT_BOOKING_MERGE, Object.assign({}, params), { bookingID }), request, constants.JSON_HEADER);
                return true;
            }
            catch (error) {
                Logger_1.default.error("Bookings.mergeBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    /**
     * Validate booking for all the mandatory fields
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    validateBooking(bookingReq) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_BOOKING_VALIDATION), bookingReq, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Bookings.validateBooking()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
}
exports.default = Bookings;
//# sourceMappingURL=Bookings.js.map