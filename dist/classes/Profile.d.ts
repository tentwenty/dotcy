import IConfig from "../interfaces/IConfig";
import IProfile from "../interfaces/IProfile";
import { IRespProfile, IRespUpdateProfile, IRespTicketAddress } from "../interfaces/responses/profile";
import { IReqSearchProfile, IReqUpdateProfile, IReqTicketAddress, IReqDownloadData, IReqDeleteProfile, IReqRevokeProfile } from "../interfaces/requests/profile";
export default class Profile implements IProfile {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /** Gets Profile Based on the Search Params
     * @param searchParams JSON Array
     * @returns Returns User Object Array
     */
    searchProfiles(searchParams: IReqSearchProfile): Promise<IRespProfile[]>;
    /** Gets Profile Based on the Search Params
     * @param profileParam JSON Array
     * @returns Returns User Object
     */
    updateProfile(profileParam: IReqUpdateProfile): Promise<IRespUpdateProfile>;
    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    getProfileById(profileId: string): Promise<IRespProfile>;
    /** Gets Profile Based on the Search Params
     * @param profileParam JSON Array
     * @returns Returns User Object
     */
    addOrUpdateTicketDeliveryAddress(ticketParam: IReqTicketAddress): Promise<IRespTicketAddress>;
    /** Gets Download Data Based on the Search Params
     * @param profileParam JSON Array
     * @returns string
     */
    downloadData(data: IReqDownloadData): Promise<string>;
    /** Initiate profile delete
     * @param profileParam JSON Array
     * @returns Returns string
     */
    deleteProfile(data: IReqDeleteProfile): Promise<string>;
    /** Revokes profile delete request
     * @param profileParam JSON Array
     * @returns string
     */
    revokeDeleteProfile(data: IReqRevokeProfile): Promise<string>;
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    getPersonalInterest(profileID: string): Promise<string[]>;
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    updatePersonalInterest(profileID: string, associatePersonalInterests: string[], disassociatePersonalInterests: string[]): Promise<boolean>;
}
