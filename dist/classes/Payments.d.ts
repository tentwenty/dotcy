import IConfig from "../interfaces/IConfig";
import IPayments from "../interfaces/IPayments";
import { ICaching } from "../interfaces/ITypes";
import { IRespPayConfig, IRespPayMethod, IRespAddPayment } from "../interfaces/responses/payments";
import { IReqAddPayment, IReqCancelPayment, IReqUpdateStatus } from "../interfaces/requests/payments";
export default class Payments implements IPayments {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Fetches all the product variants
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getPaymentMethods(caching?: ICaching, operatorID?: string): Promise<IRespPayMethod[]>;
    /**
     * Fetch the payment related information
     * @param methodType The payment type
     * @param methodID The payment method id
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getPaymentConfig({ methodType, methodID, caching, }: {
        methodType: string;
        methodID: string;
        caching?: ICaching;
    }): Promise<IRespPayConfig>;
    /**
     * Post Payment against a booking
     * @param paymentObj.operatorID Fully populated payment object
     * @param paymentObj.autoApprove Auto approve payment
     * @param paymentObj.revalidateBookingOnPayment Force re-validation of BookingRules on each payment issued using the API
     * @param paymentObj.payment Fully populated payment object
     * @param paymentObj.payment.Amount Amount being payed for
     * @param paymentObj.payment.ChannelID Channel from where the payment has been initiated
     * @param paymentObj.payment.CurrencyID Currency id of the payment
     * @param paymentObj.payment.ForceCloseBookingOnFullPayment Close booking once the full amount is payed
     * @param paymentObj.payment.MethodOfPaymentID Payment method ID
     * @param paymentObj.payment.PaymentCategory Payment category
     * @param paymentObj.payment.PaymentStatus Status of the payment. Values can be "Submitted" | "Approved"
     * @param paymentObj.payment.DiscountCoupon (Optional) Discount coupon data object
     * @param paymentObj.payment.DiscountVoucher (Optional) Discount Voucher data object
     * @param paymentObj.payment.NotesAndComments (Optional) Any additional notes
     * @returns return the ID string of the payment
     */
    addPayment(paymentObj: IReqAddPayment): Promise<IRespAddPayment>;
    /**
     * Change Payment Status and update bank response
     * @param paymentID Payment ID that needs to be updated
     * @param paymentStatus Payment status string
     * @param bankResponse Bank response object
     * @param operatorID (Optional) Operator ID
     * @returns Returns boolean value indicating the completion of task
     */
    updatePaymentStatus(updateReq: IReqUpdateStatus): Promise<boolean>;
    /** Gets Wallet Balance Based on the Customer ID
     * @param customerID string
     * @returns Returns number
     */
    getWalletBalance(customerID: string): Promise<number>;
    /** Gets Wallet Balance Based on the Customer ID
     * @param paymentReq A fully populated cancel payment request object
     * @param paymentReq.paymentID The ID of the payment to cancel
     * @param paymentReq.terminalID The Terminal from where the cancellation was requested (if applicable)
     * @param paymentReq.operatorID The Operator/User that requested the cancellation (if applicable)
     * @param paymentReq.sessionID The Terminal Session for which the cancellation is issued from
     * @returns Returns boolean value indicating the completion of task
     */
    cancelPayment(paymentReq: IReqCancelPayment): Promise<boolean>;
}
