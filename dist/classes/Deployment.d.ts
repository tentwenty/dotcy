import IConfig from "../interfaces/IConfig";
import { ICaching } from "../interfaces/ITypes";
import { IRespChannelInfo } from "../interfaces/responses/deployment";
import IDeployment from "../interfaces/IDeployment";
export default class Deployment implements IDeployment {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Get Channel Info
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    getChannelInfo(caching?: ICaching): Promise<IRespChannelInfo>;
}
