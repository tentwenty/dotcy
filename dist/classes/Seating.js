"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Error_1 = __importDefault(require("../helpers/Error"));
const Router_1 = __importDefault(require("../helpers/Router"));
const Axios_1 = __importDefault(require("../helpers/Axios"));
const constants = __importStar(require("../constants"));
const Logger_1 = __importDefault(require("../helpers/Logger"));
class Seating {
    constructor(options) {
        this.axiosInstance = new Axios_1.default(options).axiosInstance;
        this.router = new Router_1.default(options);
    }
    searchSeatingAreas(searchParams, caching = "Default") {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_SEATING_SEARCH_SEATING_AREAS, { caching }), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Seating.searchSeatingAreas()", error);
                return [];
            }
        });
    }
    searchSeats(searchParams) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_SEATING_SEARCH_SEATS), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Seating.searchSeats()", error);
                return [];
            }
        });
    }
    getSeatingSection(seatingSectionID) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.get(this.router.getRoute(constants.ENDPOINT_SEATING_GET_SEATING_SECTION, {}, { seatingSectionID }, "none"));
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Seating.getSeatingSection()", error);
                throw new Error_1.default((_a = error === null || error === void 0 ? void 0 : error.response) === null || _a === void 0 ? void 0 : _a.data, error.response.status);
            }
        });
    }
    searchSeatingSections(searchParams) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield this.axiosInstance.post(this.router.getRoute(constants.ENDPOINT_SEATING_SEARCH_SEATING_SECTIONS), searchParams, constants.JSON_HEADER);
                return response.data;
            }
            catch (error) {
                Logger_1.default.error("Seating.searchSeatingSections()", error);
                return [];
            }
        });
    }
}
exports.default = Seating;
//# sourceMappingURL=Seating.js.map