import IConfig from "../interfaces/IConfig";
import IWebsite from "../interfaces/IWebsite";
import { IReqLoginExt } from "../interfaces/requests/website";
import { IRespLoginExt } from "../interfaces/responses/website";
export default class Website implements IWebsite {
    private axiosInstance;
    private router;
    constructor(options: IConfig);
    /**
     * Use this method to generate OTP
     * @param otpReq The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    loginExtAuthMethod(data: IReqLoginExt): Promise<IRespLoginExt>;
}
