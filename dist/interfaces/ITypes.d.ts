export declare type ICaching = "Default" | "DisableCaching" | "ServerSideOnly" | "ClientSideOnly";
export declare type IArrayFormatter = "none" | "comma" | "bracket" | "index";
export declare type IPathParams = {
    [key: string]: string;
};
export declare type IRouteParams = {
    [key: string]: string | number | boolean | string[];
};
export declare type ITaxLineItems = {
    LineItemID: string;
    ProductID: string;
    AmountBeforeTax: number;
    TaxProfileID: string;
};
export declare type IRespCalculatedTax = {
    [key: string]: number;
};
export declare type IBookingType = "Order" | "Opportunity" | "Both";
export declare type IEntryTimeSlot = {
    Start: string;
    End: string;
};
export declare type IPriority = "Low" | "Normal" | "High";
export declare type ITagScope = "None" | "Case" | "Product" | "Contact" | "Account" | "Booking";
