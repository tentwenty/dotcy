import { ICaching } from "./ITypes";
import { IRespMemType, IRespMemCustInfo, IRespMemTransitionType, IRespGetLinkedMem, IRespBenefitConsumption, IRespMemSendInviteMember, IRespMemCalculateMemAddOnPrice, IRespMemValidateInvite, IRespMemInvitationUpdate } from "./responses/membership";
import { IReqMemCustInfo, IReqMemAddLinkedMember, IReqMemInviteMember, IReqMemCalculateMemAddOnPrice, IReqMemInvitationUpdate } from "./requests/membership";
export default interface IMembership {
    getMembershipTypes(caching: ICaching): Promise<IRespMemType[]>;
    getSingleCustomerMembershipInfo(profileId: string): Promise<IRespMemCustInfo[]>;
    getCustomerMembershipInfo(reqMem: IReqMemCustInfo): Promise<IRespMemCustInfo[]>;
    getMembershipTransitionTypes(caching: ICaching): Promise<IRespMemTransitionType[]>;
    getLinkedMembership(profileId: string, caching: ICaching): Promise<IRespGetLinkedMem[]>;
    sendMemberRegistrationInvite(request: IReqMemInviteMember, caching: ICaching): Promise<IRespMemSendInviteMember>;
    getBenefitConsumption(profileId: string, caching: ICaching): Promise<IRespBenefitConsumption[]>;
    addLinkedMembers(request: IReqMemAddLinkedMember, caching: ICaching): Promise<string>;
    calculateMembershipAddOnPrice(request: IReqMemCalculateMemAddOnPrice, caching: ICaching): Promise<IRespMemCalculateMemAddOnPrice>;
    validateInvitation(requestID: string, userIPAddress: string): Promise<IRespMemValidateInvite>;
    memberInvitationUpdate(request: IReqMemInvitationUpdate, userIPAddress: string): Promise<IRespMemInvitationUpdate>;
    calculateMembershipUpgradePrice(request: IReqMemCalculateMemAddOnPrice, caching: ICaching): Promise<IRespMemCalculateMemAddOnPrice>;
}
