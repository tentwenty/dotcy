import { ICaching } from "./ITypes";
import { IRespChannelInfo } from "./responses/deployment";
export default interface IDeployment {
    getChannelInfo(caching: ICaching): Promise<IRespChannelInfo>;
}
