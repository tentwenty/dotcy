import { IRespEvent, IRespEventSetup } from "./responses/event";
export default interface IEvents {
    getEventById(eventId: string): Promise<IRespEvent>;
    getEventSetUp(): Promise<IRespEventSetup[]>;
}
