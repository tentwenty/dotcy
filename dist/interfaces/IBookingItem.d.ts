interface IEntryTimeSlots {
    Start: string;
    End: string;
}
interface ITicketHolder {
    CustomerID: string;
    CustomerType: string;
    NumberOfTickets: number;
    Email?: string;
    FirstName?: string;
    LastName?: string;
    CreateProfile?: boolean;
    Gender?: "Male" | "Female";
    DateOfBirth?: string;
    TicketHolderMessage?: string;
    MemberTypeID?: string;
}
interface ISection {
    ID: string;
    Name?: string;
}
interface ISeat {
    ID: string;
    SeatNumber: string;
    Section: ISection;
    DoorID?: string;
}
interface IChildItems {
    BaseProductId?: string;
    ProductVariantId?: string;
    Quantity?: number;
    PackageProductOptionId?: string;
    EntryTimeSlots?: IEntryTimeSlots;
    ActivitySessionID?: string;
}
interface IOptions {
    [key: string]: string;
}
export default interface IBookingItem {
    ID?: string;
    InternalItemID?: string;
    BaseProductID: string;
    ProductVariantID: string;
    Quantity: number;
    EntryTimeSlots: IEntryTimeSlots[];
    TicketHolders?: ITicketHolder[];
    Seats?: ISeat[];
    childItems?: IChildItems[];
    TimeBlockSchemeID?: string;
    TimeBlockSlotUniqueKey?: string;
    EventSessionID?: string;
    EventSessionProductID?: string;
    EventSessionSlots?: string[];
    ActivitySessionId?: string;
    MembershipID?: string;
    ParentMembershipBookingItemID?: string;
    TransitionedMembershipID?: string;
    SelectedOptions?: IOptions;
}
export {};
