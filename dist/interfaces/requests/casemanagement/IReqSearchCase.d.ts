export default interface IReqSearchCase {
    ID?: string;
    CaseView?: string;
    PartnerID?: string;
    PortalUserID?: string;
    CaseTypeID?: string;
    CustomerID: string;
}
