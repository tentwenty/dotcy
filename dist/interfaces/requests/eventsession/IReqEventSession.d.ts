import { ICaching } from "../../ITypes";
export default interface IReqEventSession {
    EventSessionID?: string;
    EventIDs?: string[];
    OnlyPublishedSessions?: boolean;
    IncludeInactiveRecords?: boolean;
    EventCategories?: string[];
    TargetGroups?: string[];
    ProductIDs?: string[];
    caching?: ICaching;
}
