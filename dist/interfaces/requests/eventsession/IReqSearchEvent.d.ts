export default interface IReqSearchEvent {
    EventIDs?: string[];
    OwnerID?: string;
    IncludeInactiveRecords?: boolean;
    OrganizerSSOName?: string;
    EventExtRefNumber?: string;
    EventRefNumber?: string;
    ProductIDs?: string[];
}
