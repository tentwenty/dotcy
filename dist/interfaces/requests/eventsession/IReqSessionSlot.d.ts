import { ICaching } from "../../ITypes";
export default interface IReqSessionSlot {
    EventSessionID?: string;
    SessionSlotID?: string;
    SlotDate?: string;
    IncludeInactiveRecords?: boolean;
    ExcludePastEventSlots?: boolean;
    OnlyPublishedEventSessions?: boolean;
    EventIDs?: string[];
    TargetGroups?: string[];
    ProductIDs?: string[];
    ProductVariantIDs?: string[];
    caching?: ICaching;
}
