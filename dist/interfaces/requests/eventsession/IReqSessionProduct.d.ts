import { ICaching } from "../../ITypes";
export default interface IReqSessionProduct {
    EventSessionProductID?: string;
    EventSessionID?: string;
    ProductIDs?: string[];
    EventID?: string;
    IncludeInactiveRecords?: boolean;
    OnlyPublishedEventSessions?: boolean;
    EventSessionSlots?: string[];
    ProductVariants?: string[];
    caching?: ICaching;
}
