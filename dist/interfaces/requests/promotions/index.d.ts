import IReqSearchCoupon from "./IReqSearchCoupon";
import IReqValidateCoupon from "./IReqValidateCoupon";
import IReqSearchVouchers from "./IReqSearchVouchers";
import IReqValidateVoucher from "./IReqValidateVoucher";
export { IReqSearchCoupon, IReqValidateCoupon, IReqSearchVouchers, IReqValidateVoucher };
