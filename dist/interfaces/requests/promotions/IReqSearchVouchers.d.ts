export default interface IReqSearchVouchers {
    PaymentMethodID: string;
    ReturnInactiveCoupons?: boolean;
    SearchString?: string;
    OnlySpecificVoucherIDs?: string[];
    IncludePrerequisitesAndOffers?: boolean;
}
