export default interface IReqSearchCoupon {
    PaymentMethodID: string;
    ReturnInactiveCoupons?: boolean;
    SearchString: string;
    OnlySpecificCouponsIDs?: string[];
    IncludePrerequisitesAndOffers?: boolean;
}
