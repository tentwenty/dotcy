export interface IBankResponse {
    BankName: string;
    Resultcode: string;
    Errormessage: string;
    ReceiptNumber: string;
    ProviderResponse: string;
    PaymentMethod: string;
    VoucherID: string;
    DccOpted: string;
    DccExchangeRate: number;
    DccCurrency: string;
    DccMarkup: number;
    DccPaymentAmount: number;
    CardPaymentReceiptNumber: string;
    CardPaymentBatchNumber: string;
    PDQTransactionDate: string;
    CardNumber: string;
    POSVersion: string;
    CardPaymentAmount: number;
    PDQCryptogramInfoData: string;
    CardType: string;
    TerminalVerification: string;
    ApplicationID: string;
    PDQApplicationCryptogram: string;
    TransactionStatus: string;
    MerchantId: string;
    PaymentDeviceTerminalID: string;
    ApprovalCode: string;
}
export default interface IReqUpdateStatus {
    paymentID: string;
    paymentStatus: "None" | "Submitted" | "Approved" | "Cancelled" | "Rejected" | "Inactive" | "Draft" | "Amended" | "Refunded" | "Cancellation";
    bankResponse?: IBankResponse;
    operatorID?: string;
}
