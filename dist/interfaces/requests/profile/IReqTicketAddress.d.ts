interface ICustomerColumns {
    LogicalName: string;
    ID: string;
    Name: string;
}
interface ICountryColumns {
    ID: string;
    Name: string;
}
export default interface IReqTicketAddress {
    ID: string;
    Name: string;
    Customer: ICustomerColumns;
    Stree1?: string;
    Stree2?: string;
    Stree3?: string;
    CityOtTown?: string;
    ZipOrPostalCode?: string;
    Country: ICountryColumns;
    POBox?: string;
}
export {};
