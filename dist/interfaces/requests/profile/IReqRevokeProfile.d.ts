export default interface IReqRevokeProfile {
    profileID: string;
    userIPAddress: string;
    profileType?: string;
    reason?: string;
}
