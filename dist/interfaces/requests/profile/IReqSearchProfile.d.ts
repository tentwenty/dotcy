interface ISelectColumns {
    Contact?: string[];
}
export default interface IReqSearchProfile {
    CustomerID?: string;
    CustomerName?: string;
    CustomerType?: string;
    ParentAccountID?: string;
    SchoolCategory?: string;
    Email?: string;
    IncludeInactiveRecords?: boolean;
    PartnerType?: string;
    SelectColumns?: ISelectColumns;
}
export {};
