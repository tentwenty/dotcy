export default interface IReqDeleteProfile {
    profileID: string;
    userIPAddress: string;
    profileType?: string;
    reason?: string;
    operatorID?: string;
}
