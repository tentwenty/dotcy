export default interface IReqLoginExt {
    extAuthMethodID: string;
    userNameOrCode: string;
    userIPAddress: string;
}
