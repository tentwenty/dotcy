export default interface IReqGetBookingFromPayment {
    paymentID: string;
    operatorID?: string;
    partnerID?: string;
    portalUserID?: string;
    loadCustomerInfo?: boolean;
}
