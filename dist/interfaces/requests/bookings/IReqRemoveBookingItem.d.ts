import { IBookingType } from "../../ITypes";
export default interface IReqRemoveBookingItem {
    bookingID: string;
    bookingItemID: string;
    bookingType?: IBookingType;
    terminalID?: string;
    operatorID?: string;
}
