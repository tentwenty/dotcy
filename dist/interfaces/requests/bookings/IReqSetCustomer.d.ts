export default interface IReqSetCustomer {
    bookingID: string;
    customerID: string;
    customerType?: string;
    bookingType?: string;
    terminalID?: string;
    operatorID?: string;
}
