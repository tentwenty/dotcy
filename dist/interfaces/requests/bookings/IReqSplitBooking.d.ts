interface IRequest {
    BookingItemIDs: string[];
}
export default interface IReqSplitBooking {
    bookingID: string;
    request: IRequest;
    operatorID?: string;
    terminalID?: string;
    userIPAddress?: string;
    languageID?: string;
}
export {};
