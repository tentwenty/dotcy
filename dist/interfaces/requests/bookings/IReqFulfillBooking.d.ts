export default interface IReqFulfillBooking {
    bookingID: string;
    sessionID?: string;
    terminalID?: string;
    requestedByUserID?: string;
}
