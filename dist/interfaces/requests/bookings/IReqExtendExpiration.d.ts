export default interface IReqExtendExpiration {
    bookingID: string;
    periodInMinutes?: string;
    expirationDate?: string;
    operatorID?: string;
    terminalID?: string;
}
