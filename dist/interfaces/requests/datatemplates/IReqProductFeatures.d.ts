export default interface IRespProductFeatures {
    ProductHierarchyID: string;
    ProductIDs?: string[];
    CacheKey?: string;
}
