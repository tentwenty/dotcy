export default interface IReqGetProductCapacity {
    ProductID?: string;
    NumberOfDaysInTheFuture?: number;
    StartDate?: string;
    EndDate?: string;
    ProductHierarchyID?: string;
    ProductGroupIDs?: string[];
    LoadType?: string;
    LastReceivedVersionNo?: number;
    ReturnNonAvailable?: boolean;
}
