export default interface IReqGenerateOTP {
    profileID: string;
    OTPTypeCode: string;
    profileType?: string;
    userIPAddress?: string;
    operatorID?: string;
    languageID?: string;
    terminalID?: string;
    emailAddress?: string;
}
