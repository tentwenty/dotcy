import { ITagScope } from "../../ITypes";
export default interface IReqTagsForRecords {
    Scope: ITagScope;
    ProductHierarchyID: string;
    RecordIDs?: string[];
}
