export default interface IReqMemInvitationUpdate {
    RequestID?: string;
    Reason?: string;
    ProfileID?: string;
    Action?: string;
}
