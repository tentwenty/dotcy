interface IInviteeProfile {
    ID: string;
    CustomerType: string;
    Name: string;
}
export default interface IReqMemInviteMember {
    ParentMembershipID: string;
    InviteeProfile: IInviteeProfile;
    InviteeEmail: string;
}
export {};
