interface ILinkedProfile {
    ID: string;
    CustomerType: string;
    Name: string;
}
export default interface IReqMemAddLinkedMember {
    PrimaryMembershipID: string;
    LinkedProfile: ILinkedProfile;
}
export {};
