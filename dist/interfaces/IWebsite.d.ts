import { IReqLoginExt } from "./requests/website";
import { IRespLoginExt } from "./responses/website";
export default interface IWebsite {
    loginExtAuthMethod(data: IReqLoginExt): Promise<IRespLoginExt>;
}
