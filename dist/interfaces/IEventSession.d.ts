import { IRespEventSession, IRespSessionSlot, IRespRemainingCapacity, IRespSessionProduct } from "./responses/eventsession";
import { IReqEventSession, IReqSessionSlot, IReqSessionProduct } from "./requests/eventsession";
export default interface IEventSession {
    getEventSessions(sessionParams: IReqEventSession): Promise<IRespEventSession[]>;
    getSessionsSlots(sessionParams: IReqSessionSlot): Promise<IRespSessionSlot[]>;
    getRemainingCapacity(eventID: string): Promise<IRespRemainingCapacity>;
    getSessionProducts(sessionParams: IReqSessionProduct): Promise<IRespSessionProduct[]>;
}
