import ITranslations from "../../ITranslations";
export default interface IRespTags {
    ParentTagIDs: string[];
    ID: string;
    Name: string;
    Translations?: ITranslations;
}
