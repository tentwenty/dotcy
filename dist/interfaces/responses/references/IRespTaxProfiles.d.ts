interface ITaxRange {
    ID: number;
    FromAmount: number;
    ToAmount: number;
    Type: number;
    TaxPercentageAmount: number;
}
interface ITaxConfiguration {
    ID: string;
    TaxType: number;
    ApplyOn: number;
    TaxCalculationType: number;
    CalculationOrder: number;
    TaxRanges: ITaxRange[];
}
export default interface IRespStatesIRespTaxProfiles {
    ID: string;
    TaxConfigurations: ITaxConfiguration[];
}
export {};
