interface IDonimination {
    Name: string;
    Amount: number;
    UseCurrencyFormat: boolean;
}
export default interface IRespChCurrencies {
    ID: string;
    Name: string;
    IsBaseCurrency: boolean;
    ExchangeRate: number;
    Precision: number;
    Symbol: string;
    Code: string;
    Denominations: IDonimination[];
}
export {};
