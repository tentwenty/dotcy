export default interface IRespAgeRange {
    ID: string;
    Name: string;
}
