import ITranslations from "../../ITranslations";
export default interface IRespStates {
    Translations?: ITranslations;
    ID: string;
    Name: string;
    Code: string;
    CountryID: string;
}
