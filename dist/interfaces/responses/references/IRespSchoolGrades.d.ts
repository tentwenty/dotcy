import ITranslations from "../../ITranslations";
export default interface IRespSchoolGrades {
    Translations?: ITranslations;
    ID: string;
    Name: string;
    SchoolCategory: string;
}
