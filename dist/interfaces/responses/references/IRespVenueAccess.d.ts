import ITranslations from "src/interfaces/ITranslations";
export default interface IRespVenueAccess {
    ID: string;
    ParentAreaID?: string;
    Name: string;
    Translations?: ITranslations;
    IsOnline?: boolean;
}
