export default interface IRespGetTags {
    ID: string;
    Name: string;
    CategoryID: string;
    Scope: number;
}
