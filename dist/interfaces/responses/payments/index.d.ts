import IRespPayMethod from "./IRespPayMethod";
import IRespPayConfig from "./IRespPayConfig";
import IRespAddPayment from "./IRespAddPayment";
export { IRespPayMethod, IRespPayConfig, IRespAddPayment };
