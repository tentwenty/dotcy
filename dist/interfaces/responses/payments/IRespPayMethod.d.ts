import ITranslations from "../../ITranslations";
export default interface IRespPayMethod {
    ID: string;
    Name: string;
    PaymentType: number;
    ShortName: string;
    PluginClassType: string;
    AllowAnonymous: boolean;
    Translations: ITranslations;
    SupportedCurrencies: string[];
    CanRefundToAdvancePayment: boolean;
}
