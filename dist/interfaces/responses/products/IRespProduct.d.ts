import IProductVariant from "../../IProductVariant";
interface IVenue {
    ID: string;
    Name: string;
}
interface IPriceList {
    ID: string;
    Name: string;
}
export default interface IProduct {
    ID: string;
    Name: string;
    ProductType: number;
    Code: string;
    ProductStructure: number;
    AnonymousMode: boolean;
    IndividualTaxProfileID: string;
    CorporateTaxProfileID: string;
    GenerateTicket: boolean;
    TicketType: number;
    ActivityPricingType: number;
    VenueTimeZoneCode: number;
    VenueID: IVenue;
    PriceLists: IPriceList[];
    ProductVariants: IProductVariant[];
    VariantOptionsDisplay: number;
    SellableByMaxDaysInFuture: number;
    ValidityPeriodInDays?: number;
    ForceBookingSetting: number;
    AllowMultipleMemberships: number;
    Languages: string[];
    TargetGroups?: string[];
    AccessAreas?: string[];
    MembershipTypeID?: string;
    Event?: string;
    IsSeated?: boolean;
    ValidFrom?: string;
    ValidTo?: string;
    UseTimeBlocks?: boolean;
    MembershipMaxMembers?: number;
    MembershipMinMembers?: number;
    ActivityType?: number;
    AssignDateOnActivation?: boolean;
    WaitingList?: number;
}
export {};
