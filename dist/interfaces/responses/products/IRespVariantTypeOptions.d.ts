import ITranslations from "../../ITranslations";
interface IOpt {
    VariantTypeID?: string;
    ID?: string;
    Name?: string;
}
interface IOptions {
    [key: string]: IOpt[];
}
interface IVariantOption {
    Options: IOptions[];
    Translations: ITranslations;
    ID: string;
    Name: string;
}
export default interface IRespVariantTypeOptions {
    [key: string]: IVariantOption[];
}
export {};
