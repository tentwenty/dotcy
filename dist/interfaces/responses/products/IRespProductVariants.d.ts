import IProductVariant from "../../IProductVariant";
export default interface IRespProductVariants {
    [key: string]: IProductVariant[];
}
