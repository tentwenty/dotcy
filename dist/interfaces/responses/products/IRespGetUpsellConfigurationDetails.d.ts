export default interface IRespGetUpsellConfigurationDetails {
    ID: string;
    Name: string;
    QuantityType: string;
    SaleRelationshipType: string;
    Direction: string;
    ValidTo: string;
    ChannelPermissionType: string;
    ProductUpsellRelationshipIDs: string[];
}
