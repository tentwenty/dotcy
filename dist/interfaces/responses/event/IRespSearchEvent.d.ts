interface ILookupItem {
    ID?: string;
    Name?: string;
}
export default interface IRespSearchEvent {
    Category?: ILookupItem;
    Organizer?: ILookupItem;
    ExternalRef?: string;
    EventIDNumber?: string;
    Status?: number;
    OverrideMaxCapacity?: boolean;
    ServesAlcohol?: boolean;
    Public?: boolean;
    IsGovernment?: boolean;
    ProductsForSale?: boolean;
    PayForTicket?: boolean;
    HasTickets?: boolean;
    SetupComplete?: boolean;
    ParentEvent?: ILookupItem;
    SsoUsername?: string;
    Description?: string;
    DateSelected?: string;
    ValidFrom?: string;
    ValidTo?: string;
    ToBePromoted?: boolean;
    HasSecurityPassportCheck?: boolean;
    HasSecurityPhotoBadge?: boolean;
    HasOptionalAccommodation?: boolean;
    OverrideTimeZoneID?: string;
    ParticipationType?: number;
    ID?: string;
    Name?: string;
}
export {};
