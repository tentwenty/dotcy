export default interface IRespEvent {
    EventIDNumber?: string;
    Status?: string;
    ValidFrom?: string;
    ValidTo?: string;
    ParticipationType?: string;
    ID?: string;
    Name?: string;
}
