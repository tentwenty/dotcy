export default interface IRespVouchers {
    Status: number;
    Code: string;
    DiscountType: number;
    ValidFrom: string;
    ValidTo: string;
    ID: string;
    FreeProductValidWeekDays: string[];
    FreeProductVariantNameID: {
        ID: string;
        Name: string;
    };
    OfferName: string;
    FreeProductID: string;
    FreeProductQty: string;
    FreeProdFromDateTime: string;
    FreeProdToDateTime: string;
}
