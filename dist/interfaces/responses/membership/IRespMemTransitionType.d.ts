export default interface IRespMemTransitionType {
    SourceMembershipTypeID?: string;
    TargetMembershipTypeID?: string;
    TransitionType?: string;
}
