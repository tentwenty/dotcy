export default interface IRespMemSendInvite {
    IsSuccess: boolean;
    RequestID: string;
    ExpirationDate: string;
    ErrorCode: number;
    ErrorMessage: string;
}
