interface IResp {
    EvenSessionID: string;
}
export default interface IRespRemainingCapacity {
    SessionCapacityList?: IResp[];
}
export {};
