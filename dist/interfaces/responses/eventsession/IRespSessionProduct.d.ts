export default interface IRespSessionProduct {
    EventSessionID?: string;
    ProductID?: string;
    DateSelection?: string;
    EntryStartDate?: string;
    EntryEndDate?: string;
    MaxSlots?: number;
    MinSlots?: number;
    SlotSelectionOption?: string;
    TicketPeriod?: number;
    ForceDateSelectionInEventPeriod?: boolean;
    Variants?: string[];
    IncludedSlots?: string[];
    AllowedCheckInTimeAfterTicketEnd?: number;
    ID: string;
    Name?: string;
}
