import ITranslations from "../../ITranslations";
interface IValue {
    ProductID: string;
    ID: string;
    Caption: string;
    AttributeType: number;
    Code: string;
    ValuePickList?: {
        ID: string;
        Name: string;
        Translations?: ITranslations;
    };
    ValueList?: IValueList[];
    ValueBool?: boolean;
}
interface IValueList {
    ID: string;
    Name: string;
    Translations?: ITranslations;
}
export default interface IRespProductFeatures {
    Values: {
        [key: string]: IValue[];
    };
}
export {};
