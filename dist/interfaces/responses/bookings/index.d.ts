import IRespSaveBooking from "./IRespSaveBooking";
import IRespBookingByID from "./IRespBookingByID";
import IRespSearchTicket from "./IRespSearchTicket";
import IRespBookingByTicketID from "./IRespBookingByTicketID";
import IRespSplitBooking from "./IRespSplitBooking";
import IRespAddOrUpdateBooking from "./IRespAddOrUpdateBooking";
import IRespValidateBooking from "./IRespValidateBooking";
import IRespAddOrUpdateTicketholder from "./IRespAddOrUpdateTicketholder";
export { IRespSaveBooking, IRespBookingByID, IRespSearchTicket, IRespBookingByTicketID, IRespSplitBooking, IRespAddOrUpdateBooking, IRespValidateBooking, IRespAddOrUpdateTicketholder, };
