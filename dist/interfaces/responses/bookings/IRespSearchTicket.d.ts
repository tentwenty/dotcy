interface ITicketHolder {
    ID?: string;
    Name?: string;
}
export default interface IRespSearchTicket {
    TicketID?: string;
    BookingID?: string;
    BookingRef?: string;
    TicketNumber?: string;
    TicketHolder?: ITicketHolder;
    TicketPrice?: number;
    TicketPriceBookingCurrency?: number;
    SeatNumber?: string;
    VersionNumber?: number;
    BaseProductID?: string;
    ProductVariantID?: string;
    StartDate?: string;
    EndDate?: string;
    IndividualsNo?: number;
    SequenceNo?: string;
    Section?: string;
    SectionID?: string;
    TimesPrinted?: number;
    LastPrintedOn?: string;
    LastValidTicketScan?: string;
    IsAdditionalGuest?: boolean;
    BaseProductName?: string;
    ProductVariantName?: string;
    StatusCode?: string;
    StateCode?: string;
    VisitorCategoryID?: string;
    VisitorCategoryName?: string;
    NumberOfTicketChanges?: number;
    ValidFrom?: string;
    ValidUntil?: string;
    BookingItemID?: string;
    TicketGroupID?: string;
    EventSessionID?: string;
    EventSessionName?: string;
    EventSessionProductID?: string;
    EventSessionProductName?: string;
    MembershipID?: string;
    MembershipNumber?: string;
    TimeBlockSchemeID?: string;
    TimeBlockSlotUniqueKey?: string;
    TicketBarcode?: string;
    CreatedOn?: string;
}
export {};
