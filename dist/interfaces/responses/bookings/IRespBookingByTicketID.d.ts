interface ITicketHolder {
    ID?: string;
    Name?: string;
}
export default interface IRespBookingByTicketID {
    TicketID?: string;
    BookingID?: string;
    BookingRef?: string;
    TicketNumber?: string;
    TicketHolder?: ITicketHolder;
    TicketPrice?: number;
    TicketPriceBookingCurrency?: number;
    VersionNumber?: number;
    BaseProductID?: string;
    ProductVariantID?: string;
    StartDate?: string;
    EndDate?: string;
    SequenceNo?: string;
    TimesPrinted?: number;
    LastPrintedOn?: string;
    BaseProductName?: string;
    ProductVariantName?: string;
    StatusCode?: number;
    BookingItemID?: string;
    TicketGroupID?: string;
}
export {};
