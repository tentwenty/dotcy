interface IProfile {
    ID: string;
    Name: string;
    PartyType: number;
}
export default interface IRespBulkNotification {
    Profiles?: {
        contact: string[];
    };
    QuickCommunicationID?: string;
    ID?: string;
    Subject?: string;
    To?: IProfile[];
    CC?: IProfile[];
    BCC?: IProfile[];
    ActivityType?: number;
    BodyContent?: string;
    Metadata?: string;
    IsBulk?: boolean;
    CreatedOn?: string;
    ModifiedOn?: string;
}
export {};
