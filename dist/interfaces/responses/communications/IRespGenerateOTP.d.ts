export default interface IRespGenerateOTP {
    ProfileID?: string;
    EmailAddress?: string;
    MobilePhone?: string;
    CodeExpiresOn?: string;
    Code?: string;
    WebProfileAuditID?: string;
}
