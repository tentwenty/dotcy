import IRespGenerateOTP from "./IRespGenerateOTP";
import IRespVerifyOTP from "./IRespVerifyOTP";
import IRespSendActivity from "./IRespSendActivity";
import IRespCustomNotifications from "./IRespCustomNotifications";
import IRespSendTicketsToTicketholder from "./IRespSendTicketsToTicketholder";
import IRespBulkNotification from "./IRespBulkNotification";
export { IRespGenerateOTP, IRespVerifyOTP, IRespSendActivity, IRespCustomNotifications, IRespSendTicketsToTicketholder, IRespBulkNotification, };
