import IRespProfile from "./IRespProfile";
import IRespUpdateProfile from "./IRespUpdateProfile";
import IRespTicketAddress from "./IRespTicketAddress";
export { IRespProfile, IRespUpdateProfile, IRespTicketAddress };
