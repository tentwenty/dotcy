interface IValesType {
    ValueType: number;
    Value: string;
}
interface IValues {
    fullname?: IValesType;
    firstName?: IValesType;
    emailaddress1: IValesType;
    birthdate?: IValesType;
    dtk_customernumber?: IValesType;
    lastname?: IValesType;
    contactid?: IValesType;
}
export default interface IRespProfile {
    ID?: string;
    Name?: string;
    CustomerType?: string;
    Values?: IValues[];
    DelRequest?: {
        RequestedOn?: string;
        Status?: number;
    };
    ExternalReference?: string;
}
export {};
