import ITranslations from "../../ITranslations";
export default interface IRespCaseType {
    BookingIsRequired?: boolean;
    AvailableInBusinessPortal?: boolean;
    Translations?: ITranslations;
    ID?: string;
    NameCode?: string;
}
