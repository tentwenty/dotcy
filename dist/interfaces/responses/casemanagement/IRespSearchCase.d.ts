interface ICustomer {
    ID?: string;
    Name?: string;
}
interface IBooking {
    ID?: string;
    Name?: string;
}
export default interface IRespSearchCase {
    ID?: string;
    Title?: string;
    CaseReferenceNo?: string;
    CaseTypeID?: string;
    Customer?: ICustomer;
    CustomerType?: string;
    Booking?: IBooking;
    StatusCode?: string;
    CreatedOn?: string;
    ModifiedOn?: string;
}
export {};
