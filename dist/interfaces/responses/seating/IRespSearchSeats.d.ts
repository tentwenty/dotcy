interface ISearchSeatLookup {
    ID: string;
    Name: string;
}
export default interface IRespSearchSeats {
    ID: string;
    SeatNumber: string;
    Section: ISearchSeatLookup;
    TicketHolder: ISearchSeatLookup;
    TicketPrice: string;
    TicketNumber: string;
    BookingID: string;
}
export {};
