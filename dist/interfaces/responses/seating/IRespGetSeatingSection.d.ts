import IRespSeatingLookup from "./IRespSeatingLookup";
import ITranslations from "../../ITranslations";
interface ICustomer {
    ID: string;
    Name: string;
}
export default interface IRespGetSeatingSection {
    ID: string;
    Name: string;
    SeatingArea?: IRespSeatingLookup;
    Product?: IRespSeatingLookup;
    ProductVariants?: IRespSeatingLookup[];
    Translations?: ITranslations;
    Colour?: string;
    Status?: number;
    BookEntireSection?: boolean;
    Customer?: ICustomer;
    DefaultDoorID?: string;
}
export {};
