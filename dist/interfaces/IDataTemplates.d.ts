import { ICaching } from "./ITypes";
import { IReqProductFeatures } from "./requests/datatemplates";
import { IRespProductFeatures } from "./responses/datatemplates";
export default interface IDataTemplates {
    getProductFeatures(request: IReqProductFeatures, caching: ICaching): Promise<IRespProductFeatures[]>;
}
