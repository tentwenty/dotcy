import ITranslations from "./ITranslations";
import { IRespSeatingLookup } from "./responses/seating";
interface IOptions {
    [key: string]: string;
}
export default interface IProductVariant {
    BaseProductID: string;
    Price: number;
    CurrencyID: string;
    DisplayOrder: number;
    Options?: IOptions;
    Pricelists: string[];
    Availability: number;
    Translations: ITranslations;
    ID: string;
    Name: string;
    Message?: string;
    RenewalDaysBeforeExpiration?: number;
    MemberTypeID?: string;
    MembershipHandling?: number;
    RenewalDaysAfterExpiration?: number;
    SeatingSections: IRespSeatingLookup[];
    MinimumTicketsRequired?: number;
    MaximumTicketsAllowed?: number;
    PricingType?: number;
}
export {};
