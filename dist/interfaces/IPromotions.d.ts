import { IReqSearchCoupon, IReqValidateCoupon, IReqSearchVouchers, IReqValidateVoucher } from "./requests/promotions";
import { IRespCoupons, IRespValidateCoupon, IRespVouchers, IRespValidateVoucher } from "./responses/promotions";
export default interface IPayments {
    searchCoupon(searchParams: IReqSearchCoupon): Promise<IRespCoupons[]>;
    validateCoupon(couponData: IReqValidateCoupon): Promise<IRespValidateCoupon>;
    searchVoucher(searchParams: IReqSearchVouchers): Promise<IRespVouchers[]>;
    validateVoucher(validateReq: IReqValidateVoucher): Promise<IRespValidateVoucher>;
}
