import { ICaching } from "./ITypes";
import { IReqAddPayment, IReqCancelPayment, IReqUpdateStatus } from "./requests/payments";
import { IRespPayMethod, IRespPayConfig, IRespAddPayment } from "./responses/payments";
export default interface IPayments {
    getPaymentMethods(caching: ICaching, operatorID: string): Promise<IRespPayMethod[]>;
    getPaymentConfig({ methodType, methodID, caching, }: {
        methodType: string;
        methodID: string;
        caching?: ICaching;
    }): Promise<IRespPayConfig>;
    addPayment(paymentObj: IReqAddPayment): Promise<IRespAddPayment>;
    updatePaymentStatus(updateRe: IReqUpdateStatus): Promise<boolean>;
    getWalletBalance(customerID: string): Promise<number>;
    cancelPayment(paymentReq: IReqCancelPayment): Promise<boolean>;
}
