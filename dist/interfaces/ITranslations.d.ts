export default interface ITranslations {
    [key: string]: unknown;
}
