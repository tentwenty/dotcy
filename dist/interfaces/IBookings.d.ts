import { IBookingType, IRespCalculatedTax, ITaxLineItems } from "./ITypes";
import { IReqAddOrUpdateBooking, IReqExtendExpiration, IReqRemoveBookingItem, IReqSaveBooking, IReqUpdateBookingAddress, IReqRemoveTicketHolder, IReqAddOrUpdateTicketholder, IReqGetBookingFromPayment, IReqSearchTicket, IReqSearchBookings, IReqFulfillBooking, IReqSaveGuestDetails, IReqDownloadTicket, IReqSplitBooking, IReqMergeBooking, IReqBookingValidate } from "./requests/bookings";
import { IRespBookingByID, IRespSaveBooking, IRespSearchTicket, IRespBookingByTicketID, IRespSplitBooking, IRespAddOrUpdateBooking, IRespValidateBooking, IRespAddOrUpdateTicketholder } from "./responses/bookings";
export default interface IBookings {
    getCalculatedTax(taxLineItems: ITaxLineItems[]): Promise<IRespCalculatedTax>;
    saveBooking(booking: IReqSaveBooking): Promise<IRespSaveBooking>;
    updateBookingAddress(updateReq: IReqUpdateBookingAddress): Promise<boolean>;
    addOrUpdateBookingItem(addOrUpdateReq: IReqAddOrUpdateBooking): Promise<IRespAddOrUpdateBooking>;
    removeBookingItem(removeItemReq: IReqRemoveBookingItem): Promise<boolean>;
    getBookingById(bookingID: string, bookingType: IBookingType, loadCustomerInfo: boolean): Promise<IRespBookingByID>;
    getBookingByRefNo(bookingReference: string, emailAddress: string, loadCustomerInfo: boolean): Promise<IRespBookingByID>;
    abandonBooking(bookingID: string, noSaleReasonID?: string, operatorID?: string): Promise<boolean>;
    cancelBooking(bookingID: string, noSaleReasonID?: string, operatorID?: string, bookingRecordType?: IBookingType): Promise<boolean>;
    extendExpiration(extendExpReq: IReqExtendExpiration): Promise<boolean>;
    removeTicketholderFromBookingItem(removeReq: IReqRemoveTicketHolder): Promise<boolean>;
    addOrUpdateTicketholderFromBookingItem(addOrUpdateReq: IReqAddOrUpdateTicketholder): Promise<IRespAddOrUpdateTicketholder>;
    getBookingFromPaymentID(bookingReq: IReqGetBookingFromPayment): Promise<IRespBookingByID>;
    searchTickets(ticketReq: IReqSearchTicket): Promise<IRespSearchTicket[]>;
    searchBookings(bookingReq: IReqSearchBookings): Promise<IRespBookingByID[]>;
    fulfillBooking(bookingReq: IReqFulfillBooking): Promise<boolean>;
    getBookingByTicketId(ticketID: string): Promise<IRespBookingByTicketID>;
    saveGuestDetails(guestDetails: IReqSaveGuestDetails): Promise<boolean>;
    downloadTicket(ticket: IReqDownloadTicket): Promise<string>;
    getElectronicTicket(ticketID: string, versionNumber: string): Promise<string>;
    splitBooking(bookingReq: IReqSplitBooking): Promise<IRespSplitBooking>;
    mergeBooking(request: IReqMergeBooking): Promise<boolean>;
    validateBooking(bookingReq: IReqBookingValidate): Promise<IRespValidateBooking>;
}
