"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const classes_1 = require("./classes");
/**
 * Grants access to dotcy APIs via its public properties
 * @property products - Grants access to product APIs
 * @property references - Grants access to reference APIs
 * @property payments - Grants access to payment APIs
 * @property scheduler - Grants access to scheduler APIs
 * @property bookings - Grants access to booking APIs
 */
class Dotcy {
    /**
     * API credentials to create a connection with dotcy
     * @param options Conguration object containing the credentails
     * @param options.baseUrl Base url of the API
     * @param options.apiKey API key
     * @param options.apiUsername API Username
     * @param options.apiPassword API Password
     * @param options.apiVersion Version of API to be called. [v1, v2]
     * @param options.channelId Assigned channel ID
     * @param options.grantType Gran type used for authentication
     */
    constructor(options) {
        this.references = new classes_1.References(options);
        this.products = new classes_1.Products(options);
        this.payments = new classes_1.Payments(options);
        this.scheduler = new classes_1.Scheduler(options);
        this.membership = new classes_1.Membership(options);
        this.profile = new classes_1.Profile(options);
        this.bookings = new classes_1.Bookings(options);
        this.events = new classes_1.Events(options);
        this.eventsession = new classes_1.EventSession(options);
        this.seating = new classes_1.Seating(options);
        this.communications = new classes_1.Communications(options);
        this.website = new classes_1.Website(options);
        this.casemanagement = new classes_1.CaseManagement(options);
        this.promotions = new classes_1.Promotions(options);
        this.datatemplates = new classes_1.DataTemplates(options);
        this.tags = new classes_1.Tags(options);
        this.deployment = new classes_1.Deployment(options);
    }
}
exports.default = Dotcy;
//# sourceMappingURL=index.js.map