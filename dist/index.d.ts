import { Payments, Products, References, Scheduler, Membership, Bookings, Profile, Events, EventSession, Seating, Communications, Website, CaseManagement, Promotions, DataTemplates, Tags, Deployment } from "./classes";
import IConfig from "./interfaces/IConfig";
/**
 * Grants access to dotcy APIs via its public properties
 * @property products - Grants access to product APIs
 * @property references - Grants access to reference APIs
 * @property payments - Grants access to payment APIs
 * @property scheduler - Grants access to scheduler APIs
 * @property bookings - Grants access to booking APIs
 */
export default class Dotcy {
    products: Products;
    references: References;
    payments: Payments;
    scheduler: Scheduler;
    membership: Membership;
    profile: Profile;
    bookings: Bookings;
    events: Events;
    eventsession: EventSession;
    seating: Seating;
    communications: Communications;
    website: Website;
    casemanagement: CaseManagement;
    promotions: Promotions;
    datatemplates: DataTemplates;
    tags: Tags;
    deployment: Deployment;
    /**
     * API credentials to create a connection with dotcy
     * @param options Conguration object containing the credentails
     * @param options.baseUrl Base url of the API
     * @param options.apiKey API key
     * @param options.apiUsername API Username
     * @param options.apiPassword API Password
     * @param options.apiVersion Version of API to be called. [v1, v2]
     * @param options.channelId Assigned channel ID
     * @param options.grantType Gran type used for authentication
     */
    constructor(options: IConfig);
}
