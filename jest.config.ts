import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
    verbose: true,
    testPathIgnorePatterns: ["/node_modules/", "/dist/"],
    collectCoverageFrom: ["src/**/*.ts"],
    coveragePathIgnorePatterns: ["/node_modules/", "/dist/"],
    coverageThreshold: {
        global: {
            branches: 70,
            functions: 70,
            lines: 70,
            statements: 70,
        },
    },
    testEnvironment: "node",
    preset: "ts-jest",
};
export default config;
