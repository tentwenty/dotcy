import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import queryString from "query-string";
import { IArrayFormatter, IPathParams, IRouteParams } from "../interfaces/ITypes";

export default class Router {
    private config: IConfig;

    constructor(config: IConfig) {
        this.config = config;
    }

    public getRoute(
        key: string,
        params: IRouteParams = {},
        pathParams?: IPathParams,
        arrayFormat: IArrayFormatter = "comma",
    ): string {
        for (const index in pathParams) {
            const regex = new RegExp(`{${index}}`, "g");
            key = key.replace(regex, pathParams[index]);
        }
        key += queryString.stringify(params)
            ? "?" + queryString.stringify(params, { arrayFormat })
            : "";
        return key == constants.ENDPOINT_AUTHORIZE
            ? `${this.config.baseUrl}/api/${key}`
            : `${this.config.baseUrl}/api/${this.config.apiVersion}/${this.config.channelId}/${key}`;
    }
}
