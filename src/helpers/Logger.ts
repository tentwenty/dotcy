import winston from "winston";
import "winston-daily-rotate-file";

const myFormat = winston.format.printf(({ level, message, timestamp, ...metadata }) => {
    let msg = `${timestamp} [${level}] : ${message} `;
    if (Object.keys(metadata).length) msg += `: ${JSON.stringify(metadata)}`;

    return msg;
});

const filePath = `logs/dotcy/`;

const Logger = winston.createLogger({
    format: winston.format.combine(
        // winston.format.colorize(),
        winston.format.splat(),
        winston.format.timestamp(),
        myFormat,
    ),
    transports: [
        new winston.transports.DailyRotateFile({
            filename: `${filePath}error_%DATE%.log`,
            datePattern: "YYYY-MM-DD",
            level: "error",
            maxSize: "1m",
        }),
        new winston.transports.DailyRotateFile({
            filename: `${filePath}combined_%DATE%.log`,
            datePattern: "YYYY-MM-DD",
            maxSize: "1m",
        }),
    ],
});

// If we're not in production then log to the `console` with the format:
// if (process.env.NODE_ENV !== "production") logger.add(new winston.transports.Console());

export default Logger;
