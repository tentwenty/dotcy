import IConfig from "../interfaces/IConfig";
import axios, { AxiosInstance, AxiosRequestConfig, AxiosError, AxiosResponse } from "axios";
import db from "../helpers/db";
import { IAuthToken } from "../interfaces/IAuthToken";
import statusCodes from "http-status";
import * as constants from "../constants";
import Router from "./Router";
import Logger from "./Logger";
import http from "http";
import https from "https";

export default class Axios {
    public axiosInstance: AxiosInstance;
    private config: IConfig;
    private tokenId = "UWDAtVZHUc";
    private Router: Router;
    constructor(dotcyConfig: IConfig) {
        this.config = dotcyConfig;
        this.Router = new Router(dotcyConfig);
        this.axiosInstance = axios.create({
            timeout: 60000, //optional
            httpAgent: new http.Agent({ keepAlive: true }),
            httpsAgent: new https.Agent({ keepAlive: true }),
        });
        this.setRequestInterceptor();
        this.setResponseInterceptor();
    }
    private async getAccessToken(username: string, password: string): Promise<IAuthToken> {
        try {
            const params = new URLSearchParams();
            params.append("grant_type", this.config.grantType);
            params.append("username", username);
            params.append("password", password);
            const response = await axios.post(
                this.Router.getRoute(constants.ENDPOINT_AUTHORIZE),
                params,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                    },
                },
            );
            const result: IAuthToken = response.data;
            await db.update(
                { _id: this.tokenId },
                { _id: this.tokenId, ...result },
                { upsert: true },
            );
            return result;
        } catch (error) {
            Logger.error("getAccessToken", error.response.data);
            throw error;
        }
    }
    private setRequestInterceptor(): void {
        this.axiosInstance.interceptors.request.use(
            async (axiosConfig: AxiosRequestConfig) => {
                //Logger.info(`Hitting API: ${axiosConfig.url}`);
                let tokenObj: IAuthToken = await db.findOne({ _id: this.tokenId });
                //Logger.info(`Retrieved Token : ${tokenObj.access_token}`);
                if (!tokenObj) {
                    tokenObj = await this.getAccessToken(
                        this.config.apiUsername,
                        this.config.apiPassword,
                    );

                    //Logger.info(`Fresh Token : ${tokenObj.access_token}`);
                }
                axiosConfig.headers = {
                    Authorization: `Bearer ${tokenObj.access_token}`,
                    Accept: axiosConfig.headers.Accept
                        ? axiosConfig.headers.Accept
                        : "application/json",
                    "Content-Type": axiosConfig.headers["Content-Type"]
                        ? axiosConfig.headers["Content-Type"]
                        : "application/x-www-form-urlencoded",
                    APIKey: this.config.apiKey,
                };
                return axiosConfig;
            },
            (error: AxiosError) => {
                Logger.error("Axios Error :: setRequestInterceptor", error);
                Promise.reject(error);
            },
        );
    }
    private setResponseInterceptor(): void {
        this.axiosInstance.interceptors.response.use(
            (response: AxiosResponse) => {
                return response;
            },
            async (error: AxiosError) => {
                Logger.error(`error interceptor: ${JSON.stringify(error?.response?.data)}`);
                Logger.error(error.stack);
                const originalRequest = error.config;
                if (
                    error.response?.status === statusCodes.UNAUTHORIZED &&
                    !originalRequest["x-retry"]
                ) {
                    originalRequest["x-retry"] = true;
                    const tokenObj = await this.getAccessToken(
                        this.config.apiUsername,
                        this.config.apiPassword,
                    );
                    axios.defaults.headers.common[
                        "Authorization"
                    ] = `Bearer ${tokenObj.access_token}`;
                    return this.axiosInstance(originalRequest);
                }
                return Promise.reject(error);
            },
        );
    }
}
