import httpStatus from "http-status";

export default class CustomError extends Error {
    public message: string;
    public statusCode: number;
    public error: unknown;

    constructor(message: string, statusCode = httpStatus.INTERNAL_SERVER_ERROR) {
        super(message);
        this.statusCode = statusCode;
        this.error = httpStatus[statusCode];
        this.message = message;
    }
}
