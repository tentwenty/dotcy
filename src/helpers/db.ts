import { Datastore } from "nedb-async-await";

export default Datastore({ filename: "tokens.db", autoload: true });
