import { ICaching } from "./ITypes";
import {
    IRespChCurrencies,
    IRespCountries,
    IRespLang,
    IRespPersInterests,
    IRespChPricelists,
    IRespTaxProfiles,
    IRespVisitorCategories,
    IRespVenueAccesAreas,
} from "./responses/references";

export default interface IReferences {
    getCountries(caching: ICaching): Promise<IRespCountries[]>;
    getVenueAccessAreas(caching: ICaching): Promise<IRespVenueAccesAreas[]>;
    getChannelCurrencies(caching: ICaching): Promise<IRespChCurrencies[]>;
    getPersonalInterests(caching: ICaching): Promise<IRespPersInterests[]>;
    getLanguages(caching: ICaching): Promise<IRespLang[]>;
    getChannelPriceLists(caching: ICaching): Promise<IRespChPricelists[]>;
    getTaxProfiles(taxProfileIDs: string[], caching: ICaching): Promise<IRespTaxProfiles[]>;
    getVisitorCategories(caching: ICaching): Promise<IRespVisitorCategories[]>;
}
