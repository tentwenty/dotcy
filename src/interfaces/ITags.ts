import { ICaching } from "./ITypes";
import { ITagScope } from "../interfaces/ITypes";
import { IRespGetTags, IRespTagsForRecords } from "./responses/tags";
import { IReqTagsForRecords } from "./requests/tags";

export default interface ITags {
    getTags(scope: ITagScope, caching: ICaching): Promise<IRespGetTags[]>;
    getTagsForRecords(searchParams: IReqTagsForRecords): Promise<IRespTagsForRecords>;
}
