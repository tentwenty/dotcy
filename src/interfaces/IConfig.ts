export default interface IConfig {
    apiKey: string;
    baseUrl: string;
    channelId: string;
    apiVersion: string;
    grantType: string;
    apiUsername: string;
    apiPassword: string;
}
