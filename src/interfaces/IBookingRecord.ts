import IBookingItem from "./IBookingItem";

interface ICustomer {
    ID: string;
    CustomerType: "Contact" | "Account";
}
interface IOptions {
    [key: string]: string;
}
export default interface IBookingRecord {
    BookingRecordType: number;
    Customer: ICustomer;
    Items: IBookingItem[];
    InternalBookingID?: string;
    OpportunityRecordState?: number;
    OrderRecordState?: number;
    PriceListID: string;
    CurrencyID: string;
    SendCommunicationsUponFulfillment?: string[];
    SelectedOptions?: IOptions;
    BookingCategory?: string;
    ExternalReference?: string;
    DeliveryOptions?: string[];
}
