import {
    IRespCaseType,
    IRespSearchCase,
    IRespCreateCase,
    IRespCaseDetails,
} from "./responses/casemanagement";
import { IReqSearchCase, IReqCreateCase } from "./requests/casemanagement";
import { ICaching } from "./ITypes";

export default interface ICaseManagement {
    getCaseTypes(onlyChannelSupportedTypes: boolean, caching: ICaching): Promise<IRespCaseType[]>;
    searchCases(req: IReqSearchCase): Promise<IRespSearchCase[]>;
    createCase(req: IReqCreateCase): Promise<IRespCreateCase>;
    getCaseDetails(caseID: string): Promise<IRespCaseDetails>;
}
