import { ICaching } from "./ITypes";

export default interface IScheduler {
    getVenueWorkingDate({
        startDate,
        endDate,
        venueID,
        caching,
    }: {
        startDate: string;
        endDate: string;
        venueID: string;
        caching?: ICaching;
    }): Promise<string[]>;
}
