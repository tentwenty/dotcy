export default interface IReqDownloadTicket {
    bookingID: string;
    ticketIDs?: string[];
    returnOnlyAvailableTickets?: boolean;
    returnInBased64?: boolean;
    limitHandling?: string;
}
