import { IBookingType } from "../../ITypes";
import IBookingItem from "../../IBookingItem";

export default interface IReqAddOrUpdateBooking {
    bookingID: string;
    bookingItem: IBookingItem;
    bookingType?: IBookingType;
    terminalID?: string;
    operatorID?: string;
    priceCalculationInApi?: boolean;
}
