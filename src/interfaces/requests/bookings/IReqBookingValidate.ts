import IBookingRecord from "../../../interfaces/IBookingRecord";

export default interface IReqBookingValidate {
    Booking: IBookingRecord;
    CustomerEmailAddress: string;
}
