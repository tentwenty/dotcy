import { IBookingType } from "../../../interfaces/ITypes";

export default interface IReqRemoveTicketHolder {
    ticketHolderID: string;
    bookingItemID: string;
    bookingRecordType?: IBookingType;
    terminalID?: string;
    operatorID?: string;
}
