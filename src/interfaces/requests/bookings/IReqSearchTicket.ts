export default interface IReqSearchTicket {
    LastXTickets?: number;
    OrderBy?: string;
    TicketID?: string;
    BookingID?: string;
    BookingReferenceNumber?: string;
    StateCodes?: string[];
    StatusCodes?: string[];
    IsGuestCard?: boolean;
    MembershipIDs?: string[];
    ProfileID?: string;
    PartnerID?: string;
    ProfileTicketRelationship?: string;
    OnlyActiveTickets?: boolean;
    ProductID?: string;
    EventID?: string;
    BookingCreatedFromDate?: string;
    BookingCreatedToDate?: string;
    TicketNumber?: string;
}
