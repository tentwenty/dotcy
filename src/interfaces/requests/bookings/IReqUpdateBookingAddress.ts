export interface IAddress {
    IsBillToAddress: boolean;
    IsDeliverToAddress: boolean;
    ContactName: string;
    Street1: string;
    Street2?: string;
    Street3?: string;
    ZipOrPostalCode: string;
    City: string;
    County?: string;
    State?: string;
    Country: string;
    PhoneNumber: string;
    DeliveryDate?: string;
    DeliveryNotes?: string;
}

export default interface IReqUpdateBookingAddress {
    address: IAddress;
    bookingID: string;
    operatorID?: string;
}
