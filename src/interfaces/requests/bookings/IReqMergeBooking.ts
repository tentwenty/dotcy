interface IRequest {
    SecondaryBookingIDs: string[];
}

export default interface IReqMergeBooking {
    bookingID: string;
    request: IRequest;
    operatorID?: string;
    terminalID?: string;
    userIPAddress?: string;
    languageID?: string;
}
