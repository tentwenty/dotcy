import { IBookingType } from "../../ITypes";

interface ITicketHolder {
    ID?: string;
    CustomerID: string;
    CustomerType: string;
    NumberOfTickets: number;
    Email?: string;
    Firstname?: string;
    Lastname?: string;
    TicketHolderMessage?: string;
    MemberTypeID: string;
}

export default interface IReqAddOrUpdateTicketholder {
    bookingItemID: string;
    ticketHolder: ITicketHolder;
    bookingRecordType?: IBookingType;
    terminalID?: string;
    operatorID?: string;
}
