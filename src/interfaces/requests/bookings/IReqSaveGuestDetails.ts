import { IBookingType } from "../../../interfaces/ITypes";

interface IGuestDetails {
    AdditionalInfoFullName: string;
    AdditionalInfoEmail: string;
    AdditionalInfoPhone: string;
    AdditionalInfoLanguage?: {
        ID: string;
        Name: string;
    };
    AdditionalInfoCountry?: {
        ID: string;
        Name: string;
    };
}
export default interface IReqSaveGuestDetails {
    bookingID: string;
    GuestDetails: IGuestDetails;
    bookingType?: IBookingType;
    operatorID?: string;
}
