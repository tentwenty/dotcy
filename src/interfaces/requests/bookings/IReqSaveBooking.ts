import IBookingRecord from "../../IBookingRecord";

export default interface IReqSaveBooking {
    booking: IBookingRecord;
    operatorID?: string;
    terminalID?: string;
    sessionID?: string;
    generatePassword?: boolean;
    createBookingItems?: boolean;
}
