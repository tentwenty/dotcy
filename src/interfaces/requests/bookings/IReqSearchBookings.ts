import { IBookingType } from "../../ITypes";

type IStateCodes = "Active" | "Submitted" | "Canceled" | "Fulfilled" | "Invoiced";

type IStatusCodes =
    | "Empty"
    | "ActiveInProgress"
    | "SubmittedInProgress"
    | "Canceled"
    | "FulfilledCompleted"
    | "FulfilledPartialPayment"
    | "FulfilledReversed"
    | "Invoiced"
    | "CanceledRefunded"
    | "CanceledAmended";

type IProductTypeFilter =
    | "NoFilter"
    | "DayPass"
    | "OpenDatedDayPass"
    | "EventEntry"
    | "Stock"
    | "GroupActivity"
    | "IndividualActivity";

interface ICreatedOn {
    Start: string;
    End: string;
}

interface IDateFulfilled {
    Start: string;
    End: string;
}

interface IDateSubmitted {
    Start: string;
    End: string;
}

interface ISearchParams {
    BookingID?: string;
    ProfileID?: string;
    ChannelID?: string;
    StateCodes?: IStateCodes[];
    StatusCodes?: IStatusCodes[];
    CreatedOn?: ICreatedOn;
    DateFulfilled?: IDateFulfilled;
    DateSubmitted?: IDateSubmitted;
    TypeOfBooking?: IBookingType;
    PartnerID?: string;
    PortalUserID?: string;
    BookingReferenceNumber?: string;
    CustomerReference?: string;
    IncludeCreditPurchases?: true;
    ProductTypeFilter?: IProductTypeFilter;
    InvoiceID?: string;
    BookingCategories?: string[];
    ExternalReference?: string;
}

export default interface IReqSearchBookings {
    searchParams: ISearchParams;
    lastXBookings?: number;
}
