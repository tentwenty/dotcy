interface ITimeSlot {
    Start?: string;
    End?: string;
}

export default interface ISearchSeatParams {
    SearchType?: string;
    ID?: string;
    ProductID?: string;
    ProductVariantID?: string;
    AreaID?: string;
    SectionID?: string;
    LineItemID?: string;
    Timeslot?: ITimeSlot;
    EventSessionID?: string;
    EventSessionSlotID?: string;
    ActivitySessionID?: string;
}
