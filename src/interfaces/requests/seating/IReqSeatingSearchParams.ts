export default interface ISeatingAreaSearchParams {
    AreaID?: string;
    IncludeInactiveRecords?: boolean;
    OnlyDefaultLayout?: boolean;
    AccessZoneIDs?: string[];
    LayoutID?: string;
    ProductID: string;
}
