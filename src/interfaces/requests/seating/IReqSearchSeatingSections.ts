export default interface IReqSearchSeatingSections {
    SectionID?: string;
    IncludeInactiveRecords?: boolean;
    AreaID?: string;
    LayoutID?: string;
    ProductVariants?: string[];
}
