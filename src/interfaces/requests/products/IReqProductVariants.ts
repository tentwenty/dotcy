export default interface IReqProductVariants {
    ProductIDs: string[];
    LanguageIDs?: string[];
    OnlyChannelPriceList?: boolean;
}
