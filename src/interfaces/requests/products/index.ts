import IReqGetProductCapacity from "./IReqGetProductCapacity";
import IReqGetUpsellConfiguration from "./IReqGetUpsellConfiguration";
import IReqWaitingList from "./IReqWaitingList";
import IReqProductHierarchy from "./IReqProductHierarchy";
import IReqProductVariants from "./IReqProductVariants";

export {
    IReqGetProductCapacity,
    IReqGetUpsellConfiguration,
    IReqWaitingList,
    IReqProductHierarchy,
    IReqProductVariants,
};
