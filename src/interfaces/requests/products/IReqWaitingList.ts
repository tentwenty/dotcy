export default interface IReqWaitingList {
    EventID: string;
    ProductID: string;
    ProductVariantID?: string;
    ActivitySessionID?: string;
    ProfileID: string;
    ProfileType: string;
    RequestForDate: string;
    NoOfTickets: number;
}
