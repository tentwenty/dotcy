import { ICaching } from "../../ITypes";

export default interface IReqProductHierarchy {
    productHierarchyID?: string;
    loadProductVariants?: boolean;
    onlyChannelPriceListSupportedProducts?: boolean;
    loadCapacity?: boolean;
    loadTargetGroups?: boolean;
    loadAccessAreas?: boolean;
    loadTags?: boolean;
    loadSalesLiterature?: boolean;
    languageIDs?: string[];
    caching?: ICaching;
}
