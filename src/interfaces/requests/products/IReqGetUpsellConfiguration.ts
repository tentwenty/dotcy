export default interface IReqGetUpsellConfiguration {
    ProductIDs: string[];
    FromDate: string;
    ToDate: string;
    GetForAllChannels: boolean;
    OnlyChannelAvailableProducts: boolean;
}
