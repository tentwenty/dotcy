import IReqGenerateOTP from "./IReqGenerateOTP";
import IReqVerifyOTP from "./IReqVerifyOTP";
import IReqSendActivity from "./IReqSendActivity";
export { IReqGenerateOTP, IReqVerifyOTP, IReqSendActivity };
