export default interface IReqVerifyOTP {
    profileID: string;
    OTPTypeCode: string;
    OTPCode: string;
    profileType?: string;
    userIPAddress?: string;
    operatorID?: string;
    languageID?: string;
    terminalID?: string;
}
