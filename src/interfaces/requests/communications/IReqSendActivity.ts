interface IProfile {
    ID: string;
    Name: string;
    PartyType: "Contact" | "Account";
}

interface IActivityRequest {
    AlwaysSend: boolean;
    Subject: string;
    To: IProfile[];
    ActivityType: "Any" | "Email" | "SMS" | "WhatsApp" | "Custom";
    IsUnicode: boolean;
    LanguageID: string;
    Priority: "Low" | "Normal" | "High";
    BodyContent: string;
    BodyContentType: string;
}

export default interface IReqSendActivity {
    request: IActivityRequest;
    operatorID?: string;
    terminalID?: string;
}
