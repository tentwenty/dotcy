interface IVisitInfo {
    ID?: string;
    Name?: string;
    AgeRange?: string;
    GradeOrYear?: string;
    SubjectFocus?: string;
    TeacherName?: string;
    TeacherPhone?: string;
    NumberOfStudents?: number;
    NumberOfTeachersAndAssistants?: number;
    NumberOfPersonsWithDetermination?: number;
    NumberAssistantsForPersonsWithDetermination?: number;
    BusDriverName?: string;
    BusDrivePhone?: string;
    SchoolLocationCountry?: string;
    SchoolLocationState?: string;
    Meals?: number;
    SupervisorName?: string;
    SupervisorPhone?: string;
    SpecialRequestsComments?: string;
    LevelOfEducation?: string;
    MajorOrModule?: string;
}

export default interface IReqCreateCase {
    Title?: string;
    CaseTypeID?: string;
    CustomerID?: string;
    CustomerType?: string;
    PartnerID?: string;
    PortalUserID?: string;
    BookingID?: string;
    PaymentID?: string;
    Origin?: string;
    Description?: string;
    Tags?: string[];
    LanguageID?: string;
    PlannedVisitDate?: string;
    NumberOfVisitors?: number;
    ProductID?: string;
    ProductVariantID?: string;
    ActivitySessionID?: string;
    VisitInfo?: IVisitInfo;
    FollowUpRequired?: boolean;
    operatorID?: string;
}
