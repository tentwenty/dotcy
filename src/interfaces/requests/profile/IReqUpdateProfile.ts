interface IValesType {
    ValueType: string;
    Value: string;
}

interface IValues {
    fullname?: IValesType;
    firstname?: IValesType;
    lastname?: IValesType;
    emailaddress1?: IValesType;
    birthdate?: IValesType;
    emailaddress2?: IValesType;
    emailaddress3?: IValesType;
    telephone1?: IValesType;
    telephone2?: IValesType;
    telephone3?: IValesType;
    address1_line1?: IValesType;
    address1_line2?: IValesType;
    address1_line3?: IValesType;
    address1_city?: IValesType;
    address1_county?: IValesType;
    dtk_address1state?: IValesType;
    dtk_address1country?: IValesType;
    address1_postalcode?: IValesType;
    address1_postofficebox?: IValesType;
    address1_latitude?: IValesType;
    address1_longitude?: IValesType;
    address1_fax?: IValesType;
    address1_telephone1?: IValesType;
    address1_telephone3?: IValesType;
    address2_line1?: IValesType;
    address2_line2?: IValesType;
    address2_line3?: IValesType;
    address2_city?: IValesType;
    address2_county?: IValesType;
    dtk_address2state?: IValesType;
    dtk_address2country?: IValesType;
    address2_postalcode?: IValesType;
    address2_postofficebox?: IValesType;
    address2_latitude?: IValesType;
    address2_longitude?: IValesType;
    address2_fax?: IValesType;
    address2_telephone1?: IValesType;
    address2_telephone2?: IValesType;
    address2_telephone3?: IValesType;
    dtk_preferredcontactlanguage?: IValesType;
    dtk_donotallowsms?: boolean;
    donotphone?: boolean;
    donotemail?: boolean;
    donotbulkemail?: boolean;
    dtk_donotallowwhatsapp?: boolean;
}

export default interface IReqUpdateProfile {
    ID?: string;
    CustomerType: string;
    Values?: IValues;
    IsAnonymous?: boolean;
}
