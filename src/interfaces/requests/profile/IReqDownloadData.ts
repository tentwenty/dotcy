export default interface IReqDownloadData {
    profileID: string;
    userIPAddress: string;
    profileType?: string;
    password?: string;
}
