import IBookingRecord from "../../IBookingRecord";

export default interface IReqValidateCoupon {
    PaymentMethodID: string;
    CouponCode: string;
    BookingID: string;
    CurrencyID: string;
    Booking?: IBookingRecord;
    EntryStartDate?: string;
    OperatorID?: string;
}
