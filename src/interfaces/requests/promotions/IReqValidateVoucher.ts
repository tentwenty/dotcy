import { IRespBookingByID } from "./../../responses/bookings";

export default interface IReqValidateVoucher {
    PaymentMethodID: string;
    VoucherCode: string;
    Booking?: IRespBookingByID;
    BookingID?: string;
    EntryStartDate?: string;
    CurrencyID?: string;
    OperatorID?: string;
}
