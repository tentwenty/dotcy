export default interface IReqCancelPayment {
    paymentID: string;
    terminalID?: string;
    operatorID?: string;
    sessionID?: string;
}
