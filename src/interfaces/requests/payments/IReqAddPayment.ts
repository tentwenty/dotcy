interface IDiscountCoupon {
    ID: string;
    Name?: string;
}
interface IDiscountVoucher {
    ID: string;
    Name?: string;
}

interface ICustomer {
    ID: string;
    CustomerType: "Contact" | "Account";
    Name: string;
}

interface IFreeProductTicketholder {
    ID?: string;
    InternalID?: string;
    CustomerID?: string;
    CustomerType?: "Contact" | "Account";
    NumberOfTickets: number;
    Customer?: {
        ID: string;
        Name: string;
        CustomerType: "Contact" | "Account";
        MembershipTypeIDs: string[];
        Email: string;
        AccountTRN: string;
        AccountRef: string;
        ImageIDs: string;
        IsActiveMember: boolean;
        FundSource: string;
    };
    NameOverride?: string;
    FirstName?: string;
    LastName?: string;
    SendWelcomeEmailOption?:
        | "DoNotSend"
        | "OnPurchaseDate"
        | "OnMembershipStartDate"
        | "OnSpecifiedDate";
    SendWelcomeEmailOn?: string;
    Email?: string;
    TicketHolderMessage?: string;
    CreateProfile?: boolean;
    Gender?: string;
    DateOfBirth?: string;
}

interface IFreeProductSeats {
    ID: string;
    SeatNumber: string;
    Section: {
        ID: string;
        Name: string;
    };
    TicketHolder: {
        ID: string;
        Name: string;
    };
    TicketPrice: number;
    TicketNumber: string;
    BookingID: string;
}

interface IPayment {
    ID?: string;
    Name?: string;
    Origin?: string;
    BookingSequenceNo?: string;
    PaymentCategory: string;
    CustomerTaxNo?: string;
    Amount: number;
    PaymentDate?: string;
    ChannelID: string;
    CurrencyID: string;
    MethodOfPaymentID: string;
    ForceCloseBookingOnFullPayment: boolean;
    PaymentStatus: "Submitted" | "Approved";
    DiscountCoupon?: IDiscountCoupon;
    DiscountVoucher?: IDiscountVoucher;
    NotesAndComments?: string;
    Customer?: ICustomer;
    SendEmail?: "Send" | "DoNotSend";
    PaymentType?: string;
    FreeProductEntryStart?: string;
    FreeProductEntryEnd?: string;
    FreeProductGroupMembers?: number;
    FreeProductActivitySession?: string;
    FreeProductTimeBlockSchemeID?: string;
    FreeProductTimeBlockSlotUniqueKey?: string;
    FreeProductTicketHolders?: IFreeProductTicketholder[];
    FreeProductSeats?: IFreeProductSeats;
}

export default interface IReqAddPayment {
    bookingID: string;
    payment: IPayment;
    operatorID?: string;
    autoApprove?: boolean;
    revalidateBookingOnPayment?: boolean;
}
