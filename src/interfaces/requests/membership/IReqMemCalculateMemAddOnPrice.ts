interface IRequest {
    MembershipID: string;
    ProductVariantID: string;
    CurrencyID: string;
}
export default interface IReqMemCalculateMemAddOnPrice {
    request: IRequest;
    calculationType: "AddOnMember" | "TypeTransition";
}
