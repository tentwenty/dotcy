export default interface IReqMemCustInfo {
    profileIDs: string;
    profileType: string;
    retrieveOnlyActiveMembers: boolean;
}
