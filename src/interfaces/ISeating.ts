import { ICaching } from "./ITypes";
import {
    IReqSearchSeatParams,
    IReqSeatingSearchParams,
    IReqSearchSeatingSections,
} from "./requests/seating";
import { IRespSearchSeats, IRespSeatingAreas, IRespGetSeatingSection } from "./responses/seating";

export default interface ISeating {
    searchSeatingAreas(
        searchParams: IReqSeatingSearchParams,
        caching?: ICaching,
    ): Promise<IRespSeatingAreas[]>;
    searchSeats(searchParams: IReqSearchSeatParams): Promise<IRespSearchSeats[]>;
    getSeatingSection(seatingSectionID: string): Promise<IRespGetSeatingSection>;
    searchSeatingSections(
        searchParams: IReqSearchSeatingSections,
    ): Promise<IRespGetSeatingSection[]>;
}
