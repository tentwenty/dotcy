import { IPriority } from "./ITypes";
import { IReqGenerateOTP, IReqVerifyOTP, IReqSendActivity } from "./requests/communications";
import {
    IRespGenerateOTP,
    IRespVerifyOTP,
    IRespSendActivity,
    IRespCustomNotifications,
    IRespBulkNotification,
    IRespSendTicketsToTicketholder,
} from "./responses/communications";

export default interface ICommunications {
    generateOTP(otpReq: IReqGenerateOTP): Promise<IRespGenerateOTP>;
    verifyOTP(otpReq: IReqVerifyOTP): Promise<IRespVerifyOTP>;
    sendActivity(activityReq: IReqSendActivity): Promise<IRespSendActivity>;
    getCustomNotifications(
        lastActivityTimeStamp: string,
        activityPriority: IPriority,
    ): Promise<IRespCustomNotifications[]>;
    getBulkNotification(activityID: string): Promise<IRespBulkNotification>;
    sendTicketsToTicketHolder(
        ticketID: string,
        bookingID?: string,
        senderProfileID?: string,
    ): Promise<IRespSendTicketsToTicketholder>;
}
