export type ICaching = "Default" | "DisableCaching" | "ServerSideOnly" | "ClientSideOnly";
export type IArrayFormatter = "none" | "comma" | "bracket" | "index";
export type IPathParams = { [key: string]: string };
export type IRouteParams = {
    [key: string]: string | number | boolean | string[];
};
export type ITaxLineItems = {
    LineItemID: string;
    ProductID: string;
    AmountBeforeTax: number;
    TaxProfileID: string;
};

export type IRespCalculatedTax = {
    [key: string]: number;
};

export type IBookingType = "Order" | "Opportunity" | "Both";
export type IEntryTimeSlot = {
    Start: string;
    End: string;
};

export type IPriority = "Low" | "Normal" | "High";
export type ITagScope = "None" | "Case" | "Product" | "Contact" | "Account" | "Booking";
