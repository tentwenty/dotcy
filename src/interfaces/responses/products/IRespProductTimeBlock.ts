interface IAvailabilitySlot {
    [key: string]: ITime;
}

interface ITime {
    Start: string;
    End: string;
}

export default interface IRespProductTimeBlock {
    ID: string;
    ProductID: string;
    Capacity: number;
    Duration: number;
    FromTime: string;
    ToTime: string;
    TimeBetweenSlots: number;
    ValidWeekDays: number[];
    AvailableSlots: IAvailabilitySlot[];
}
