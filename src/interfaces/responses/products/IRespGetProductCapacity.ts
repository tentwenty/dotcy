interface ICapacityDates {
    Start?: string;
    End?: string;
}

interface ICapacity {
    ProductID?: string;
    ActivitySessionID?: string;
    TimeBlockSchemeID?: string;
    CapacityDates?: ICapacityDates;
    TotalCapacity?: number;
    RemainingCapacity?: number;
    ResourceIDs?: string;
    TimeBlockSlotUniqueKey?: string;
}

export default interface IRespGetProductCapacity {
    Capacity?: ICapacity;
    VersionNo?: number;
}
