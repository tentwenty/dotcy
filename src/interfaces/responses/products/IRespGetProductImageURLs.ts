interface IProductImages {
    Small: string;
    Medium: string;
    Large: string;
}

export default interface IRespGetProductImageURLs {
    [key: string]: IProductImages;
}
