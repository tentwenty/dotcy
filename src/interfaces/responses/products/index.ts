import IRespProductHierarchy from "./IRespProductHierarchy";
import IRespProductVariants from "./IRespProductVariants";
import IRespGetProductCapacity from "./IRespGetProductCapacity";
import IRespProductTimeBlock from "./IRespProductTimeBlock";
import IRespProduct from "./IRespProduct";
import IRespVariantTypeOptions from "./IRespVariantTypeOptions";
import IRespGetUpsellConfiguration from "./IRespGetUpsellConfiguration";
import IRespWaitingList from "./IRespWaitingList";
import IRespGetUpsellConfigurationDetails from "./IRespGetUpsellConfigurationDetails";
import IRespGetProductImageURLs from "./IRespGetProductImageURLs";
import IRespGetProductLiterature from "./IRespGetProductLiterature";

export {
    IRespProductHierarchy,
    IRespProductVariants,
    IRespGetProductCapacity,
    IRespProductTimeBlock,
    IRespProduct,
    IRespVariantTypeOptions,
    IRespGetUpsellConfiguration,
    IRespWaitingList,
    IRespGetUpsellConfigurationDetails,
    IRespGetProductImageURLs,
    IRespGetProductLiterature,
};
