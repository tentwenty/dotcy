export default interface IRespGetUpsellConfiguration {
    UpSellConfigID: string;
    BaseProductID: string;
    UpSellProductID: string;
    BaseProductVariants: string[];
    UpSellProductVariants: string[];
    ValidFrom: string;
    ValidTo: string;
    QuantityType: string;
    SaleRelationshipType: string;
}
