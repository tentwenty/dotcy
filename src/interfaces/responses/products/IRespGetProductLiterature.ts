export default interface IRespGetProductLiterature {
    ID: string;
    VersionNumber?: number;
    Subject?: {
        ID: string;
        Name: string;
    };
    LiteratureType?: string;
    Title: string;
    ProductIDs: string[];
    Attachments: IProductAttachment[];
}

interface IProductAttachment {
    ID: string;
    Title: string;
    VersionNumber?: number;
    DocumentData: string;
    FileName: string;
    MimeType: string;
}
