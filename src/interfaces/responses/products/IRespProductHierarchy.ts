import IProductVariant from "../../IProductVariant";
import ITranslations from "../../ITranslations";

interface IVenue {
    ID: string;
    Name: string;
}

interface IPriceList {
    ID: string;
    Name: string;
}
interface IProductAssociations {
    ProductID?: string;
    ProductVariantID?: string;
    Quantity?: number;
    Requirement?: string;
}
interface IPackageOptions {
    ID?: string;
    Quantity?: number;
    ProductID?: string;
    ProductVariantID?: string;
    AdditionalFee?: number;
    AdditionalFeeCurrencyID?: string;
    DateHandling?: number;
}
interface IPackageProducts {
    ID?: string;
    Required?: number;
    Options?: IPackageOptions[];
}

interface IProduct {
    ID: string;
    Name: string;
    ProductType: number;
    Code: string;
    ProductStructure: number;
    AnonymousMode: boolean;
    IndividualTaxProfileID: string;
    CorporateTaxProfileID: string;
    GenerateTicket: boolean;
    TicketType: number;
    ActivityPricingType: number;
    VenueTimeZoneCode: number;
    VenueID: IVenue;
    PriceLists: IPriceList[];
    ProductVariants: IProductVariant[];
    VariantOptionsDisplay: number;
    SellableByMaxDaysInFuture: number;
    ValidityPeriodInDays?: number;
    ForceBookingSetting: number;
    AllowMultipleMemberships: number;
    Languages: string[];
    TargetGroups?: string[];
    AccessAreas?: string[];
    MembershipTypeID?: string;
    Event?: string;
    IsSeated?: boolean;
    ValidFrom?: string;
    ValidTo?: string;
    Associations?: IProductAssociations[];
    PackageProducts?: IPackageProducts[];
    UseTimeBlocks?: boolean;
    MembershipMaxMembers?: number;
    MembershipMinMembers?: number;
    ActivityType?: number;
    AssignDateOnActivation?: boolean;
    WaitingList?: number;
    MapURL: string;
    Tags: string[];
    Literature?: {
        [key: string]: string;
    };
    TimeBeforeEntranceToSellInMins?: number;
}

export default interface IRespProductHierarchy {
    Translations?: ITranslations;
    ID?: string;
    Name?: string;
    Products?: IProduct[];
}
