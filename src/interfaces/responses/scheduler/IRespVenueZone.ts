import ITranslations from "../../ITranslations";

export default interface IRespVenueZone {
    Translations?: ITranslations;
    VenueTimeZoneId?: string;
    ID?: string;
    Name?: string;
}
