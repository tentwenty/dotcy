interface IMembershipType {
    ID?: string;
    Name?: string;
}

export default interface IRespLoginExt {
    ProfileUserId?: string;
    Name?: string;
    ErrorCode?: string;
    Message?: string;
    IsLoginSuccessful?: boolean;
    LastLogin?: string;
    IsLocked?: boolean;
    IsActiveMember?: boolean;
    MembershipType?: IMembershipType;
    WebSiteRegistrationStatus?: IMembershipType;
}
