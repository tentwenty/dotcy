import IRespSeatingLookup from "./IRespSeatingLookup";
import ITranslations from "../../ITranslations";
export default interface IRespSeatingAreas {
    MaxCapacity: number;
    AccessZones: IRespSeatingLookup[];
    SeatingLayout: IRespSeatingLookup;
    AreaXmlDefinition: string;
    AreaDefinitionJson: string;
    Rows: number;
    Columns: number;
    Angle: number;
    RowNumbering: number;
    RowNumberingDirection: number;
    ColumnNumbering: number;
    ColumnNumberingDirection: number;
    ID: string;
    Name: string;
    Translations?: ITranslations;
    DefaultDoorID?: string;
}
