export default interface IRespMemSendInviteMember {
    IsSuccess?: boolean;
    RequestID?: string;
    ExpirationDate?: string;
    ErrorCode?: number;
    ErrorMessage?: string;
}
