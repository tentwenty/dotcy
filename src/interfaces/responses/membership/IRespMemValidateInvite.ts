export default interface IRespMemValidateInvite {
    InvitedProfileID?: string;
    ExpirationDate?: string;
    ParentMembershipID?: string;
    ErrorCode?: string;
    ErrorValues?: string;
    ErrorDescription?: string;
}
