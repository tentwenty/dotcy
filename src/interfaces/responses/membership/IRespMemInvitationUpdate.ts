export default interface IRespMemInvitationUpdate {
    RequestID?: string;
    Reason?: string;
}
