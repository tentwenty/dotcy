export default interface IRespMemCustInfo {
    CustomerID?: string;
    CustomerName?: string;
    CustomerType?: string;
    MembershipID?: string;
    MembershipNumber?: string;
    MembershipStartDate?: string;
    MembershipExpiryDate?: string;
    OnHoldFrom?: string;
    OnHoldUntil?: string;
    MembershipTypeID?: string;
    MembershipStatus?: string;
    ParentMemberID?: string;
    ParentMemberCustomerType?: string;
    ParentMembershipID?: string;
    MembershipTicketID?: string;
    PendingVerification?: boolean;
}
