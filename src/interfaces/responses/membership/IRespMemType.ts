import ITranslations from "../../ITranslations";

interface IBenefit {
    MembershipTypeID?: string;
    BenefitType?: string;
    BenefitEnforcement?: string;
    ProductVariantID?: string;
    Quantity?: number;
    BenefitPeriod?: string;
    BenefitConsumptionPeriod?: string;
    ID?: string;
    Name?: string;
}

interface IMemberType {
    ID?: string;
    Name?: string;
    MembershipTypeID?: string;
    MinimumMembers?: number;
    FreeMembers?: number;
    TotalMembers?: number;
    ExternalReference?: string;
}
export default interface IRespMemType {
    ID: string;
    ShortName?: string;
    NumberOfAllowedGuest?: number;
    AllowMultipleMemberships?: number;
    MemberGuestCardScanEnum?: number;
    MembershipDurationInMonths?: number;
    DaysFromLapsedMembershipForRenewal?: number;
    MembershipRenewalPeriodOptions?: number;
    RenewalOption?: number;
    DaysBeforeCurrentPeriodEndForRenewal?: number;
    FutureRenewalDaysThreshold?: number;
    MembershipMaxMembers?: number;
    MembershipMinMembers?: number;
    Benefits?: IBenefit[];
    MemberTypes?: IMemberType[];
    SendWelcomeEmailOption?: number;
    SendWelcomeEmails?: boolean;
    Name?: string;
    Translations?: ITranslations;
    PrimaryMemberMinAgeInMonths?: number;
}
