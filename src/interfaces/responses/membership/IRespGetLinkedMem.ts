interface IlastInvitation {
    ID?: string;
    ExpirationDate?: string;
    InvitationDate?: string;
    Status?: string;
    InviteeEmail?: string;
}
export default interface IRespGetLinkedMem {
    CustomerID?: string;
    CustomerName?: string;
    CustomerType?: string;
    MembershipID?: string;
    MembershipNumber?: string;
    MembershipStartDate?: string;
    MembershipExpiryDate?: string;
    OnHoldFrom?: string;
    OnHoldUntil?: string;
    MembershipTypeID?: string;
    MembershipStatus?: string;
    ParentMemberID?: string;
    ParentMemberCustomerType?: string;
    ParentMembershipID?: string;
    MembershipTicketID?: string;
    LastInvitation?: IlastInvitation;
}
