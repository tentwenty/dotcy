interface IMemDetails {
    ID: string;
    Name: string;
}
export default interface IRespBenefitConsumption {
    Membership?: IMemDetails;
    MembershipType?: string;
    BenefitID?: string;
    BenefitType?: string;
    ProductVariantID?: string;
    BenefitPeriod?: string;
    BenefitConsumptionPeriod?: string;
    TotalQuantity?: number;
    ConsumedQuantity?: number;
    AvailableQuantity?: number;
    BenefitConsumptionDate?: string;
}
