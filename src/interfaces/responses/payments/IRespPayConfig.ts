export default interface IRespPayConfig {
    ConfigVersionNo?: number;
    AllowPartialPayments?: boolean;
    AutoApprove?: boolean;
}
