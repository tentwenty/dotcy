export default interface IRespAddPayment {
    ID?: string;
    BookingItemIDs?: string[];
}
