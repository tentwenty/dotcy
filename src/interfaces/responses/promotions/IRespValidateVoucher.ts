export default interface IRespValidateVoucher {
    VoucherDetails?: {
        Status?: number;
        Code?: string;
        DiscountType?: number;
        ValidFrom?: string;
        ValidTo?: string;
        ID?: string;
        FreeProductValidWeekDays?: [];
        FreeProductVariantNameID?: {
            ID?: string;
            Name?: string;
        };
        OfferName?: string;
        FreeProductID?: string;
        FreeProductQty?: number;
    };
}
