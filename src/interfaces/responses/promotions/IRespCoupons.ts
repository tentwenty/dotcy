interface IOffering {
    ProductVariantID: string;
    ProductVariantName: string;
    Quantity: number;
    EntryFromDateTime: string;
    EntryToDateTime: string;
}

interface IPrerequisite {
    ProductVariantID: string;
    Quantity: number;
    EntryFromDateTime: string;
    EntryToDateTime: string;
}

interface IFreeProduct {
    ID: string;
    Name: string;
}

export default interface IRespCoupons {
    IsRestrictedToProfiles: boolean;
    AgentProfiles: string[];
    MaxAllowedUsagePerBooking: number;
    MaxAllowedUsagePerCustomerPerPeriod: number;
    IntervalBetweenUsesPerCustomer: number;
    TotalUsageLimit: number;
    IntervalBetweenTotalUsage: number;
    PaymentMethodID: string;
    Code: string;
    DiscountType: string;
    ValidFrom: string;
    ValidTo: string;
    ID: string;
    FreeProductValidWeekDays: string[];
    DiscountAmount: number;
    DiscountOverpriceHandling: string[];
    DiscountAmountCurrencyID: string;
    DiscountPercentage: number;
    FreeProductVariantNameID: IFreeProduct;
    OfferName: string;
    FreeProductID: string;
    FreeProductLimiToWeekDays: true;
    FreeProdFromDateTime: string;
    FreeProdToDateTime: string;
    FreeProductQty: number;
    DiscountApplication: string;
    Offerings: IOffering[];
    Prerequisites: IPrerequisite[];
}
