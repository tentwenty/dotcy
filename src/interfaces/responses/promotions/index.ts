import IRespCoupons from "./IRespCoupons";
import IRespValidateCoupon from "./IRespValidateCoupon";
import IRespValidateVoucher from "./IRespValidateVoucher";
import IRespVouchers from "./IRespVouchers";

export { IRespCoupons, IRespValidateCoupon, IRespValidateVoucher, IRespVouchers };
