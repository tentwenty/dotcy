interface IGenericItem {
    ProductVariantID: string;
    ProductVariantName: string;
    Quantity?: number;
    EntryFromDateTime: string;
    EntryToDateTime: string;
}

interface IFreeProductVariant {
    ID: string;
    Name: string;
}

interface ICouponDetails {
    IsRestrictedToProfiles: boolean;
    AgentProfiles: string[];
    MaxAllowedUsagePerBooking: number;
    MaxAllowedUsagePerCustomerPerPeriod: number;
    IntervalBetweenUsesPerCustomer: number;
    TotalUsageLimit: number;
    IntervalBetweenTotalUsage: number;
    Code: string;
    DiscountType: string;
    ValidFrom: string;
    ValidTo: string;
    ID: string;
    FreeProductValidWeekDays: string;
    DiscountAmount: number;
    DiscountOverpriceHandling: string;
    DiscountAmountCurrencyID: string;
    DiscountPercentage: number;
    FreeProductVariantNameID: IFreeProductVariant;
    OfferName: string;
    FreeProductID: string;
    FreeProductLimiToWeekDays: true;
    FreeProdFromDateTime: string;
    FreeProdToDateTime: string;
    FreeProductQty: number;
    DiscountApplication: string;
    Offerings: IGenericItem[];
    Prerequisites: IGenericItem[];
}

export default interface IRespValidateCoupon {
    ValidationResult?: string;
    DiscountAmount?: number;
    RemainingLimitDays?: number;
    CouponDetails?: ICouponDetails;
}
