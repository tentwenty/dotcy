interface IProfile {
    ID: string;
    Name: string;
}

export default interface IRespCustomNotifications {
    ID: string;
    Subject: string;
    To: IProfile[];
    CC: IProfile[];
    BCC: IProfile[];
    ActivityType: "Any" | "Email" | "SMS" | "WhatsApp" | "Custom";
    LanguageID: string;
    BodyContent: string;
    Metadata: string;
    CreatedOn: string;
    ModifiedOn: string;
    IsBulk: boolean;
}
