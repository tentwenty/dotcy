export default interface IRespVerifyOTP {
    IsValid?: boolean;
    ValidationMessage?: string;
}
