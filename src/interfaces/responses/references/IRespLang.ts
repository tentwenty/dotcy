export default interface IRespLang {
    ID: string;
    Name: string;
    CultureCode: string;
    DisplayName: string;
}
