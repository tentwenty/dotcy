import ITranslations from "../../ITranslations";

export default interface IRespEduLevels {
    Translations?: ITranslations;
    ID: string;
    Name: string;
}
