export default interface IRespTargetGroup {
    ID: string;
    Name: string;
}
