import ITranslations from "src/interfaces/ITranslations";
export default interface IRespVenueAccesAreas {
    IsOnline?: boolean;
    ParentAreaID?: string;
    Translations?: ITranslations;
    ID: string;
    Name: string;
}
