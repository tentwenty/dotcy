export default interface IRespChPricelists {
    ID: string;
    Name: string;
    ChannelSupported: boolean;
}
