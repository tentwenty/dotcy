import ITranslations from "../../ITranslations";

export default interface IRespCountries {
    Translations: ITranslations;
    ID: string;
    Name: string;
}
