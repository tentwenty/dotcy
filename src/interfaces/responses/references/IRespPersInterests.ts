import ITranslations from "../../ITranslations";

export default interface IRespPersInterests {
    ID: string;
    Name: string;
    Category: string;
    Description: string;
    Translations: ITranslations;
}
