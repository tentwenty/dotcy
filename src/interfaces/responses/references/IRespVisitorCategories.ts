import ITranslations from "../../ITranslations";

export default interface IRespVisitorCategories {
    ShortName: string;
    ProductID: string;
    ProductVariantID: string;
    DisplayOrderID: number;
    DisplayOnAccessControlForFreeTicket: boolean;
    Translations: ITranslations;
    ID: string;
    Name: string;
}
