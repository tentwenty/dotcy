export default interface IRespEventSession {
    EventID?: string;
    Code?: string;
    Title?: string;
    MaxCapacity?: number;
    Status?: number;
    SessionType?: number;
    BookingCreationType?: number;
    IsPublishedOnPortal?: boolean;
    ID?: string;
    Name?: string;
}
