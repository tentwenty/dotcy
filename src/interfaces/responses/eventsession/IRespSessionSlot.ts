interface ISession {
    ID: string;
    Name: string;
}

export default interface IRespSessionSlot {
    EventSession?: ISession;
    FromDateTime?: string;
    ToDateTime?: string;
    Status?: number;
    TargetGroups?: string[];
    MutuallyExclusiveSlots?: string[];
    ID?: string;
    Name?: string;
}
