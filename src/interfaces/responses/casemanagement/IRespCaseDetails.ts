interface IBooking {
    ID?: string;
    Name?: string;
}

interface ICustomer {
    ID: string;
    CustomerType: string;
    Name: string;
}

export default interface IRespCaseDetails {
    ID?: string;
    Title?: string;
    CaseReferenceNo?: string;
    CaseTypeID?: string;
    Customer?: ICustomer;
    Booking?: IBooking;
    StatusCode?: string;
    CreatedOn?: string;
}
