import { IEntryTimeSlot } from "../../ITypes";
import IProductVariant from "../../IProductVariant";

interface IVenue {
    ID: string;
    Name: string;
}

interface ITicketHolder {
    CustomerID: string;
    CustomerType: string;
    NumberOfTickets: number;
    Email?: string;
    FirstName?: string;
    LastName?: string;
    CreateProfile?: boolean;
    Gender?: "Male" | "Female";
    DateOfBirth?: string;
    TicketHolderMessage?: string;
    MemberTypeID?: string;
}

interface IBaseProduct {
    ID: string;
    Name: string;
    Code: string;
    ProductType: number;
    ProductStructure: number;
    AnonymousMode: boolean;
    VenueID: IVenue;
    ProductVariants: IProductVariant[];
    VariantOptionsDisplay: number;
    ForceBookingSetting: number;
    MembershipTypeID: string;
    MembershipMaxMembers: number;
    MembershipMinMembers: number;
    MembershipDurationInMonths: number;
    AllowMultipleMemberships: number;
    DaysFromLapsedMembershipForRenewal: number;
    MembershipRenewalPeriodOption: number;
    IndividualTaxProfileID: string;
    CorporateTaxProfileID: string;
    GenerateTicket: boolean;
    TicketType: number;
    ActivityPricingType: number;
    VenueTimeZoneCode: number;
}

interface ISection {
    ID: string;
    Name?: string;
}

interface ISeat {
    ID: string;
    SeatNumber: string;
    Section: ISection;
}

interface IBookingItem {
    ID: string;
    InternalItemID: string;
    Quantity: number;
    TotalCommission: number;
    AdvancedCommission: number;
    BookingCommission: number;
    ProductCommission: number;
    DeductedTotalCommission: number;
    BaseProductID: string;
    BaseProduct: IBaseProduct;
    ProductVariantID: string;
    ProductVariant: IProductVariant;
    VarProdCurrencyID: string;
    BookingCurrencyID: string;
    ExchangeRate: number;
    EntryTimeSlots: IEntryTimeSlot[];
    Seats: ISeat[];
    TicketHolders: ITicketHolder[];
    ProductType: number;
    UnitPrice: number;
    TaxValue: number;
    WriteInProduct: string;
    PackageProductQuantity: number;
    FreeProductVoucherID: string;
    FreeProductCouponID: string;
}

interface ICustomer {
    CustomerType: number;
    ID: string;
    Name: string;
    Email?: string;
}

interface IPaymentsMade {
    ID: string;
    Name: string;
    PaymentCategory: number;
    BookingSequenceNo: string;
    Customer: ICustomer;
    Amount: number;
    PaymentDate: string;
    ChannelID: string;
    AmountInBookingCurrency: number;
    CurrencyID: string;
    MethodOfPaymentID: string;
    NotesAndComments: string;
    PaymentStatus: number;
    PaymentType: number;
    BankName: string;
    LinkedBookingItemIDs: string[];
    MethodOfPaymentName: string;
    OutstandingBalance: number;
    DCCOpted: number;
}

export default interface IRespBookingByID {
    BookingRecordType?: number;
    ReferenceNo?: string;
    Items?: IBookingItem[];
    BookingOrOpportunityID?: string;
    InternalBookingID?: string;
    BookingDate?: string;
    CreatedByChannelID?: string;
    TopicOrName?: string;
    ServerTotalGrossAmount?: number;
    ServerTotalPaidAmount?: number;
    ServerTotalNetAmount?: number;
    ServerTotalDiscount?: number;
    ServerTotalTax?: number;
    PaymentsMade?: IPaymentsMade[];
    OpportunityRecordState?: number;
    OrderRecordState?: number;
    PriceListID?: string;
    CurrencyID?: string;
    Customer?: ICustomer;
    ExpiresOn?: string;
    BookingCategory?: string;
    ExternalReference?: string;
    ItemsCount?: number;
    TotalDueBalance?: number;
}
