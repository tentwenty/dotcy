export default interface IRespExtendExpiry {
    ExpirationDate: string;
    ExpirationDateUTC: string;
}
