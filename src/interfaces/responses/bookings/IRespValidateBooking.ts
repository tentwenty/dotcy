interface IValidationError {
    ValidationCode: string;
    Description: string;
    BookingItemID: string;
    ProductVariantID: string;
    EvaluatedProduct: string;
    BookingMinTotalAmount: number;
    BookingMaxTotalAmount: number;
    VariantMinQuantity: number;
    VariantMaxQuantity: number;
    PreviouslyPurchasedVariantQuantity: number;
    BookingProductsMinQuantity: number;
    BookingProductsMaxQuantity: number;
    PreviouslyPurchasedBookingProductsQuantity: number;
    RuleApplication: string;
    ValidationDateType: string;
    ValidationPeriod: string;
    ValidationDateFrom: string;
    ValidationDateTo: string;
}

export default interface IRespValidateBooking {
    IsSuccess: boolean;
    CustomerID: string;
    CustomerPartnerType: "Contact" | "Account";
    ValidationErrors: IValidationError[];
}
