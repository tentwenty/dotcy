interface IBookingItem {
    BookingItemID: string;
    ItemResources: string[];
    ChildLinkedItems: unknown;
    TicketHolders: unknown;
}

export default interface IRespSaveBooking {
    BookingID?: string;
    ReferenceNo?: string;
    BookingItemsAdded?: IBookingItem[];
    TotalAmount?: number;
    TotalTaxAmount?: number;
    DueAmount?: number;
}
