import { IRespEvent } from "../event";
import { IRespProduct } from "../products";
import { IRespEventSession, IRespSessionSlot } from "../eventsession";

export default interface IRespEventSetup {
    EventItemDate?: string;
    EventSlots?: IRespSessionSlot;
    EventSession?: IRespEventSession;
    Event?: IRespEvent;
    Products?: IRespProduct;
}
