export default interface IRespTagsForRecords {
    [key: string]: string[];
}
