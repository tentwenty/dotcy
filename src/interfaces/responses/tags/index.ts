import IRespGetTags from "./IRespGetTags";
import IRespTagsForRecords from "./IRespTagsForRecords";

export { IRespGetTags, IRespTagsForRecords };
