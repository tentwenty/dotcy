import ITranslations from "../../ITranslations";

export default interface IRespChannelInfo {
    ChannelTypeID: number;
    DefaultProductHierarchyID: string;
    Translations: ITranslations;
    ID: string;
    Name: string;
    TimeBeforeEntranceToSellProduct?: number;
}
