import { ICaching } from "./ITypes";
import {
    IReqGetProductCapacity,
    IReqGetUpsellConfiguration,
    IReqProductHierarchy,
    IReqWaitingList,
    IReqProductVariants,
} from "./requests/products";
import {
    IRespGetProductCapacity,
    IRespProductHierarchy,
    IRespProductVariants,
    IRespProductTimeBlock,
    IRespVariantTypeOptions,
    IRespGetUpsellConfiguration,
    IRespWaitingList,
    IRespGetUpsellConfigurationDetails,
    IRespGetProductImageURLs,
} from "./responses/products";

export default interface IProducts {
    getProductHierarchy(params: IReqProductHierarchy): Promise<IRespProductHierarchy[]>;
    getProductVariants(
        request: IReqProductVariants,
        caching: ICaching,
    ): Promise<IRespProductVariants>;
    getProductCapacity(
        searchParams: IReqGetProductCapacity,
        caching: ICaching,
    ): Promise<IRespGetProductCapacity>;
    getProductTimeBlocks(
        productID: string,
        startDate: string,
        endDate: string,
    ): Promise<IRespProductTimeBlock[]>;
    getVariantTypeOptions(caching: ICaching): Promise<IRespVariantTypeOptions[]>;
    getUpsellConfiguration(
        request: IReqGetUpsellConfiguration,
        caching: ICaching,
    ): Promise<IRespGetUpsellConfiguration[]>;
    addToWaitingList(params: IReqWaitingList): Promise<IRespWaitingList>;
    getUpSellConfigurationDetails(
        upsellConfigIDs: string[],
        caching: ICaching,
    ): Promise<IRespGetUpsellConfigurationDetails[]>;
    getProductImageUrls(productIDs: string[], caching: ICaching): Promise<IRespGetProductImageURLs>;
}
