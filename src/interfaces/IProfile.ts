import { IRespProfile, IRespUpdateProfile, IRespTicketAddress } from "./responses/profile";
import {
    IReqSearchProfile,
    IReqUpdateProfile,
    IReqTicketAddress,
    IReqDownloadData,
    IReqDeleteProfile,
    IReqRevokeProfile,
} from "./requests/profile";

export default interface IProfile {
    searchProfiles(searchParams: IReqSearchProfile): Promise<IRespProfile[]>;
    updateProfile(updateParams: IReqUpdateProfile): Promise<IRespUpdateProfile>;
    getProfileById(profileId: string): Promise<IRespProfile>;
    addOrUpdateTicketDeliveryAddress(ticketParams: IReqTicketAddress): Promise<IRespTicketAddress>;
    downloadData(data: IReqDownloadData): Promise<string>;
    deleteProfile(data: IReqDeleteProfile): Promise<string>;
    revokeDeleteProfile(data: IReqRevokeProfile): Promise<string>;
    getPersonalInterest(profileID: string): Promise<string[]>;
    updatePersonalInterest(
        profileID: string,
        associatePersonalInterests: string[],
        disassociatePersonalInterests: string[],
    ): Promise<boolean>;
}
