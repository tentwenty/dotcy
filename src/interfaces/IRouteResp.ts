export default interface IRouteResp {
    [key: string]: unknown;
}
