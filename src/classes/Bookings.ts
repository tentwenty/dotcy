import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import { IBookingType, IRespCalculatedTax, ITaxLineItems } from "../interfaces/ITypes";
import IBookings from "../interfaces/IBookings";
import {
    IReqAddOrUpdateBooking,
    IReqExtendExpiration,
    IReqRemoveBookingItem,
    IReqSaveBooking,
    IReqUpdateBookingAddress,
    IReqRemoveTicketHolder,
    IReqAddOrUpdateTicketholder,
    IReqGetBookingFromPayment,
    IReqSearchTicket,
    IReqSearchBookings,
    IReqFulfillBooking,
    IReqSetCustomer,
    IReqSaveGuestDetails,
    IReqDownloadTicket,
    IReqSplitBooking,
    IReqMergeBooking,
    IReqBookingValidate,
} from "../interfaces/requests/bookings";
import {
    IRespBookingByID,
    IRespSaveBooking,
    IRespSearchTicket,
    IRespBookingByTicketID,
    IRespSplitBooking,
    IRespAddOrUpdateBooking,
    IRespValidateBooking,
    IRespAddOrUpdateTicketholder,
} from "../interfaces/responses/bookings";
import Error from "../helpers/Error";

export default class Bookings implements IBookings {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Use this method to calculate the tax of various items based on their tax profile
     * @param taxLineItems List of items for tax calculation along with their tax information
     * @param taxLineItems.LineItemID - Line item ID
     * @param taxLineItems.ProductID - Product ID
     * @param taxLineItems.AmountBeforeTax - Total amount for a product
     * @param taxLineItems.TaxProfileID - IndividualTaxProfileID of the product
     * @returns Returns the reponse from dotcy platform
     */
    public async getCalculatedTax(taxLineItems: ITaxLineItems[]): Promise<IRespCalculatedTax> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_BOOKING_CALCULATE_TAX),
                taxLineItems,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.getCalculatedTax()", error);
            return {};
        }
    }

    /**
     * Use this method to create a booking
     * @param bookingReq The populated request object
     * @param bookingReq.booking The populated booking object
     * @param bookingReq.operatorID The Operator ID creating the booking
     * @param bookingReq.terminalID The terminal ID (POS or KIOSK) to which the booking is allocated to
     * @param bookingReq.sessionID The terminal session on which to assign the booking
     * @param bookingReq.generatePassword (Optional) If a password should be generated on the booking. Defaults to false.
     * @param bookingReq.createBookingItems (Optional) if set to true it will also create the items in the booking object that are not assigned a CRM ID. Default to true.
     * @returns Returns the reponse from dotcy platform
     */
    public async saveBooking(bookingReq: IReqSaveBooking): Promise<IRespSaveBooking> {
        try {
            const {
                generatePassword = false,
                createBookingItems = true,
                booking,
                ...params
            } = bookingReq;

            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_BOOKING_SAVE_BOOKING, {
                    generatePassword,
                    createBookingItems,
                    ...params,
                }),
                booking,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.saveBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Use this method to create a booking
     * @param updateReq The populated request object
     * @param updateReq.address The populated address object
     * @param updateReq.address.IsBillToAddress Use as billing address
     * @param updateReq.address.IsDeliverToAddress Use as delivery address
     * @param updateReq.address.ContactName Shipping contact name
     * @param updateReq.address.Street1 Address string for street
     * @param updateReq.address.Street2 (Optional) Address string for street
     * @param updateReq.address.Street3 (Optional) Address string for street
     * @param updateReq.address.ZipOrPostalCode Post code number
     * @param updateReq.address.City City name
     * @param updateReq.address.County County name
     * @param updateReq.address.Country Country name
     * @param updateReq.address.State State name
     * @param updateReq.address.State State name
     * @param updateReq.address.DeliveryDate delivery date
     * @param updateReq.address.DeliveryNotes Additional notes
     * @param updateReq.bookingID Booking ID to update
     * @param updateReq.operatorID The user doing this action
     * @returns Returns the reponse from dotcy platform
     */
    public async updateBookingAddress(updateReq: IReqUpdateBookingAddress): Promise<boolean> {
        try {
            const { address, bookingID, operatorID } = updateReq;
            await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_UPDATE_ADDRESS,
                    { operatorID },
                    { bookingID },
                ),
                address,
                constants.JSON_HEADER,
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.updateBookingAddress()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Adds or Updates a product in the booking
     * @param addOrUpdateReq The populated request object
     * @param bookingID - ID of booking that needs to be updated
     * @param bookingItem - Fully populated booking item
     * @param bookingItem.BaseProductID - ID of the base product
     * @param bookingItem.ProductVariantID - ID of the product Variant
     * @param bookingItem.EntryTimeSlots - Entry slots object
     * @param bookingItem.EntryTimeSlots.Start - Start date time string
     * @param bookingItem.EntryTimeSlots.End - End date time string
     * @param bookingItem.ID - (Optional) Updated the booking item if present else creates a new booking item
     * @param terminalID The assigned terminal from where the action was entered
     * @param operatorID The user executing the action
     * @param bookingType (Optional) "Order" | "Opportunity" | "Both". Defaults to Order
     * @param priceCalculationInApi (Optional) If the API should recalculate the pricing and taxes. Defaults to false
     * @returns Returns response from dotcy platform
     */
    public async addOrUpdateBookingItem(
        addOrUpdateReq: IReqAddOrUpdateBooking,
    ): Promise<IRespAddOrUpdateBooking> {
        try {
            const {
                bookingID,
                bookingItem,
                bookingType = "Order",
                priceCalculationInApi = false,
                ...params
            } = addOrUpdateReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_ADD_OR_UPDATE_BOOKING,
                    { bookingType, priceCalculationInApi, ...params },
                    { bookingID },
                ),
                bookingItem,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.addOrUpdateBookingItem()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Removes the specified line from the provided booking
     * @param removeItemReq Booking item details
     * @param removeItemReq.bookingID Booking item details
     * @param removeItemReq.bookingItemID Booking item details
     * @param removeItemReq.bookingType (Optional) "Order" | "Opportunity" | "Both". Defaults to Order
     * @param removeItemReq.terminalID (Optional) The temrinal from which the request was made
     * @param removeItemReq.operatorID (Optional) The user executing the action
     * @returns Returns boolean value to indicate the success of the removal of item
     */
    public async removeBookingItem(removeItemReq: IReqRemoveBookingItem): Promise<boolean> {
        try {
            const { bookingID, bookingItemID, bookingType = "Order", ...params } = removeItemReq;
            await this.axiosInstance.delete(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_REMOVE_ITEM,
                    { bookingType, ...params },
                    { bookingID, bookingItemID },
                ),
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.removeBookingItem()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Get booking by ID
     * @param bookingID ID string of the booking
     * @param bookingType (Optional) Type of booking. Defaults to Order
     * @param loadCustomerInfo (Optional) Default to false
     * @returns Returns the reponse from dotcy platform
     */
    public async getBookingById(
        bookingID: string,
        bookingType: IBookingType = "Order",
        loadCustomerInfo = false,
    ): Promise<IRespBookingByID> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_GET_BY_ID,
                    { bookingType, loadCustomerInfo },
                    { bookingID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.getBookingById()", error);
            return {};
        }
    }

    /**
     * Use this method to retrieve a booking by order-number (booking reference no) and email
     * If booking is null then the method will not cross reference the email address
     * @param bookingReference Unique booking reference no/Order ID
     * @param emailAddress (Optional) Email address of customer for additional check
     * @param loadCustomerInfo (Optional) Default to false
     * @returns Returns the reponse from dotcy platform
     */
    public async getBookingByRefNo(
        bookingReference: string,
        emailAddress?: string,
        loadCustomerInfo = false,
    ): Promise<IRespBookingByID> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_GET_BY_BOOKING_REFNO,
                    { emailAddress, loadCustomerInfo },
                    { bookingReference },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.getBookingByRefNo()", error);
            return {};
        }
    }

    /**
     * Cancels the specified booking (opportunity or order). if the booking is an order then also update the no sale reason if one is supplied
     * @param bookingID The ID of the booking to cancel
     * @param noSaleReasonID (Optional) The ID of the no-sales reason
     * @param operatorID (Optional) The user doing this action
     * @returns Returns the reponse from dotcy platform
     */
    public async abandonBooking(
        bookingID: string,
        noSaleReasonID?: string,
        operatorID?: string,
    ): Promise<boolean> {
        try {
            await this.axiosInstance.delete(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_ABANDON_BOOKING,
                    { noSaleReasonID, operatorID },
                    { bookingID },
                ),
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.abandonBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Cancels the specified booking (opportunity or order). if the booking is an order then also update the no sale reason if one is supplied
     * @param bookingID The ID of the booking to cancel
     * @param noSaleReasonID (Optional) The ID of the no-sales reason
     * @param operatorID (Optional) The user doing this action
     * @param bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @returns Returns the reponse from dotcy platform
     */
    public async cancelBooking(
        bookingID: string,
        noSaleReasonID?: string,
        operatorID?: string,
        bookingRecordType: IBookingType = "Order",
    ): Promise<boolean> {
        try {
            await this.axiosInstance.delete(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_CANCEL_BOOKING,
                    { noSaleReasonID, operatorID, bookingRecordType },
                    { bookingID },
                ),
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.cancelBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Extends the booking's expiration date
     * @param extendExpReq A fully populated request object
     * @param extendExpReq.bookingID The ID of the booking to cancel
     * @param extendExpReq.periodInMinutes (Optional) A period in minutes to extend the expiration of the booking - starting from the current time
     * @param extendExpReq.expirationDate (Optional) A date the booking expiration will be extended to. Date and Time
     * @param extendExpReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param extendExpReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    public async extendExpiration(extendExpReq: IReqExtendExpiration): Promise<boolean> {
        try {
            const { bookingID, ...params } = extendExpReq;

            const response = await this.axiosInstance.patch(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_EXTEND_EXPIRATION,
                    { ...params },
                    { bookingID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.extendExpiration()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Remove the specified ticket holder from the booking item
     * @param removeReq A fully populated request object
     * @param removeReq.bookingItemID The ID of the booking to cancel
     * @param removeReq.ticketHolderID The ID of the booking to cancel
     * @param removeReq.bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @param removeReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param removeReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    public async removeTicketholderFromBookingItem(
        removeReq: IReqRemoveTicketHolder,
    ): Promise<boolean> {
        try {
            const {
                bookingItemID,
                ticketHolderID,
                bookingRecordType = "Order",
                ...params
            } = removeReq;

            await this.axiosInstance.delete(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_REMOVE_TICKETHOLDER,
                    { bookingRecordType, ...params },
                    { bookingItemID, ticketHolderID },
                ),
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.removeTicketholderFromBookingItem()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Add a new ticket holder record to the booking item or update an existing one
     * @param addOrUpdateReq A fully populated request object
     * @param addOrUpdateReq.bookingItemID The booking item ID
     * @param addOrUpdateReq.ticketHolder The ticket holder object.
     * @param removeReq.bookingRecordType (Optional) The type of booking (opportunity or sales order). Defaults to Order
     * @param removeReq.operatorID (Optional) The operator making the extension request - used for terminal based channels like the POS
     * @param removeReq.terminalID (Optional) The terminal from which the request was made - used for terminal based channels like the POS
     * @returns Returns the reponse from dotcy platform
     */
    public async addOrUpdateTicketholderFromBookingItem(
        addOrUpdateReq: IReqAddOrUpdateTicketholder,
    ): Promise<IRespAddOrUpdateTicketholder> {
        try {
            const {
                bookingItemID,
                ticketHolder,
                bookingRecordType = "Order",
                ...params
            } = addOrUpdateReq;

            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_ADD_OR_UPD_TICKETHOLDER,
                    { bookingRecordType, ...params },
                    { bookingItemID },
                ),
                ticketHolder,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.addOrUpdateTicketholderFromBookingItem()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Loads and returns the details of the specified booking using the payment ID
     * @param bookingReq A fully populated request object
     * @param bookingReq.paymentID The ID of the payment to match against the booking
     * @param bookingReq.operatorID (Optional) The operator / user making the request
     * @param bookingReq.partnerID (Optional) The ID of the business partner (request from business portal)
     * @param bookingReq.portalUserID (Optional)The ID of the business portal user
     * @param bookingReq.loadCustomerInfo (Optional) Load additional customer info. Defaults to false
     * @returns Returns the reponse from dotcy platform
     */
    public async getBookingFromPaymentID(
        bookingReq: IReqGetBookingFromPayment,
    ): Promise<IRespBookingByID> {
        try {
            const { paymentID, loadCustomerInfo = false, ...params } = bookingReq;
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_GET_BY_PAYMENT_ID,
                    { loadCustomerInfo, ...params },
                    { paymentID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.getBookingFromPaymentID()", error);
            return {};
        }
    }

    /**
     * Get bookings based on search criteria
     * @param searchParams A fully populated request object
     * @param lastXBookings The last x number of bookings that need to be fetched
     * @returns Returns the reponse from dotcy platform
     */
    public async searchBookings(bookingReq: IReqSearchBookings): Promise<IRespBookingByID[]> {
        try {
            const { searchParams, lastXBookings = 100 } = bookingReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_BOOKING_SEARCH_BOOKINGS, { lastXBookings }),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.searchBookings()", error);
            return [];
        }
    }

    /**
     * Loads and returns the tickets purchased by the user
     * @param ticketReq json response
     * @returns Returns the reponse from dotcy platform
     */
    public async searchTickets(ticketReq: IReqSearchTicket): Promise<IRespSearchTicket[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_BOOKING_SEARCH_TICKETS),
                ticketReq,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.searchTickets()", error);
            return [];
        }
    }

    /**
     * Fulfill a booking
     * @param bookingReq - A fully populated booking request object
     * @returns Returns the response from dotcy platform
     */
    public async fulfillBooking(bookingReq: IReqFulfillBooking): Promise<boolean> {
        try {
            const { bookingID, ...params } = bookingReq;
            const response = await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_FULFILL_BOOKING,
                    { ...params },
                    { bookingID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.fulfillBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Set a customer for a booking
     * @param bookingId - A fully populated booking request object
     * @returns Returns the response from dotcy platform
     */
    public async setCustomer(bookingReq: IReqSetCustomer): Promise<boolean> {
        try {
            const { bookingID, ...params } = bookingReq;
            await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_SET_CUSTOMER,
                    { ...params },
                    { bookingID },
                ),
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.setCustomer()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    public async getBookingByTicketId(ticketID: string): Promise<IRespBookingByTicketID> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_BOOKING_GET_BY_TICKET_ID, {}, { ticketID }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.getBookingByTicketId()", error);
            return {};
        }
    }

    /**
     * Sets guest details for a booking
     * @param guestDetails A fully populated guest details object
     * @returns Returns the response from dotcy platform
     */
    public async saveGuestDetails(guestDetails: IReqSaveGuestDetails): Promise<boolean> {
        try {
            const { bookingID, GuestDetails, operatorID, bookingType = "Order" } = guestDetails;
            const response = await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_SAVE_GUEST_DETAILS,
                    {
                        operatorID,
                        bookingType,
                    },
                    { bookingID },
                ),
                GuestDetails,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.saveGuestDetails()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    public async downloadTicket(ticket: IReqDownloadTicket): Promise<string> {
        try {
            const bookingID = ticket.bookingID;
            const ticketIDs = ticket.ticketIDs;
            const returnInBased64 = ticket.returnInBased64;
            const returnOnlyAvailableTickets = ticket.returnOnlyAvailableTickets;
            const limitHandling = ticket.limitHandling;
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_DOWNLOAD_TICKET,
                    { ticketIDs, returnInBased64, returnOnlyAvailableTickets, limitHandling },
                    { bookingID },
                    "none",
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.downloadTicket()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Get booking by ID
     * @param ticketID ID string of the booking
     * @returns Returns the reponse from dotcy platform
     */
    public async getElectronicTicket(ticketID: string, versionNumber: string): Promise<string> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_ELECTRONIC_TICKET,
                    {},
                    { ticketID, versionNumber },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.getElectronicTicket()", error);
            return "";
        }
    }

    /**
     * Split booking
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    public async splitBooking(bookingReq: IReqSplitBooking): Promise<IRespSplitBooking> {
        try {
            const { bookingID, request, ...params } = bookingReq;
            const response = await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_SPLIT,
                    { ...params },
                    { bookingID },
                ),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.splitBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * merge booking
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    public async mergeBooking(bookingReq: IReqMergeBooking): Promise<boolean> {
        try {
            const { bookingID, request, ...params } = bookingReq;
            await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_BOOKING_MERGE,
                    { ...params },
                    { bookingID },
                ),
                request,
                constants.JSON_HEADER,
            );
            return true;
        } catch (error) {
            Logger.error("Bookings.mergeBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Validate booking for all the mandatory fields
     * @param bookingReq Fully populates booking request object
     * @returns Returns the reponse from dotcy platform
     */
    public async validateBooking(bookingReq: IReqBookingValidate): Promise<IRespValidateBooking> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_BOOKING_VALIDATION),
                bookingReq,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Bookings.validateBooking()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
