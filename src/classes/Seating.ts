import { AxiosInstance } from "axios";
import IConfig from "../interfaces/IConfig";
import Error from "../helpers/Error";
import Router from "../helpers/Router";
import ISeating from "../interfaces/ISeating";
import Axios from "../helpers/Axios";
import {
    IReqSearchSeatParams,
    IReqSeatingSearchParams,
    IReqSearchSeatingSections,
} from "../interfaces/requests/seating";
import { ICaching } from "../interfaces/ITypes";
import * as constants from "../constants";
import {
    IRespSeatingAreas,
    IRespSearchSeats,
    IRespGetSeatingSection,
} from "../interfaces/responses/seating";
import Logger from "../helpers/Logger";

export default class Seating implements ISeating {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    public async searchSeatingAreas(
        searchParams: IReqSeatingSearchParams,
        caching: ICaching = "Default",
    ): Promise<IRespSeatingAreas[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_SEATING_SEARCH_SEATING_AREAS, { caching }),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Seating.searchSeatingAreas()", error);
            return [];
        }
    }

    public async searchSeats(searchParams: IReqSearchSeatParams): Promise<IRespSearchSeats[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_SEATING_SEARCH_SEATS),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Seating.searchSeats()", error);
            return [];
        }
    }

    public async getSeatingSection(seatingSectionID: string): Promise<IRespGetSeatingSection> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_SEATING_GET_SEATING_SECTION,
                    {},
                    { seatingSectionID },
                    "none",
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Seating.getSeatingSection()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    public async searchSeatingSections(
        searchParams: IReqSearchSeatingSections,
    ): Promise<IRespGetSeatingSection[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_SEATING_SEARCH_SEATING_SECTIONS),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Seating.searchSeatingSections()", error);
            return [];
        }
    }
}
