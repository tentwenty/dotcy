import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import ICommunications from "../interfaces/ICommunications";
import {
    IReqGenerateOTP,
    IReqSendActivity,
    IReqVerifyOTP,
} from "../interfaces/requests/communications";
import {
    IRespCustomNotifications,
    IRespGenerateOTP,
    IRespSendActivity,
    IRespVerifyOTP,
    IRespSendTicketsToTicketholder,
    IRespBulkNotification,
} from "../interfaces/responses/communications";
import Error from "../helpers/Error";
import { IPriority } from "../interfaces/ITypes";

export default class Communications implements ICommunications {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Use this method to generate OTP
     * @param otpReq The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async generateOTP(otpReq: IReqGenerateOTP): Promise<IRespGenerateOTP> {
        try {
            const { profileID, OTPTypeCode, profileType, emailAddress } = otpReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_COMMUNICATIONS_GENERATE_OTP,
                    { OTPTypeCode, profileType, emailAddress },
                    { profileID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Communications.generateOTP()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Use this method to verify OTP
     * @param otpReq The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async verifyOTP(otpReq: IReqVerifyOTP): Promise<IRespVerifyOTP> {
        try {
            const { profileID, OTPTypeCode, profileType, OTPCode } = otpReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_COMMUNICATIONS_VERIFY_OTP,
                    { OTPTypeCode, profileType },
                    { profileID, OTPCode },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Communications.verifyOTP()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Creates and Sends a direct activity (email, SMS, WhatsApp)
     * @param activityReq A fully populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async sendActivity(activityReq: IReqSendActivity): Promise<IRespSendActivity> {
        try {
            const { request, ...params } = activityReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_COMMUNICATIONS_SEND_ACTIVITY, {
                    ...params,
                }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Communications.sendActivity()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Returns the list of custom notifications generated from the platform (such as mobile notifications)
     * @param lastActivityTimeStamp The date/time stamp from which onwards the method will return the created activities
     * @param activityPriority A filter of the priority of the messages to be retrieved
     * @returns Returns the reponse from dotcy platform
     */
    public async getCustomNotifications(
        lastActivityTimeStamp: string,
        activityPriority?: IPriority,
    ): Promise<IRespCustomNotifications[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_COMMUNICATIONS_GET_CUST_NOTIFICATIONS, {
                    lastActivityTimeStamp,
                    activityPriority,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Communications.getCustomNotifications()", error);
            return [];
        }
    }

    /**
     * Returns bulk notification data
     * @param activityID Notification ID for which information needs to be fetched
     * @returns Returns the reponse from dotcy platform
     */
    public async getBulkNotification(activityID: string): Promise<IRespBulkNotification> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_COMMUNICATIONS_GET_BULK_NOTIFICATIONS, {
                    activityID,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Communications.getBulkNotification()", error);
            return {};
        }
    }

    /**
     * Returns the list of custom notifications generated from the platform (such as mobile notifications)
     * @param lastActivityTimeStamp The date/time stamp from which onwards the method will return the created activities
     * @param activityPriority A filter of the priority of the messages to be retrieved
     * @returns Returns the reponse from dotcy platform
     */
    public async sendTicketsToTicketHolder(
        ticketID: string,
        bookingID?: string,
        senderProfileID?: string,
    ): Promise<IRespSendTicketsToTicketholder> {
        try {
            const response = await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_COMMUNICATIONS_SEND_TICKETS_TO_TICKETHOLDER,
                    { bookingID, senderProfileID },
                    { ticketID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Communications.sendTicketsToTicketHolder()", error);
            return {};
        }
    }
}
