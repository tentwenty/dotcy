import { AxiosInstance } from "axios";
import IReferences from "../interfaces/IReferences";
import { ICaching } from "../interfaces/ITypes";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import {
    IRespChCurrencies,
    IRespChPricelists,
    IRespCountries,
    IRespLang,
    IRespPersInterests,
    IRespTaxProfiles,
    IRespStates,
    IRespTargetGroup,
    IRespVenueAccess,
    IRespAgeRange,
    IRespSchoolGrades,
    IRespEduLevels,
    IRespTags,
    IRespVisitorCategories,
} from "../interfaces/responses/references";

export default class References implements IReferences {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getCountries(caching: ICaching = "Default"): Promise<IRespCountries[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_COUNTRIES, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getCountries()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getStates(caching: ICaching = "Default"): Promise<IRespStates[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_STATES, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getStates()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getTargetGroups(caching: ICaching = "Default"): Promise<IRespTargetGroup[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_TARGET_GROUPS, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getTargetGroups()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getVenueAccessAreas(
        venueID: string,
        caching: ICaching = "Default",
    ): Promise<IRespVenueAccess[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_REFERENCES_VENUE_ACCESS_AREAS,
                    { caching },
                    { venueID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getVenueAccessAreas()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getAgeRanges(caching: ICaching = "Default"): Promise<IRespAgeRange[]> {
        try {
            Logger.info(`Calling api :  ${constants.ENDPOINT_REFERENCES_AGE_RANGE}`);
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_AGE_RANGE, { caching }),
            );
            Logger.info(`References.getAgeRange() : ${response}`);
            return response.data;
        } catch (error) {
            Logger.error("References.getAgeRanges()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getSchoolGrades(caching: ICaching = "Default"): Promise<IRespSchoolGrades[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_SCHOOL_GRADES, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getSchoolGrades()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getEducationalLevels(caching: ICaching = "Default"): Promise<IRespEduLevels[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_EDUCATIONAL_LEVELS, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getEducationalLevels()", error);
            return [];
        }
    }

    /**
     * Get the list of countries defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getTags(caching: ICaching = "Default"): Promise<IRespTags[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_TAGS, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getTags()", error);
            return [];
        }
    }

    /**
     * Get the list of Currencies supported by the current channel
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getChannelCurrencies(caching: ICaching = "Default"): Promise<IRespChCurrencies[]> {
        try {
            Logger.info(`Calling api :  ${constants.ENDPOINT_REFERENCES_CH_CURRENCIES}`);
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_CH_CURRENCIES, { caching }),
            );
            Logger.info(`References.getChannelCurrencies() : ${response}`);
            return response.data;
        } catch (error) {
            Logger.error("References.getChannelCurrencies()", error);
            return [];
        }
    }

    /**
     * Gets the list of Personal Interests
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getPersonalInterests(
        caching: ICaching = "Default",
    ): Promise<IRespPersInterests[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_PER_INTERESTS, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getPersonalInterests()", error);
            return [];
        }
    }

    /**
     * Get the list of languages defined in the platform
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getLanguages(caching: ICaching = "Default"): Promise<IRespLang[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_LANGUAGES, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getLanguages()", error);
            return [];
        }
    }

    /**
     * Get the list of available price lists for the specified channel
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @param onlyChannelSupportedPriceLists (Optional) Fetches only pricelist supported by the channel if set to true
     * @returns Returns the reponse from dotcy platform
     */
    public async getChannelPriceLists(
        caching: ICaching = "Default",
        onlyChannelSupportedPriceLists = false,
    ): Promise<IRespChPricelists[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_CH_PRICELIST, {
                    caching,
                    onlyChannelSupportedPriceLists,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getChannelPriceLists()", error);
            return [];
        }
    }

    /**
     * Get the list of tax profiles - based on the specified tax profile ids
     * @param taxProfileIDs The IDs of the tax profiles to return their details
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getTaxProfiles(
        taxProfileIDs: string[] = [],
        caching: ICaching = "Default",
    ): Promise<IRespTaxProfiles[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_REFERENCES_TAX_PROFILES,
                    {
                        caching,
                        taxProfileIDs,
                    },
                    {},
                    constants.QS_INDEX_SEPARATOR,
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getTaxProfiles()", error);
            return [];
        }
    }
    /**
     * Get the list of visitor categories
     * @param caching (Optional) Caching policy to apply. Sets to Default if not explicitly set.
     * @returns Returns the reponse from dotcy platform
     */
    public async getVisitorCategories(
        caching: ICaching = "Default",
    ): Promise<IRespVisitorCategories[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_REFERENCES_VISITOR_CATEGORIES, {
                    caching,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("References.getVisitorCategories()", error);
            return [];
        }
    }
}
