import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import IScheduler from "../interfaces/IScheduler";
import { ICaching } from "../interfaces/ITypes";
import { IRespVenueZone } from "../interfaces/responses/scheduler";
import { IReqVenueZone } from "../interfaces/requests/scheduler";

export default class Scheduler implements IScheduler {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /** Gets Venue Working dates
     * @param caching Caching policy to apply
     * @param startDate Start date to start the search from
     * @param endDate End date to end the search on
     * @returns The available timestamps in the form of a string[]
     */
    public async getVenueWorkingDate({
        startDate = "",
        endDate = "",
        venueID = "",
        caching = "Default",
    }: {
        startDate: string;
        endDate: string;
        venueID: string;
        caching?: ICaching;
    }): Promise<string[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_SCHEDULER_VENUE_WORK_DATE, {
                    caching,
                    startDate,
                    endDate,
                    venueID,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Scheduler.getVenueWorkingDate()", error);
            return [];
        }
    }

    /**
     * Loads and returns the tickets purchased by the user
     * @param ticketReq json response
     * @returns Returns the reponse from dotcy platform
     */
    public async getVenueZone(veueReq: IReqVenueZone): Promise<IRespVenueZone[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_DEPLOYMENT_VENUE_INFO),
                veueReq,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Scheduler.getVenueZone()", error);
            return [];
        }
    }
}
