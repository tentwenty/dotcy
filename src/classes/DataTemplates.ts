import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import Error from "../helpers/Error";
import IDataTemplates from "../interfaces/IDataTemplates";
import { ICaching } from "../interfaces/ITypes";
import { IReqProductFeatures } from "../interfaces/requests/datatemplates";
import { IRespProductFeatures } from "../interfaces/responses/datatemplates";

export default class DataTemplates implements IDataTemplates {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Returns the template values for the specified product
     * @param request - A fully populated request object
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    public async getProductFeatures(
        request: IReqProductFeatures,
        caching: ICaching = "Default",
    ): Promise<IRespProductFeatures[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_DATA_TEMP_PRODUCT_FEATURES, { caching }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("DataTemplates.getProductFeatures()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
