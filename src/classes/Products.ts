import { AxiosInstance } from "axios";
import IProducts from "../interfaces/IProducts";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import { ICaching } from "../interfaces/ITypes";
import {
    IRespProductHierarchy,
    IRespProductVariants,
    IRespGetProductCapacity,
    IRespProductTimeBlock,
    IRespVariantTypeOptions,
    IRespGetUpsellConfiguration,
    IRespWaitingList,
    IRespGetUpsellConfigurationDetails,
    IRespGetProductImageURLs,
    IRespGetProductLiterature,
} from "../interfaces/responses/products";
import {
    IReqGetProductCapacity,
    IReqGetUpsellConfiguration,
    IReqProductHierarchy,
    IReqWaitingList,
    IReqProductVariants,
} from "../interfaces/requests/products";

import Error from "../helpers/Error";

export default class Products implements IProducts {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Fetches Product Hierarchy
     * @param productHierarchyID (Optional) The product hierarchy to load the groups from.
     * @param loadProductVariants (Optional) Loads product variants. Defaults to false.
     * @param onlyChannelPriceListSupportedProducts (Optional) Return only products for a channel. Defaults to true.
     * @param loadCapacity (Optional) Loads capacity if set to true. Defaults to false.
     * @param loadTargetGroups (Optional) Loads target groups. Defaults to false.
     * @param loadAccessAreas (Optional) Loads access areas for products if set to true. Defaults to false.
     * @param languageIDs (Optional) Loads products from specific lang. Defaults to empty Array.
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */

    public async getProductHierarchy(
        params: IReqProductHierarchy = {},
    ): Promise<IRespProductHierarchy[]> {
        try {
            const {
                productHierarchyID = "",
                loadProductVariants = false,
                onlyChannelPriceListSupportedProducts = true,
                loadCapacity = true,
                loadTargetGroups = true,
                loadAccessAreas = true,
                loadTags = true,
                loadSalesLiterature = true,
                languageIDs = [],
                caching = "Default",
            } = params;
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_PRODUCT_HIERARCHY, {
                    productHierarchyID,
                    loadProductVariants,
                    onlyChannelPriceListSupportedProducts,
                    loadCapacity,
                    loadTargetGroups,
                    loadAccessAreas,
                    loadTags,
                    loadSalesLiterature,
                    languageIDs,
                    caching,
                }),
            );
            Logger.info(response.data);
            return response.data;
        } catch (error) {
            Logger.error("Products.getProductHierarchy()", error);
            return [{}];
        }
    }

    /**
     * Fetches all the product variants
     * @param productIDs - Ids of products for which variants need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    public async getProductVariants(
        request: IReqProductVariants,
        caching: ICaching = "Default",
    ): Promise<IRespProductVariants> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PRODUCT_VARIANTS, { caching }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getProductVariants()", error);
            return {};
        }
    }

    /**
     * Fetches the product capacity
     * @param searchParams JSON Array
     * @returns Returns the response from dotcy platform
     */
    public async getProductCapacity(
        searchParams: IReqGetProductCapacity,
    ): Promise<IRespGetProductCapacity> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_PRODUCT_CAPACITY),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getProductCapacity()", error);
            return {};
        }
    }

    /**
     * Fetches all the product time blocks
     * @param startdate - current date
     * @param enddate - current date + sellablebymaxdaysinfuture
     * @returns Returns the response from dotcy platform
     */
    public async getProductTimeBlocks(
        productID: string,
        startDate: string,
        endDate: string,
    ): Promise<IRespProductTimeBlock[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PRODUCT_GET_TIME_BLOCKS,
                    { startDate, endDate },
                    { productID },
                    constants.QS_INDEX_SEPARATOR,
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getProductTimeBlocks()", error);
            return [];
        }
    }
    /**
     * Fetches all the variant type options
     * @returns Returns the response from dotcy platform
     */
    public async getVariantTypeOptions(
        caching: ICaching = "Default",
    ): Promise<IRespVariantTypeOptions[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_PRODUCT_VARIANT_TYPE_OPTIONS, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getVariantTypeOptions()", error);
            return [];
        }
    }

    /** Fetches the upsell products
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async getUpsellConfiguration(
        request: IReqGetUpsellConfiguration,
    ): Promise<IRespGetUpsellConfiguration[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getUpsellConfiguration()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
    /**
     * Fetches all the variant type options
     * @returns Returns the response from dotcy platform
     */
    public async addToWaitingList(params: IReqWaitingList): Promise<IRespWaitingList> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_WAITING_LIST),
                params,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.addToWaitingList()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Fetches the upsell products
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async getUpSellConfigurationDetails(
        upsellConfigIDs: string[],
        caching: ICaching = "Default",
    ): Promise<IRespGetUpsellConfigurationDetails[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG_DETAILS, {
                    caching,
                }),
                upsellConfigIDs,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getUpSellConfigurationDetails()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Fetches all images for specified product IDs
     * @param productIDs - Ids of products for which images need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    public async getProductImageUrls(
        productIDs: string[],
        caching?: ICaching,
    ): Promise<IRespGetProductImageURLs> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PRODUCT_GET_IMAGE_URLS,
                    {
                        productIDs,
                        caching,
                    },
                    null,
                    "index",
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getProductImageUrls()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Fetches all images for specified product IDs
     * @param salesLiteratureID - Ids of products for which images need to be fetched
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    public async getproductLiterature(
        salesLiteratureID: string,
        caching?: ICaching,
    ): Promise<IRespGetProductLiterature> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PRODUCT_GET_PRODUCT_LITERATURE,
                    {
                        salesLiteratureID,
                        caching,
                    },
                    null,
                    "index",
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Products.getproductLiterature()", error);
            throw new Error(error?.response?.data, error?.response.status);
        }
    }
}
