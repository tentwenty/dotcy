import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import { ICaching } from "../interfaces/ITypes";
import IMembership from "../interfaces/IMembership";
import {
    IRespMemCustInfo,
    IRespMemType,
    IRespMemTransitionType,
    IRespGetLinkedMem,
    IRespBenefitConsumption,
    IRespMemSendInviteMember,
    IRespMemCalculateMemAddOnPrice,
    IRespMemValidateInvite,
    IRespMemInvitationUpdate,
} from "../interfaces/responses/membership";

import {
    IReqMemCustInfo,
    IReqMemAddLinkedMember,
    IReqMemInviteMember,
    IReqMemCalculateMemAddOnPrice,
    IReqMemInvitationUpdate,
} from "../interfaces/requests/membership";

import Error from "../helpers/Error";

export default class Membership implements IMembership {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Fetches all the membership types
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    public async getMembershipTypes(caching: ICaching = "Default"): Promise<IRespMemType[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_TYPES, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.getPaymentMethods()", error);
            return [];
        }
    }

    /**
     * Fetch membership information
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    public async getSingleCustomerMembershipInfo(profileId: string): Promise<IRespMemCustInfo[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_MEMBERSHIP_SINGLE_MEM_INFO,
                    {},
                    { profileId },
                ),
            );

            return response.data;
        } catch (error) {
            Logger.error("Membership.getSingleCustomerMembershipInfo()", error);
            return [];
        }
    }

    /**
     * Fetch membership information
     * @param reqMem - IReqMemCustInfo
     * @returns Returns the response from dotcy platform
     */
    public async getCustomerMembershipInfo(reqMem: IReqMemCustInfo): Promise<IRespMemCustInfo[]> {
        try {
            const { profileIDs, profileType, retrieveOnlyActiveMembers } = reqMem;
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_CUSTOMER_MEMBERSHIP_INFO, {
                    profileIDs,
                    profileType,
                    retrieveOnlyActiveMembers,
                }),
            );

            return response.data;
        } catch (error) {
            Logger.error("Membership.getCustomerMembershipInfo()", error);
            return [];
        }
    }
    /**
     * Fetch membership transition type
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    public async getMembershipTransitionTypes(
        caching: ICaching = "Default",
    ): Promise<IRespMemTransitionType[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_TRANSITION_TYPES, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.getMembershipTransitionTypes()", error);
            return [];
        }
    }

    /**
     * Fetch linked members of a family membership
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    public async getLinkedMembership(
        profileId: string,
        caching = "Default",
    ): Promise<IRespGetLinkedMem[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_LINKED_MEM_INFO, {
                    caching,
                    profileId,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.getLinkedMembership()", error);
            return [];
        }
    }
    /**
     * Send membership invite for sub member
     * @param request - profile id of the submember to be invited
     * @param caching Default | Any. Sets to Default if not specified
     * @returns Returns the response from dotcy platform
     */
    public async sendMemberRegistrationInvite(
        request: IReqMemInviteMember,
        userIPAddress: string,
    ): Promise<IRespMemSendInviteMember> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_SEND_MEMB_REG_INVITE, {
                    userIPAddress,
                }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.sendMemberRegistrationInvite()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Fetch the benefit consumption of membership by a member
     * @param caching - Default | Any. Sets to Default if not specified
     * @param profileId - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    public async getBenefitConsumption(
        profileId: string,
        caching = "Default",
    ): Promise<IRespBenefitConsumption[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_BENEFIT_CONSUMPTION, {
                    caching,
                    profileId,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.getBenefitConsumption()", error);
            return [];
        }
    }

    /**
     * @param request - profile id of the customer
     * @returns Returns the response from dotcy platform
     */
    public async addLinkedMembers(request: IReqMemAddLinkedMember): Promise<string> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_ADD_LINKED_MEMBER),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.addLinkedMember()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async calculateMembershipAddOnPrice(
        memAddOnReq: IReqMemCalculateMemAddOnPrice,
    ): Promise<IRespMemCalculateMemAddOnPrice> {
        try {
            const { calculationType, request } = memAddOnReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_ADD_ON_PRICE, {
                    calculationType,
                }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.calculateMembershipAddOnPrice()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async validateInvitation(
        requestID: string,
        userIPAddress: string,
    ): Promise<IRespMemValidateInvite> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_MEMBERSHIP_VALIDATE_PRICE,
                    { userIPAddress },
                    { requestID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.validateInvitation()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async memberInvitationUpdate(
        request: IReqMemInvitationUpdate,
        userIPAddress?: string,
    ): Promise<IRespMemInvitationUpdate> {
        try {
            const response = await this.axiosInstance.put(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_INVITATION_UPDATE, {
                    userIPAddress,
                }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.memberInvitationUpdate()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    public async calculateMembershipUpgradePrice(
        memAddOnReq: IReqMemCalculateMemAddOnPrice,
    ): Promise<IRespMemCalculateMemAddOnPrice> {
        try {
            const { calculationType, request } = memAddOnReq;
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_UPGRADE_PRICE, {
                    calculationType,
                }),
                request,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Membership.calculateMembershipUpgradePrice()", error);
            return {};
        }
    }
}
