import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import { ICaching } from "../interfaces/ITypes";
import ICaseManagement from "../interfaces/ICaseManagement";
import { IReqSearchCase, IReqCreateCase } from "../interfaces/requests/casemanagement";
import {
    IRespCaseType,
    IRespSearchCase,
    IRespCreateCase,
    IRespCaseDetails,
} from "../interfaces/responses/casemanagement";
import Error from "../helpers/Error";

export default class CaseManagement implements ICaseManagement {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Use this method to get case types
     * @returns Returns the reponse from dotcy platform
     */
    public async getCaseTypes(
        onlyChannelSupportedTypes = false,
        caching: ICaching = "Default",
    ): Promise<IRespCaseType[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_GET_CASE_TYPES, {
                    caching,
                    onlyChannelSupportedTypes,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("CaseManagement.getCaseTypes()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Use this method to generate OTP
     * @param req The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async searchCases(req: IReqSearchCase): Promise<IRespSearchCase[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_SEARCH_CASES),
                req,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("CaseManagement.searchCases()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Use this method to generate OTP
     * @param req The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async createCase(req: IReqCreateCase): Promise<IRespCreateCase> {
        try {
            const { operatorID, ...params } = req;
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_CREATE_CASE, {
                    operatorID,
                }),
                params,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("CaseManagement.createCase()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Fetch individual case details
     * @param caseID The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async getCaseDetails(caseID: string): Promise<IRespCaseDetails> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_CASE_MANAGEMENT_CASE_DETAILS, { caseID }),
                caseID,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("CaseManagement.getCaseDetails()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
