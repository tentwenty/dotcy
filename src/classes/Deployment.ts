import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import { ICaching } from "../interfaces/ITypes";
import { IRespChannelInfo } from "../interfaces/responses/deployment";
import Error from "../helpers/Error";
import IDeployment from "../interfaces/IDeployment";

export default class Deployment implements IDeployment {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Get Channel Info
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    public async getChannelInfo(caching?: ICaching): Promise<IRespChannelInfo> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_DEPLOYMENT_CHANNEL_INFO, { caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Deployment.getChannelInfo()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
