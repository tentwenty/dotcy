import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import IEvents from "../interfaces/IEvents";
import { IRespEvent, IRespEventSetup, IRespSearchEvent } from "../interfaces/responses/event";
import { IReqSearchEvent } from "../interfaces/requests/eventsession";
import { ICaching } from "src/interfaces/ITypes";

export default class Events implements IEvents {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    public async getEventById(eventId: string): Promise<IRespEvent> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_EVENT_GET_BY_ID, {}, { eventId }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Events.getProfileById()", error);
            return {};
        }
    }

    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    public async getEventSetUp(): Promise<IRespEventSetup[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_EVENT_SETUP),
            );
            return response.data;
        } catch (error) {
            Logger.error("Events.getEventSetUp()", error);
            return [];
        }
    }

    public async searchEvents(
        searchParams: IReqSearchEvent,
        caching: ICaching = "Default",
    ): Promise<IRespSearchEvent[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_EVENT_SEARCH, { caching }),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Events.searchEvents()", error);
            return [];
        }
    }
}
