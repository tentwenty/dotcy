import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import IProfile from "../interfaces/IProfile";
import Error from "../helpers/Error";
import {
    IRespProfile,
    IRespUpdateProfile,
    IRespTicketAddress,
} from "../interfaces/responses/profile";
import {
    IReqSearchProfile,
    IReqUpdateProfile,
    IReqTicketAddress,
    IReqDownloadData,
    IReqDeleteProfile,
    IReqRevokeProfile,
} from "../interfaces/requests/profile";

export default class Profile implements IProfile {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /** Gets Profile Based on the Search Params
     * @param searchParams JSON Array
     * @returns Returns User Object Array
     */
    public async searchProfiles(searchParams: IReqSearchProfile): Promise<IRespProfile[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PROFILE_SEARCH_PROFILES),
                searchParams,
                constants.JSON_HEADER,
            );

            return response.data;
        } catch (error) {
            Logger.error("Profile.searchProfiles()", error);
            return [];
        }
    }

    /** Gets Profile Based on the Search Params
     * @param profileParam JSON Array
     * @returns Returns User Object
     */
    public async updateProfile(profileParam: IReqUpdateProfile): Promise<IRespUpdateProfile> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PROFILE_UPDATE_PROFILE),
                profileParam,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.updateProfile()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Gets Profile Based on the ID
     * @param id string
     * @returns Returns User Object
     */
    public async getProfileById(profileId: string): Promise<IRespProfile> {
        try {
            const AdditionalAttributes: string[] = [
                "birthdate",
                "emailaddress1",
                "emailaddress2",
                "emailaddress3",
                // "dtk_preferredcontactlanguage",
                "birthdate",
                "salutation",
                "address1_line1",
                "address1_county",
                "telephone1",
                "telephone2",
                "telephone3",
                "address1_line2",
                "address1_line3",
                "address1_city",
                "dtk_address1state",
                "dtk_address1country",
                "address1_postalcode",
                "address1_postofficebox",
                "address1_latitude",
                "address1_longitude",
                "address1_fax",
                "address1_telephone1",
                "address1_telephone3",
                "address2_line1",
                "address2_line2",
                "address2_line3",
                "address2_city",
                "address2_county",
                "dtk_address2state",
                "dtk_address2country",
                "address2_postalcode",
                "address2_postofficebox",
                "address2_latitude",
                "address2_longitude",
                "address2_fax",
                "address2_telephone1",
                "address2_telephone2",
                "address2_telephone3",
                "gendercode",
                "preferredcontactmethodcode",
                "donotphone",
                "dtk_donotallowsms",
                "donotemail",
                "donotbulkemail",
                "dtk_donotallowwhatsapp",
                "lifelongmembershipnumber",
            ];

            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PROFILE_GET_BY_ID,
                    { AdditionalAttributes },
                    { profileId },
                    "none",
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.getProfileById()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Gets Profile Based on the Search Params
     * @param profileParam JSON Array
     * @returns Returns User Object
     */
    public async addOrUpdateTicketDeliveryAddress(
        ticketParam: IReqTicketAddress,
    ): Promise<IRespTicketAddress> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_PROFILE_ADD_OR_UPDATE_TICKET_DETAILS),
                ticketParam,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.addOrUpdateTicketDeliveryAddress()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Gets Download Data Based on the Search Params
     * @param profileParam JSON Array
     * @returns string
     */
    public async downloadData(data: IReqDownloadData): Promise<string> {
        try {
            const { profileID, userIPAddress, profileType, password } = data;
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PROFILE_DOWNLOAD_DATA,
                    { userIPAddress, profileType, password },
                    { profileID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.downloadData()", error);
            return "";
        }
    }

    /** Initiate profile delete
     * @param profileParam JSON Array
     * @returns Returns string
     */
    public async deleteProfile(data: IReqDeleteProfile): Promise<string> {
        try {
            const { profileID, userIPAddress, profileType, reason, operatorID } = data;
            const response = await this.axiosInstance.delete(
                this.router.getRoute(
                    constants.ENDPOINT_PROFILE_DELETE_PROFILE,
                    { userIPAddress, profileType, reason, operatorID },
                    { profileID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.deleteProfile()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Revokes profile delete request
     * @param profileParam JSON Array
     * @returns string
     */
    public async revokeDeleteProfile(data: IReqRevokeProfile): Promise<string> {
        try {
            const { profileID, userIPAddress, profileType, reason } = data;
            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_PROFILE_REVOKE_DELETE_PROFILE,
                    { userIPAddress, profileType, reason },
                    { profileID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.revokeDeleteProfile()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async getPersonalInterest(profileID: string): Promise<string[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PROFILE_PERSONAL_INTEREST,
                    {},
                    { profileID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("Profile.getPersonalInterest()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
    /**
     * @param request - json object
     * @returns Returns the response from dotcy platform
     */
    public async updatePersonalInterest(
        profileID: string,
        associatePersonalInterests: string[],
        disassociatePersonalInterests: string[],
    ): Promise<boolean> {
        try {
            await this.axiosInstance.put(
                this.router.getRoute(
                    constants.ENDPOINT_PROFILE_PERSONAL_INTEREST,
                    { associatePersonalInterests, disassociatePersonalInterests },
                    { profileID },
                    "none",
                ),
                constants.JSON_HEADER,
            );
            return true;
        } catch (error) {
            Logger.error("Profile.updatePersonalInterest()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
