import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import IPayments from "../interfaces/IPayments";
import { ICaching } from "../interfaces/ITypes";
import { IRespPayConfig, IRespPayMethod, IRespAddPayment } from "../interfaces/responses/payments";
import {
    IReqAddPayment,
    IReqCancelPayment,
    IReqUpdateStatus,
} from "../interfaces/requests/payments";
import Error from "../helpers/Error";

export default class Payments implements IPayments {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Fetches all the product variants
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    public async getPaymentMethods(
        caching: ICaching = "Default",
        operatorID = "",
    ): Promise<IRespPayMethod[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_PAYMENT_METHODS, { caching, operatorID }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Payments.getPaymentMethods()", error);
            return [];
        }
    }

    /**
     * Fetch the payment related information
     * @param methodType The payment type
     * @param methodID The payment method id
     * @param caching - Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    public async getPaymentConfig({
        methodType,
        methodID,
        caching = "Default",
    }: {
        methodType: string;
        methodID: string;
        caching?: ICaching;
    }): Promise<IRespPayConfig> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PAYMENT_CONFIGS,
                    { caching },
                    { methodType, methodID },
                ),
            );

            return response.data;
        } catch (error) {
            Logger.error("Payments.getPaymentConfig()", error);
            return {};
        }
    }

    /**
     * Post Payment against a booking
     * @param paymentObj.operatorID Fully populated payment object
     * @param paymentObj.autoApprove Auto approve payment
     * @param paymentObj.revalidateBookingOnPayment Force re-validation of BookingRules on each payment issued using the API
     * @param paymentObj.payment Fully populated payment object
     * @param paymentObj.payment.Amount Amount being payed for
     * @param paymentObj.payment.ChannelID Channel from where the payment has been initiated
     * @param paymentObj.payment.CurrencyID Currency id of the payment
     * @param paymentObj.payment.ForceCloseBookingOnFullPayment Close booking once the full amount is payed
     * @param paymentObj.payment.MethodOfPaymentID Payment method ID
     * @param paymentObj.payment.PaymentCategory Payment category
     * @param paymentObj.payment.PaymentStatus Status of the payment. Values can be "Submitted" | "Approved"
     * @param paymentObj.payment.DiscountCoupon (Optional) Discount coupon data object
     * @param paymentObj.payment.DiscountVoucher (Optional) Discount Voucher data object
     * @param paymentObj.payment.NotesAndComments (Optional) Any additional notes
     * @returns return the ID string of the payment
     */
    public async addPayment(paymentObj: IReqAddPayment): Promise<IRespAddPayment> {
        try {
            const {
                bookingID,
                payment,
                autoApprove = false,
                revalidateBookingOnPayment = false,
                ...params
            } = paymentObj;
            const response = await this.axiosInstance.post(
                this.router.getRoute(
                    constants.ENDPOINT_PAYMENT_ADD_PAYMENT,
                    { autoApprove, revalidateBookingOnPayment, ...params },
                    { bookingID },
                ),
                payment,
                constants.JSON_HEADER,
            );

            return response.data;
        } catch (error) {
            Logger.error("Payments.addPayment()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /**
     * Change Payment Status and update bank response
     * @param paymentID Payment ID that needs to be updated
     * @param paymentStatus Payment status string
     * @param bankResponse Bank response object
     * @param operatorID (Optional) Operator ID
     * @returns Returns boolean value indicating the completion of task
     */
    public async updatePaymentStatus(updateReq: IReqUpdateStatus): Promise<boolean> {
        try {
            const { paymentID, paymentStatus = "Submitted", operatorID, bankResponse } = updateReq;
            await this.axiosInstance.patch(
                this.router.getRoute(
                    constants.ENDPOINT_PAYMENT_UPDATE_STATUS,
                    { paymentStatus, operatorID },
                    { paymentID },
                ),
                bankResponse,
                constants.JSON_HEADER,
            );

            return true;
        } catch (error) {
            Logger.error("Payments.updatePaymentStatus()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }

    /** Gets Wallet Balance Based on the Customer ID
     * @param customerID string
     * @returns Returns number
     */
    public async getWalletBalance(customerID: string): Promise<number> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_PAYMENT_GET_WALLET_BALANCE,
                    {},
                    { customerID },
                    "none",
                ),
            );

            return response.data;
        } catch (error) {
            Logger.error("Payment.getWalletBalance()", error);
            return 0;
        }
    }

    /** Gets Wallet Balance Based on the Customer ID
     * @param paymentReq A fully populated cancel payment request object
     * @param paymentReq.paymentID The ID of the payment to cancel
     * @param paymentReq.terminalID The Terminal from where the cancellation was requested (if applicable)
     * @param paymentReq.operatorID The Operator/User that requested the cancellation (if applicable)
     * @param paymentReq.sessionID The Terminal Session for which the cancellation is issued from
     * @returns Returns boolean value indicating the completion of task
     */
    public async cancelPayment(paymentReq: IReqCancelPayment): Promise<boolean> {
        try {
            const { paymentID, ...params } = paymentReq;
            await this.axiosInstance.delete(
                this.router.getRoute(
                    constants.ENDPOINT_PAYMENT_CANCEL_PAYMENT,
                    { ...params },
                    { paymentID },
                ),
            );

            return true;
        } catch (error) {
            Logger.error("Payment.cancelPayment()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
