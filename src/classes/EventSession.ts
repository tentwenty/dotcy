import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import IEventSession from "../interfaces/IEventSession";
import {
    IRespEventSession,
    IRespSessionSlot,
    IRespRemainingCapacity,
    IRespSessionProduct,
} from "../interfaces/responses/eventsession";
import {
    IReqEventSession,
    IReqSessionSlot,
    IReqSessionProduct,
} from "../interfaces/requests/eventsession";

export default class EventSession implements IEventSession {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }
    /** Posts Search Event Sessions
     * @param sessionParams IReqEventSession
     * @returns Returns Session Object Array
     */
    public async getEventSessions(sessionParams: IReqEventSession): Promise<IRespEventSession[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_EVENT_SESSION_SEARCH_EVENT_SESSION),
                sessionParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("EventSession.getEventSessions()", error);
            return [];
        }
    }

    /** Posts Search Event Sessions
     * @param sessionParams IReqEventSession
     * @returns Returns Session Object Array
     */
    public async getSessionsSlots(sessionParams: IReqSessionSlot): Promise<IRespSessionSlot[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_EVENT_SESSION_SEARCH_SESSION_SLOTS),
                sessionParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("EventSession.getSessionsSlots()", error);
            return [];
        }
    }

    /** Posts Search Event Sessions
     * @param capacityParams IReqRemainingCapacity
     * @returns Returns capacity Object
     */
    public async getRemainingCapacity(eventID: string): Promise<IRespRemainingCapacity> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(
                    constants.ENDPOINT_EVENT_SESSION_REMAINING_CAPACITY,
                    {},
                    { eventID },
                ),
            );
            return response.data;
        } catch (error) {
            Logger.error("EventSession.getSegetRemainingCapacityssionsSlots()", error);
            return {};
        }
    }

    /** Posts Search Event Sessions
     * @param sessionParams IReqEventSession
     * @returns Returns Session Object Array
     */
    public async getSessionProducts(
        sessionParams: IReqSessionProduct,
    ): Promise<IRespSessionProduct[]> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_EVENT_SESSION_PRODUCTS),
                sessionParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("EventSession.getEventSessions()", error);
            return [];
        }
    }
}
