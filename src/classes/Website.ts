import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import IWebsite from "../interfaces/IWebsite";
import { IReqLoginExt } from "../interfaces/requests/website";
import { IRespLoginExt } from "../interfaces/responses/website";
import Error from "../helpers/Error";

export default class Website implements IWebsite {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Use this method to generate OTP
     * @param otpReq The populated request object
     * @returns Returns the reponse from dotcy platform
     */
    public async loginExtAuthMethod(data: IReqLoginExt): Promise<IRespLoginExt> {
        try {
            const { extAuthMethodID, userNameOrCode, userIPAddress } = data;
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_WEBSITE_LOGIN_EXT_AUTH, {
                    extAuthMethodID,
                    userNameOrCode,
                    userIPAddress,
                }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Website.loginExtAuthMethod()", error);
            throw new Error(error?.response?.data, error.response.status);
        }
    }
}
