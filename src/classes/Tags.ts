import { AxiosInstance } from "axios";
import Axios from "../helpers/Axios";
import IConfig from "../interfaces/IConfig";
import * as constants from "../constants";
import Router from "../helpers/Router";
import Logger from "../helpers/Logger";
import { ICaching, ITagScope } from "../interfaces/ITypes";
import ITags from "../interfaces/ITags";
import { IRespGetTags, IRespTagsForRecords } from "../interfaces/responses/tags";
import { IReqTagsForRecords } from "../interfaces/requests/tags";

export default class Tags implements ITags {
    private axiosInstance: AxiosInstance;
    private router: Router;

    constructor(options: IConfig) {
        this.axiosInstance = new Axios(options).axiosInstance;
        this.router = new Router(options);
    }

    /**
     * Returns the list of tags for the provided entity type
     * @param scope - Scope of tags
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    public async getTags(scope: ITagScope, caching?: ICaching): Promise<IRespGetTags[]> {
        try {
            const response = await this.axiosInstance.get(
                this.router.getRoute(constants.ENDPOINT_TAGS_GET_TAGS, { scope, caching }),
            );
            return response.data;
        } catch (error) {
            Logger.error("Tags.getTags()", error);
            return [];
        }
    }

    /**
     * Returns the list of tags linked to a set of records specified by the search criteria
     * @param scope - Scope of tags
     * @param caching (Optional) Default | Any. Sets to Default if not specified
     * @returns Returns the reponse from dotcy platform
     */
    public async getTagsForRecords(searchParams: IReqTagsForRecords): Promise<IRespTagsForRecords> {
        try {
            const response = await this.axiosInstance.post(
                this.router.getRoute(constants.ENDPOINT_TAGS_GET_TAGS_FOR_RECORDS),
                searchParams,
                constants.JSON_HEADER,
            );
            return response.data;
        } catch (error) {
            Logger.error("Tags.getTagsForRecords()", error);
            return {};
        }
    }
}
