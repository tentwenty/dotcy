import {
    Payments,
    Products,
    References,
    Scheduler,
    Membership,
    Bookings,
    Profile,
    Events,
    EventSession,
    Seating,
    Communications,
    Website,
    CaseManagement,
    Promotions,
    DataTemplates,
    Tags,
    Deployment,
} from "./classes";
import IConfig from "./interfaces/IConfig";

/**
 * Grants access to dotcy APIs via its public properties
 * @property products - Grants access to product APIs
 * @property references - Grants access to reference APIs
 * @property payments - Grants access to payment APIs
 * @property scheduler - Grants access to scheduler APIs
 * @property bookings - Grants access to booking APIs
 */
export default class Dotcy {
    public products: Products;
    public references: References;
    public payments: Payments;
    public scheduler: Scheduler;
    public membership: Membership;
    public profile: Profile;
    public bookings: Bookings;
    public events: Events;
    public eventsession: EventSession;
    public seating: Seating;
    public communications: Communications;
    public website: Website;
    public casemanagement: CaseManagement;
    public promotions: Promotions;
    public datatemplates: DataTemplates;
    public tags: Tags;
    public deployment: Deployment;

    /**
     * API credentials to create a connection with dotcy
     * @param options Conguration object containing the credentails
     * @param options.baseUrl Base url of the API
     * @param options.apiKey API key
     * @param options.apiUsername API Username
     * @param options.apiPassword API Password
     * @param options.apiVersion Version of API to be called. [v1, v2]
     * @param options.channelId Assigned channel ID
     * @param options.grantType Gran type used for authentication
     */
    constructor(options: IConfig) {
        this.references = new References(options);
        this.products = new Products(options);
        this.payments = new Payments(options);
        this.scheduler = new Scheduler(options);
        this.membership = new Membership(options);
        this.profile = new Profile(options);
        this.bookings = new Bookings(options);
        this.events = new Events(options);
        this.eventsession = new EventSession(options);
        this.seating = new Seating(options);
        this.communications = new Communications(options);
        this.website = new Website(options);
        this.casemanagement = new CaseManagement(options);
        this.promotions = new Promotions(options);
        this.datatemplates = new DataTemplates(options);
        this.tags = new Tags(options);
        this.deployment = new Deployment(options);
    }
}
