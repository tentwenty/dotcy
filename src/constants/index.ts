// HTTP Query string seperators
export const QS_COMMA_SEPARATOR = "comma";
export const QS_INDEX_SEPARATOR = "index";
export const QS_BRACKET_SEPARATOR = "bracket";

// HTTP HEADERS
export const JSON_HEADER = { headers: { "Content-Type": "application/json" } };

// Authentication
export const ENDPOINT_AUTHORIZE = "token";

// Products APIs
export const ENDPOINT_PRODUCT_HIERARCHY = "Product/Hierarchy";
export const ENDPOINT_PRODUCT_VARIANTS = "Product/ProductVariants";
export const ENDPOINT_PRODUCT_GET_PRODUCT_CAPACITY = "Product/ProductCapacity";
export const ENDPOINT_PRODUCT_GET_TIME_BLOCKS = "Product/{productID}/TimeBlocks";
export const ENDPOINT_PRODUCT_VARIANT_TYPE_OPTIONS = "Product/VariantTypeOptions";
export const ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG = "Product/UpsellConfiguration";
export const ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG_DETAILS =
    "Product/UpSellConfigurationDetails";
export const ENDPOINT_PRODUCT_GET_IMAGE_URLS = "Product/ImageURLs";
export const ENDPOINT_PRODUCT_GET_PRODUCT_LITERATURE = "Product/ProductLiterature";

//Membership APIs
export const ENDPOINT_MEMBERSHIP_TYPES = "Memberships/MembershipTypes";
export const ENDPOINT_MEMBERSHIP_SINGLE_MEM_INFO =
    "Memberships/GetSingleCustomerMembershipInfo/{profileId}";
export const ENDPOINT_MEMBERSHIP_CUSTOMER_MEMBERSHIP_INFO = "Memberships/CustomersMembershipInfo";
export const ENDPOINT_MEMBERSHIP_TRANSITION_TYPES = "Memberships/TransitionTypes";
export const ENDPOINT_MEMBERSHIP_LINKED_MEM_INFO = "Memberships/GetLinkedMemberships/{profileID}";
export const ENDPOINT_MEMBERSHIP_SEND_MEMB_REG_INVITE = "Memberships/InviteMember";
export const ENDPOINT_MEMBERSHIP_BENEFIT_CONSUMPTION = "Memberships/BenefitConsumption/{profileID}";
export const ENDPOINT_MEMBERSHIP_ADD_LINKED_MEMBER = "Memberships/MergeMembershipToProfile";
export const ENDPOINT_MEMBERSHIP_CALC_MEM_ADD_ON_PRICE =
    "Memberships/CalculateMembershipAddOnPrice";
export const ENDPOINT_MEMBERSHIP_CALC_MEM_UPGRADE_PRICE =
    "Memberships/CalculatePrice/{calculationType}";
export const ENDPOINT_MEMBERSHIP_VALIDATE_PRICE = "Memberships/ValidateInvitation/{requestID}";
export const ENDPOINT_MEMBERSHIP_INVITATION_UPDATE = "Memberships/MemberInvitationUpdate";

// References APIs
export const ENDPOINT_REFERENCES_COUNTRIES = "References/Countries";
export const ENDPOINT_REFERENCES_STATES = "References/States";
export const ENDPOINT_REFERENCES_TARGET_GROUPS = "References/TargetGroups";
export const ENDPOINT_REFERENCES_AGE_RANGE = "References/AgeRanges";
export const ENDPOINT_REFERENCES_SCHOOL_GRADES = "References/SchoolGrades";
export const ENDPOINT_REFERENCES_EDUCATIONAL_LEVELS = "References/EducationLevels";
export const ENDPOINT_REFERENCES_TAGS = "References/Tags";
export const ENDPOINT_REFERENCES_PER_INTERESTS = "References/PersonalInterests";
export const ENDPOINT_REFERENCES_LANGUAGES = "References/Languages";
export const ENDPOINT_REFERENCES_CH_CURRENCIES = "References/ChannelCurrencies";
export const ENDPOINT_REFERENCES_CH_PRICELIST = "References/ChannelPriceLists";
export const ENDPOINT_REFERENCES_TAX_PROFILES = "References/TaxProfiles";
export const ENDPOINT_REFERENCES_VENUE_ACCESS_AREAS = "References/VenueAccessAreas/{venueID}";
export const ENDPOINT_REFERENCES_VISITOR_CATEGORIES = "References/VisitorCategories";

// Payment APIs
export const ENDPOINT_PAYMENT_METHODS = "Payment/PaymentMethods";
export const ENDPOINT_PAYMENT_CONFIGS = "Payment/Config/{methodType}/{methodID}";
export const ENDPOINT_PAYMENT_ADD_PAYMENT = "Payment/AddPayment/{bookingID}";
export const ENDPOINT_PAYMENT_UPDATE_STATUS = "Payment/{paymentID}/PaymentStatus";
export const ENDPOINT_PAYMENT_CANCEL_PAYMENT = "Payment/{paymentID}/Cancel";
export const ENDPOINT_PAYMENT_GET_WALLET_BALANCE =
    "Payment/CustomerAdvancePaymentBalance/{customerID}";

export const ENDPOINT_PPROMOTIONS_SEARCH_COUPONS = "Promotions/Coupons";
export const ENDPOINT_PPROMOTIONS_VALIDATE_COUPON = "Promotions/ValidateCoupon";
export const ENDPOINT_PPROMOTIONS_SEARCH_VOUCHER = "Promotions/SearchVouchers";
export const ENDPOINT_PPROMOTIONS_VALIDATE_VOUCHER = "Promotions/ValidateVoucher";

// Scheduler APIs
export const ENDPOINT_SCHEDULER_VENUE_WORK_DATE = "Scheduler/VenueWorkingDates";

//deployment APIs
export const ENDPOINT_DEPLOYMENT_VENUE_INFO = "Deployment/VenueInfo";

//Profile APIs
export const ENDPOINT_PROFILE_SEARCH_PROFILES = "Profile/SearchProfiles";
export const ENDPOINT_PROFILE_UPDATE_PROFILE = "Profile";
export const ENDPOINT_PROFILE_GET_BY_ID = "Profile/{profileId}";
export const ENDPOINT_PROFILE_ADD_OR_UPDATE_TICKET_DETAILS =
    "Profile/AddOrUpdateTicketDeliveryAddress";
export const ENDPOINT_PROFILE_DOWNLOAD_DATA = "Profile/{profileID}/DownloadData";
export const ENDPOINT_PROFILE_DELETE_PROFILE = "Profile/{profileID}/DeleteProfile";
export const ENDPOINT_PROFILE_REVOKE_DELETE_PROFILE = "Profile/{profileID}/RevokeDeleteRequest";
export const ENDPOINT_PROFILE_PERSONAL_INTEREST =
    "Profile/{profileID}/ProfileSelectedPersonalInterest";

// Booking APIs
export const ENDPOINT_BOOKING_CALCULATE_TAX = "Booking/CalculateTax";
export const ENDPOINT_BOOKING_SAVE_BOOKING = "Booking/SaveBooking";
export const ENDPOINT_BOOKING_UPDATE_ADDRESS = "Booking/{bookingID}/UpdateBookingAddress";
export const ENDPOINT_BOOKING_ADD_OR_UPDATE_BOOKING =
    "Booking/AddOrUpdateProductToBooking/{bookingID}";
export const ENDPOINT_BOOKING_REMOVE_ITEM = "Booking/RemoveProduct/{bookingItemID}/{bookingID}";
export const ENDPOINT_BOOKING_GET_BY_ID = "Booking/{bookingID}";
export const ENDPOINT_BOOKING_GET_BY_BOOKING_REFNO =
    "Booking/bookingByRefAndEmail/{bookingReference}";
export const ENDPOINT_BOOKING_ABANDON_BOOKING = "Booking/{bookingID}/Abandon";
export const ENDPOINT_BOOKING_CANCEL_BOOKING = "Booking/{bookingID}/Cancel";
export const ENDPOINT_BOOKING_EXTEND_EXPIRATION = "Booking/{bookingID}/ExtendExpiration";
export const ENDPOINT_BOOKING_ADD_OR_UPD_TICKETHOLDER =
    "Booking/AddOrUpdateTicketHolder/{bookingItemID}";
export const ENDPOINT_BOOKING_REMOVE_TICKETHOLDER =
    "Booking/RemoveTicketHolderFromBookingItem/{bookingItemID}/{ticketHolderID}";
export const ENDPOINT_BOOKING_GET_BY_PAYMENT_ID = "Booking/BookingFromPaymentID/{paymentID}";
export const ENDPOINT_BOOKING_SEARCH_TICKETS = "Booking/SearchTickets";
export const ENDPOINT_BOOKING_SEARCH_BOOKINGS = "Booking/SearchBookings";
export const ENDPOINT_BOOKING_FULFILL_BOOKING = "Booking/{bookingID}/Fulfill";
export const ENDPOINT_BOOKING_SET_CUSTOMER = "Booking/{bookingID}/SetCustomer";
export const ENDPOINT_BOOKING_GET_BY_TICKET_ID = "Booking/TicketByID/{ticketID}";
export const ENDPOINT_BOOKING_SAVE_GUEST_DETAILS = "Booking/SaveBookingGuestDetails/{bookingID}";
export const ENDPOINT_BOOKING_DOWNLOAD_TICKET = "Booking/{bookingID}/Tickets";
export const ENDPOINT_BOOKING_ELECTRONIC_TICKET =
    "Booking/ElectronicTicket/{ticketID}/{versionNumber}";
export const ENDPOINT_BOOKING_SPLIT = "Booking/Split/{bookingID}";
export const ENDPOINT_BOOKING_MERGE = "Booking/Merge/{bookingID}";
export const ENDPOINT_BOOKING_VALIDATION = "Booking/ValidateBooking";

//Events APIs
export const ENDPOINT_EVENT_GET_BY_ID = "Event/{eventId}";
export const ENDPOINT_EVENT_SETUP = "Event/EventSetup";
export const ENDPOINT_EVENT_SEARCH = "Event/SearchEvents";

//Event Session APIs
export const ENDPOINT_EVENT_SESSION_SEARCH_EVENT_SESSION = "EventSession/SearchEventSessions";
export const ENDPOINT_EVENT_SESSION_SEARCH_SESSION_SLOTS = "EventSession/SearchSessionSlots";
export const ENDPOINT_EVENT_SESSION_REMAINING_CAPACITY = "EventSession/RemainingCapacity/{eventID}";
export const ENDPOINT_EVENT_SESSION_PRODUCTS = "EventSession/SearchSessionProducts";

// Seating APIs
export const ENDPOINT_SEATING_SEARCH_SEATING_AREAS = "Seating/SearchSeatingAreas";
export const ENDPOINT_SEATING_SEARCH_SEATS = "Seating/SearchSeats";
export const ENDPOINT_SEATING_GET_SEATING_SECTION = "Seating/SeatingSection/{seatingSectionID}";
export const ENDPOINT_SEATING_SEARCH_SEATING_SECTIONS = "Seating/SearchSeatingSections";

//Communications APIs
export const ENDPOINT_COMMUNICATIONS_GENERATE_OTP = "Communications/{profileID}/GenerateOTP";
export const ENDPOINT_COMMUNICATIONS_VERIFY_OTP = "Communications/{profileID}/VerifyOTP/{OTPCode}";
export const ENDPOINT_COMMUNICATIONS_SEND_ACTIVITY = "Communications/SendActivity";
export const ENDPOINT_COMMUNICATIONS_GET_CUST_NOTIFICATIONS = "Communications/CustomNotifications";
export const ENDPOINT_COMMUNICATIONS_GET_BULK_NOTIFICATIONS =
    "Communications/BulkCustomNotification/{activityID}";
export const ENDPOINT_COMMUNICATIONS_SEND_TICKETS_TO_TICKETHOLDER =
    "Communications/{ticketID}/SendTicketToTicketHolder";

//Website
export const ENDPOINT_WEBSITE_LOGIN_EXT_AUTH = "WebSite/LoginExtAuthMethod";

//Case Management
export const ENDPOINT_CASE_MANAGEMENT_GET_CASE_TYPES = "CaseManagement/GetCaseTypes";
export const ENDPOINT_CASE_MANAGEMENT_SEARCH_CASES = "CaseManagement/SearchCases";
export const ENDPOINT_CASE_MANAGEMENT_CREATE_CASE = "CaseManagement/CreateCase";
export const ENDPOINT_CASE_MANAGEMENT_CASE_DETAILS = "CaseManagement/{caseID}";

//waiting list
export const ENDPOINT_WAITING_LIST = "WaitingList/RegisterForWaitingList";

// Data templates
export const ENDPOINT_DATA_TEMP_PRODUCT_FEATURES = "DataTemplates/ProductFeatures";

// Tags
export const ENDPOINT_TAGS_GET_TAGS = "Tags/Tags";
export const ENDPOINT_TAGS_GET_TAGS_FOR_RECORDS = "Tags/RecordTags";

// Deployment
export const ENDPOINT_DEPLOYMENT_CHANNEL_INFO = "Deployment/ChannelInfo";
