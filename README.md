# Dotcy

Dot.Cy, a Microsoft Gold Partner in Dynamics 365 for Customer Engagement (CRM), was founded in 1999. Since then, Dot.Cy has been providing business solutions in Cyprus and the Middle East using Microsoft platforms and technologies. Headquartered in Nicosia, Cyprus, and with a regional office in Dubai, UAE, Dot.Cy is one of the leading providers of Dynamics CRM and xRM based solutions in the region, serving the CRM needs of some of the most prestigious organizations.

Dot.Cy has extended Microsoft Dynamics CRM and is providing industry-specific solutions to some of the most prestigious organizations in the region.

### Installation / Getting Started
```bash
npm install dotcy
# or
yarn add dotcy
```

### Example Code / Quick Start
```js
import Dotcy from "dotcy";

let dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://xxxxxxxxxxxxxxxxxxxxxxxxx",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

let products = await dotcy.products.getProductVariants();
```
