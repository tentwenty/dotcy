import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Scheduler Class Test suite", () => {
    test("getVenueWorkingDate - Returns workings dates on successfull response from dotcy", async () => {
        const output = ["a", "b"];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_SCHEDULER_VENUE_WORK_DATE))
            .reply(httpStatus.OK, output);
        const result = await dotcy.scheduler.getVenueWorkingDate({
            startDate: undefined,
            endDate: undefined,
            venueID: undefined,
        });
        expect(result).toEqual(output);
    });

    test("getVenueWorkingDate - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_SCHEDULER_VENUE_WORK_DATE))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.scheduler.getVenueWorkingDate({
            startDate: new Date().toDateString(),
            endDate: new Date().toDateString(),
            venueID: "",
        });
        expect(result).toEqual([]);
    });

    test("getVenueZone - Returns workings dates on successfull response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_DEPLOYMENT_VENUE_INFO))
            .reply(httpStatus.OK, output);
        const result = await dotcy.scheduler.getVenueZone({
            VenueID: "xxx",
        });
        expect(result).toEqual(output);
    });

    test("getVenueZone - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_DEPLOYMENT_VENUE_INFO))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.scheduler.getVenueZone({
            VenueID: "xxxx",
        });
        expect(result).toEqual([]);
    });
});
