import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Tags methods", () => {
    test("getTags - Returns successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_TAGS_GET_TAGS))
            .reply(httpStatus.OK, [1, 2, 3]);
        const result = await dotcy.tags.getTags("Product");
        expect(result).toEqual([1, 2, 3]);
    });

    test("getTags - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_TAGS_GET_TAGS))
            .reply(httpStatus.BAD_REQUEST);

        const result = await dotcy.tags.getTags("Product");
        expect(result).toEqual([]);
    });

    test("getTagsForRecords - Returns successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_TAGS_GET_TAGS_FOR_RECORDS))
            .reply(httpStatus.OK, {});
        const result = await dotcy.tags.getTagsForRecords({
            Scope: "Product",
            ProductHierarchyID: "c898f836-736c-eb11-a815-00224899bbfd",
        });
        expect(result).toEqual({});
    });

    test("getTagsForRecords - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_TAGS_GET_TAGS_FOR_RECORDS))
            .reply(httpStatus.BAD_REQUEST);

        const result = await dotcy.tags.getTagsForRecords({
            Scope: "Product",
            ProductHierarchyID: "c898f836-736c-eb11-a815-00224899bbfd",
        });
        expect(result).toEqual({});
    });
});
