import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";
import Error from "../../src/helpers/Error";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Payments Class Test suite", () => {
    test("searchCoupon - searches a given coupon and returns response from dotcy", async () => {
        const output = { a: "a" };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_SEARCH_COUPONS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.promotions.searchCoupon({
            PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            SearchString: "xxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("searchCoupon - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_SEARCH_COUPONS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.promotions.searchCoupon({
            PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            SearchString: "xxxxxxxx",
        });
        expect(result).toEqual([]);
    });

    test("validateCoupon - validates coupon and returns response from dotcy", async () => {
        const output = { a: "a" };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_VALIDATE_COUPON))
            .reply(httpStatus.OK, output);
        const result = await dotcy.promotions.validateCoupon({
            BookingID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            CouponCode: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            CurrencyID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("validateCoupon - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_VALIDATE_COUPON))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.promotions.validateCoupon({
                BookingID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                CouponCode: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                CurrencyID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("validateVoucher - validates voucher and returns response from dotcy", async () => {
        const output = { a: "a" };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_VALIDATE_VOUCHER))
            .reply(httpStatus.OK, output);
        const result = await dotcy.promotions.validateVoucher({
            BookingID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            EntryStartDate: "2021-08-07T04:46:10.205Z",
            PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            VoucherCode: "XXXXXXXXXX",
            CurrencyID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("validateVoucher - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_VALIDATE_VOUCHER))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.promotions.validateVoucher({
                BookingID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                EntryStartDate: "2021-08-07T04:46:10.205Z",
                PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                VoucherCode: "XXXXXXXXXX",
                CurrencyID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("searchVoucher - searches a given voucher and returns response from dotcy", async () => {
        const output = [{ a: "a" }];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_SEARCH_VOUCHER))
            .reply(httpStatus.OK, output);
        const result = await dotcy.promotions.searchVoucher({
            PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            SearchString: "XXXXXXXXXX",
        });
        expect(result).toEqual(output);
    });

    test("searchVoucher - Handles bad request response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PPROMOTIONS_SEARCH_VOUCHER))
            .reply(httpStatus.OK, output);
        const result = await dotcy.promotions.searchVoucher({
            PaymentMethodID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            SearchString: "XXXXXXXXXX",
        });
        expect(result).toEqual(output);
    });
});
