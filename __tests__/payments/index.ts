import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";
import Error from "../../src/helpers/Error";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Payments Class Test suite", () => {
    test("getPaymentMethods - Returns pay methods on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PAYMENT_METHODS))
            .reply(httpStatus.OK, {});
        const result = await dotcy.payments.getPaymentMethods();
        expect(result).toEqual({});
    });

    test("getPaymentMethods - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PAYMENT_METHODS))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.payments.getPaymentMethods();
        expect(result).toEqual([]);
    });

    test("getPaymentConfig - Returns payment config on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Payment/Config/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.payments.getPaymentConfig({
            methodID: "aaaaaaaaaaaaaaaaaa",
            methodType: "bbbbbbbbbb",
        });
        expect(result).toEqual({});
    });

    test("getPaymentConfig - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Payment/Config/"))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.payments.getPaymentConfig({
            methodID: "aaaaaaaaaaaaaaaaaa",
            methodType: "bbbbbbbbbb",
        });
        expect(result).toEqual({});
    });

    test("addPayment - Returns payment id on successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Payment/AddPayment"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.payments.addPayment({
            payment: {
                PaymentCategory: "BookingPayment",
                Amount: 10,
                ChannelID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                CurrencyID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                MethodOfPaymentID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                NotesAndComments: "Payed for order via CC",
                ForceCloseBookingOnFullPayment: true,
                PaymentStatus: "Submitted",
            },
            bookingID: "xxxxxxxxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("addPayment - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Payment/AddPayment"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.payments.addPayment({
                payment: {
                    PaymentCategory: "BookingPayment",
                    Amount: 10,
                    ChannelID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                    CurrencyID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                    MethodOfPaymentID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                    NotesAndComments: "Payed for order via CC",
                    ForceCloseBookingOnFullPayment: true,
                    PaymentStatus: "Submitted",
                },
                bookingID: "xxxxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("updatePaymentStatus - Updates payment status on dotcy successfully", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .patch((uri) => uri.includes("PaymentStatus"))
            .reply(httpStatus.OK, "");
        const result = await dotcy.payments.updatePaymentStatus({
            paymentID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            paymentStatus: "Submitted",
        });
        expect(result).toBeTruthy();
    });

    test("updatePaymentStatus - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .patch((uri) => uri.includes("PaymentStatus"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.payments.updatePaymentStatus({
                paymentID: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
                paymentStatus: "Submitted",
            }),
        ).rejects.toThrowError(Error);
    });

    test("getWalletBalance - Returns wallet ballence on successfull response from dotcy", async () => {
        const output = "10000";
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Payment/CustomerAdvancePaymentBalance/"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.payments.getWalletBalance("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        expect(result).toEqual(10000);
    });

    test("getWalletBalance - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Payment/CustomerAdvancePaymentBalance/"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.payments.getWalletBalance("xxxxxxxxxxxxxxxxxxxxxxx");
        expect(result).toEqual(0);
    });

    test("cancelPayment - Cancels payment successfully on dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/Cancel"))
            .reply(httpStatus.OK);
        const result = await dotcy.payments.cancelPayment({
            paymentID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        });
        expect(result).toBeTruthy();
    });

    test("cancelPayment - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/Cancel"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.payments.cancelPayment({
                paymentID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });
});
