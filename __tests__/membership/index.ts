import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";
import Error from "../../src/helpers/Error";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Membership methods", () => {
    test("getMembershipTypes - Returns pay methods on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_TYPES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.membership.getMembershipTypes();
        expect(result).toEqual({});
    });

    test("getMembershipTypes - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_TYPES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.membership.getMembershipTypes();
        expect(result).toEqual([]);
    });

    test("getSingleCustomerMembershipInfo - Returns pay methods on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/GetSingleCustomerMembershipInfo"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.membership.getSingleCustomerMembershipInfo("a");
        expect(result).toEqual({});
    });

    test("getSingleCustomerMembershipInfo - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/GetSingleCustomerMembershipInfo"))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.membership.getSingleCustomerMembershipInfo("a");
        expect(result).toEqual([]);
    });

    test("getCustomerMembershipInfo - Returns pay methods on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/CustomersMembershipInfo"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.membership.getCustomerMembershipInfo({
            profileIDs: "",
            profileType: "",
            retrieveOnlyActiveMembers: false,
        });
        expect(result).toEqual({});
    });

    test("getCustomerMembershipInfo - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/CustomersMembershipInfo"))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.membership.getCustomerMembershipInfo({
            profileIDs: "",
            profileType: "",
            retrieveOnlyActiveMembers: false,
        });
        expect(result).toEqual([]);
    });

    test("getMembershipTransitionTypes - Returns pay methods on successful response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_TRANSITION_TYPES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.membership.getMembershipTransitionTypes();
        expect(result).toEqual({});
    });

    test("getMembershipTransitionTypes - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_TRANSITION_TYPES))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.membership.getMembershipTransitionTypes();
        expect(result).toEqual([]);
    });

    test("getLinkedMembership - Returns pay methods on successful response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/GetLinkedMemberships"))
            .reply(httpStatus.OK, []);
        const result = await dotcy.membership.getLinkedMembership("a");
        expect(result).toEqual([]);
    });

    test("getLinkedMembership - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/GetLinkedMemberships"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.membership.getLinkedMembership("a");
        expect(result).toEqual([]);
    });

    test("sendMemberRegistrationInvite - Returns pay methods on successful response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_SEND_MEMB_REG_INVITE))
            .reply(httpStatus.OK, output);
        const result = await dotcy.membership.sendMemberRegistrationInvite(
            {
                ParentMembershipID: "xxx",
                InviteeProfile: {
                    ID: "xxxx",
                    CustomerType: "xxx",
                    Name: "xxx",
                },
                InviteeEmail: "xxx",
            },
            "127.0.0.1",
        );
        expect(result).toEqual(output);
    });

    test("sendMemberRegistrationInvite - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_SEND_MEMB_REG_INVITE))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.membership.sendMemberRegistrationInvite(
                {
                    ParentMembershipID: "xxx",
                    InviteeProfile: {
                        ID: "xxxx",
                        CustomerType: "xxx",
                        Name: "xxx",
                    },
                    InviteeEmail: "xxx",
                },
                "127.0.0.1",
            ),
        ).rejects.toThrowError(Error);
    });

    test("getBenefitConsumption - Returns pay methods on successful response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/BenefitConsumption"))
            .reply(httpStatus.OK, []);
        const result = await dotcy.membership.getBenefitConsumption("a");
        expect(result).toEqual([]);
    });

    test("getBenefitConsumption - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Memberships/BenefitConsumption"))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.membership.getBenefitConsumption("a");
        expect(result).toEqual([]);
    });

    test("addLinkedMember - Returns json array successfull response from dotcy", async () => {
        const output = ["a", "b"];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_ADD_LINKED_MEMBER))
            .reply(httpStatus.OK, output);
        const result = await dotcy.membership.addLinkedMembers({
            PrimaryMembershipID: "xxx",
            LinkedProfile: {
                ID: "xxxx",
                CustomerType: "xxx",
                Name: "xxx",
            },
        });
        expect(result).toEqual(output);
    });

    test("addLinkedMember - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_ADD_LINKED_MEMBER))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.membership.addLinkedMembers({
                PrimaryMembershipID: "xxx",
                LinkedProfile: {
                    ID: "xxxx",
                    CustomerType: "xxx",
                    Name: "xxx",
                },
            }),
        ).rejects.toThrowError(Error);
    });

    test("calculateMembershipAddOnPrice - Returns json array successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_ADD_ON_PRICE))
            .reply(httpStatus.OK, output);
        const result = await dotcy.membership.calculateMembershipAddOnPrice({
            calculationType: "AddOnMember",
            request: {
                MembershipID: "xxx",
                ProductVariantID: "xxxxx",
                CurrencyID: "xxxx",
            },
        });
        expect(result).toEqual(output);
    });

    test("calculateMembershipAddOnPrice - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_ADD_ON_PRICE))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.membership.calculateMembershipAddOnPrice({
                calculationType: "AddOnMember",
                request: {
                    MembershipID: "xxx",
                    ProductVariantID: "xxxxx",
                    CurrencyID: "xxxx",
                },
            }),
        ).rejects.toThrowError(Error);
    });

    test("validateInvitation - Returns json array successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/ValidateInvitation"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.membership.validateInvitation(
            "xxxxx-xxxx-xxxxxxxx-xxxxxxxx",
            "xxxxxxxxxxxxxx",
        );
        expect(result).toEqual(output);
    });

    test("validateInvitation - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/ValidateInvitation"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.membership.validateInvitation("xxxxxxxxxxxxxx", "xxxxxxxxxxxxxx"),
        ).rejects.toThrowError(Error);
    });

    test("memberInvitationUpdate - Returns event on successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/MemberInvitationUpdate"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.membership.memberInvitationUpdate({
            RequestID: "xxxx",
            Reason: "test",
            ProfileID: "00000000-0000-0000-0000-000000000000",
            Action: "Cancel",
        });
        expect(result).toEqual(output);
    });

    test("memberInvitationUpdate - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/MemberInvitationUpdate"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.membership.memberInvitationUpdate({
                RequestID: "xxxx",
                Reason: "test",
                ProfileID: "00000000-0000-0000-0000-000000000000",
                Action: "Cancel",
            }),
        ).rejects.toThrow(Error);
    });

    test("calculateMembershipUpgradePrice - Returns json array successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_UPGRADE_PRICE))
            .reply(httpStatus.OK, output);
        const result = await dotcy.membership.calculateMembershipUpgradePrice({
            calculationType: "TypeTransition",
            request: {
                MembershipID: "xxx",
                ProductVariantID: "xxxxx",
                CurrencyID: "xxxx",
            },
        });
        expect(result).toEqual(output);
    });

    test("calculateMembershipUpgradePrice - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_MEMBERSHIP_CALC_MEM_UPGRADE_PRICE))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.membership.calculateMembershipUpgradePrice({
            calculationType: "TypeTransition",
            request: {
                MembershipID: "xxx",
                ProductVariantID: "xxxxx",
                CurrencyID: "xxxx",
            },
        });

        expect(result).toEqual({});
    });
});
