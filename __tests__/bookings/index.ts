import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";
import Error from "../../src/helpers/Error";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Bookings methods", () => {
    test("getCalculatedTax - Returns calculated tax on successfull response from dotcy.", async () => {
        const output = { "00000000-0000-0000-0000-000000000000": 4.5 };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_CALCULATE_TAX))
            .reply(httpStatus.OK, output);
        const result = await dotcy.bookings.getCalculatedTax([
            {
                LineItemID: "00000000-0000-0000-0000-000000000000",
                ProductID: "258f4974-cb80-eb11-a819-00224899bbfd",
                AmountBeforeTax: 30,
                TaxProfileID: "5ddce2cb-c380-eb11-a819-00224899bbfd",
            },
        ]);
        expect(result).toEqual(output);
    });

    test("getCalculatedTax - Handles bad request response from dotcy.", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_CALCULATE_TAX))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.getCalculatedTax([
            {
                LineItemID: "00000000-0000-0000-0000-000000000000",
                ProductID: "258f4974-cb80-eb11-a819-00224899bbfd",
                AmountBeforeTax: 30,
                TaxProfileID: "5ddce2cb-c380-eb11-a819-00224899bbfd",
            },
        ]);
        expect(result).toEqual({});
    });

    test("saveBooking - Returns booking data on successfull response from dotcy", async () => {
        const output = { success: "ok" };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_SAVE_BOOKING))
            .reply(httpStatus.OK, output);

        const result = await dotcy.bookings.saveBooking({
            booking: {
                BookingRecordType: 1,
                PriceListID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                Items: [
                    {
                        BaseProductID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                        ProductVariantID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                        Quantity: 2,
                        EntryTimeSlots: [
                            {
                                Start: "2021-05-04T00:00:00",
                                End: "2021-05-04T00:00:00",
                            },
                        ],
                    },
                ],
                CurrencyID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                SendCommunicationsUponFulfillment: ["a", "b"],
                Customer: {
                    ID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    CustomerType: "Contact",
                },
            },
        });
        expect(result).toEqual(output);
    });

    test("saveBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_SAVE_BOOKING))
            .reply(httpStatus.BAD_REQUEST);

        await expect(
            dotcy.bookings.saveBooking({
                booking: {
                    BookingRecordType: 1,
                    PriceListID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    Items: [
                        {
                            BaseProductID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                            ProductVariantID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                            Quantity: 2,
                            EntryTimeSlots: [
                                {
                                    Start: "2021-05-04T00:00:00",
                                    End: "2021-05-04T00:00:00",
                                },
                            ],
                        },
                    ],
                    CurrencyID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    SendCommunicationsUponFulfillment: ["a", "b"],
                    Customer: {
                        ID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                        CustomerType: "Contact",
                    },
                },
            }),
        ).rejects.toThrow(Error);
    });

    test("updateBookingAddress - Returns boolean on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("UpdateBookingAddress"))
            .reply(httpStatus.OK);
        const result = await dotcy.bookings.updateBookingAddress({
            address: {
                IsBillToAddress: true,
                IsDeliverToAddress: true,
                ContactName: "XXXXXXX",
                Street1: "XXXXXXX",
                ZipOrPostalCode: "XXXXXXX",
                City: "XXXXXXX",
                Country: "XXXXXXX",
                PhoneNumber: "XXXXXXX",
            },
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toBeTruthy();
    });

    test("updateBookingAddress - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("UpdateBookingAddress"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.updateBookingAddress({
                address: {
                    IsBillToAddress: true,
                    IsDeliverToAddress: true,
                    ContactName: "XXXXXXX",
                    Street1: "XXXXXXX",
                    ZipOrPostalCode: "XXXXXXX",
                    City: "XXXXXXX",
                    Country: "XXXXXXX",
                    PhoneNumber: "XXXXXXX",
                },
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrow(Error);
    });

    test("addOrUpdateBookingItem - Returns uuid string on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Booking/AddOrUpdateProductToBooking"))
            .reply(httpStatus.OK, "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        const result = await dotcy.bookings.addOrUpdateBookingItem({
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            bookingItem: {
                ID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                BaseProductID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                ProductVariantID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                Quantity: 3,
                EntryTimeSlots: [
                    {
                        Start: "2021-05-07T03:24:44.723Z",
                        End: "2021-05-07T03:24:44.723Z",
                    },
                ],
            },
        });
        expect(result).toBeTruthy();
    });

    test("addOrUpdateBookingItem - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Booking/AddOrUpdateProductToBooking"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.addOrUpdateBookingItem({
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                bookingItem: {
                    ID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    BaseProductID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    ProductVariantID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    Quantity: 3,
                    EntryTimeSlots: [
                        {
                            Start: "2021-05-07T03:24:44.723Z",
                            End: "2021-05-07T03:24:44.723Z",
                        },
                    ],
                },
            }),
        ).rejects.toThrowError(Error);
    });

    test("removeBookingItem - Returns true on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("Booking/RemoveProduct"))
            .reply(httpStatus.OK, "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        const result = await dotcy.bookings.removeBookingItem({
            bookingID: "",
            bookingItemID: "",
        });
        expect(result).toBeTruthy();
    });

    test("removeBookingItem - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("Booking/RemoveProduct"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.removeBookingItem({
                bookingID: "",
                bookingItemID: "",
            }),
        ).rejects.toThrowError(Error);
    });

    test("getBookingById - Returns true on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.getBookingById("xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getBookingById - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.getBookingById("xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getBookingByRefNo - Returns booking data on successful response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/bookingByRefAndEmail/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.getBookingByRefNo(
            "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        );
        expect(result).toEqual({});
    });

    test("getBookingByRefNo - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/bookingByRefAndEmail/"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.getBookingByRefNo(
            "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        );
        expect(result).toEqual({});
    });

    test("abandonBooking - Marks a booking as abondoned successfully in dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/Abandon"))
            .reply(httpStatus.OK);
        const result = await dotcy.bookings.abandonBooking("xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        expect(result).toBeTruthy();
    });

    test("abandonBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/Abandon"))
            .reply(httpStatus.BAD_REQUEST);

        await expect(
            dotcy.bookings.abandonBooking("xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx"),
        ).rejects.toThrowError(Error);
    });

    test("cancelBooking - Marks a booking as cancelled successfully in dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/Cancel"))
            .reply(httpStatus.OK);
        const result = await dotcy.bookings.cancelBooking("xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        expect(result).toBeTruthy();
    });

    test("cancelBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/Cancel"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.cancelBooking("xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx"),
        ).rejects.toThrowError(Error);
    });

    test("extendExpiration - Extends time successfully on dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .patch((uri) => uri.includes("/ExtendExpiration"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.extendExpiration({
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            periodInMinutes: "60",
        });
        expect(result).toEqual({});
    });

    test("extendExpiration - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .patch((uri) => uri.includes("/ExtendExpiration"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.extendExpiration({
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                periodInMinutes: "60",
            }),
        ).rejects.toThrowError(Error);
    });

    test("removeTicketholderFromBookingItem - Removes ticket holder successfully on dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("Booking/RemoveTicketHolderFromBookingItem"))
            .reply(httpStatus.OK);
        const result = await dotcy.bookings.removeTicketholderFromBookingItem({
            ticketHolderID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            bookingItemID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toBeTruthy();
    });

    test("removeTicketholderFromBookingItem - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("Booking/RemoveTicketHolderFromBookingItem"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.removeTicketholderFromBookingItem({
                ticketHolderID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                bookingItemID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("addOrUpdateTicketholderFromBookingItem - Adds or Updates ticketholders successfully on dotcy", async () => {
        const output = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Booking/AddOrUpdateTicketHolder"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.bookings.addOrUpdateTicketholderFromBookingItem({
            ticketHolder: {
                CustomerID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                CustomerType: "Contact",
                NumberOfTickets: 1,
                MemberTypeID: "xxx",
            },
            bookingItemID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("addOrUpdateTicketholderFromBookingItem - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Booking/AddOrUpdateTicketHolder"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.addOrUpdateTicketholderFromBookingItem({
                ticketHolder: {
                    CustomerID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    CustomerType: "Contact",
                    NumberOfTickets: 1,
                    MemberTypeID: "xxx",
                },
                bookingItemID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("getBookingFromPaymentID - Returns booking data on successfull response from dotcy", async () => {
        const output = { a: "a", b: "b" };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/BookingFromPaymentID"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.bookings.getBookingFromPaymentID({
            paymentID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("getBookingFromPaymentID - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/BookingFromPaymentID"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.getBookingFromPaymentID({
            paymentID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toEqual({});
    });

    test("searchTickets - Returns booking data on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Booking/SearchTickets"))
            .reply(httpStatus.OK, []);
        const result = await dotcy.bookings.searchTickets({
            ProfileID: "xxxxxxxxxxxxxxx",
            OnlyActiveTickets: true,
            MembershipIDs: ["xxxxxx-xxxxxx"],
        });
        expect(result).toEqual([]);
    });

    test("searchTickets - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("Booking/SearchTickets"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.searchTickets({
            ProfileID: "xxxxxxxxxxxxxxx",
            OnlyActiveTickets: true,
        });
        expect(result).toEqual([]);
    });

    test("searchBookings - Returns bookings on successfull response from dotcy", async () => {
        const output = [{ a: "a", b: "b" }];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_SEARCH_BOOKINGS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.bookings.searchBookings({
            searchParams: {
                ProfileID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                StateCodes: ["Active"],
                StatusCodes: ["ActiveInProgress"],
            },
        });
        expect(result).toEqual(output);
    });

    test("searchBookings - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_SEARCH_BOOKINGS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.searchBookings({
            searchParams: {
                ProfileID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                StateCodes: ["Active"],
                StatusCodes: ["ActiveInProgress"],
            },
        });
        expect(result).toEqual([]);
    });

    test("fulfillBooking - Fulfills a booking successfully on dotcy", async () => {
        const output = "a";
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/Fulfill"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.bookings.fulfillBooking({
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("fulfillBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/Fulfill"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.fulfillBooking({
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrow(Error);
    });

    test("setCustomer - Fulfills a booking successfully on dotcy", async () => {
        const output = "a";
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/SetCustomer"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.bookings.setCustomer({
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            customerID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        });
        expect(result).toBeTruthy();
    });

    test("setCustomer - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/SetCustomer"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.setCustomer({
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                customerID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrow(Error);
    });

    test("getBookingByTicketId - Returns booking object on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/TicketByID/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.getBookingByTicketId("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getBookingByTicketId - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/TicketByID/"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.getBookingByTicketId(
            "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
        );
        expect(result).toEqual({});
    });

    test("saveGuestDetails - Returns response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("Booking/SaveBookingGuestDetails/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.saveGuestDetails({
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            GuestDetails: {
                AdditionalInfoFullName: "string",
                AdditionalInfoEmail: "string",
                AdditionalInfoPhone: "string",
            },
        });
        expect(result).toEqual({});
    });

    test("saveGuestDetails - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("Booking/SaveBookingGuestDetails/"))
            .reply(httpStatus.BAD_REQUEST);

        await expect(
            dotcy.bookings.saveGuestDetails({
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                GuestDetails: {
                    AdditionalInfoFullName: "string",
                    AdditionalInfoEmail: "string",
                    AdditionalInfoPhone: "string",
                },
            }),
        ).rejects.toThrow(Error);
    });

    test("downloadTicket - Returns uuid string on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/"))
            .reply(httpStatus.OK, "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        const result = await dotcy.bookings.downloadTicket({
            bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
            ticketIDs: [],
            returnOnlyAvailableTickets: true,
            returnInBased64: true,
        });
        expect(result).toBeTruthy();
    });

    test("downloadTicket - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.downloadTicket({
                bookingID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                ticketIDs: [],
                returnOnlyAvailableTickets: true,
                returnInBased64: true,
            }),
        ).rejects.toThrowError(Error);
    });

    test("getElectronicTicket - Returns uuid string on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/ElectronicTicket/"))
            .reply(httpStatus.OK, "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx");
        const result = await dotcy.bookings.getElectronicTicket("xxxx", "xxx");
        expect(result).toBeTruthy();
    });

    test("getElectronicTicket - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Booking/ElectronicTicket/"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.bookings.getElectronicTicket("xxxx", "xxx");
        expect(result).toEqual("");
    });

    test("splitBooking - Returns uuid string on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("Booking/Split/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.splitBooking({
            bookingID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            request: {
                BookingItemIDs: ["00000000-0000-0000-0000-000000000000"],
            },
        });
        expect(result).toEqual({});
    });

    test("splitBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("Booking/Split/"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.splitBooking({
                bookingID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                request: {
                    BookingItemIDs: ["00000000-0000-0000-0000-000000000000"],
                },
            }),
        ).rejects.toThrowError(Error);
    });

    test("mergeBooking - Returns uuid string on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("Booking/Merge/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.mergeBooking({
            bookingID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            request: {
                SecondaryBookingIDs: ["00000000-0000-0000-0000-000000000000"],
            },
        });
        expect(result).toBeTruthy();
    });

    test("mergeBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("Booking/Merge/"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.mergeBooking({
                bookingID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                request: {
                    SecondaryBookingIDs: ["00000000-0000-0000-0000-000000000000"],
                },
            }),
        ).rejects.toThrowError(Error);
    });

    test("validateBooking - Returns uuid string on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_VALIDATION))
            .reply(httpStatus.OK, {});
        const result = await dotcy.bookings.validateBooking({
            Booking: {
                BookingRecordType: 1,
                PriceListID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                Items: [
                    {
                        BaseProductID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                        ProductVariantID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                        Quantity: 2,
                        EntryTimeSlots: [
                            {
                                Start: "2021-05-04T00:00:00",
                                End: "2021-05-04T00:00:00",
                            },
                        ],
                    },
                ],
                CurrencyID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                SendCommunicationsUponFulfillment: ["a", "b"],
                Customer: {
                    ID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    CustomerType: "Contact",
                },
            },
            CustomerEmailAddress: "",
        });
        expect(result).toBeTruthy();
    });

    test("validateBooking - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_BOOKING_VALIDATION))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.bookings.validateBooking({
                Booking: {
                    BookingRecordType: 1,
                    PriceListID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    Items: [
                        {
                            BaseProductID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                            ProductVariantID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                            Quantity: 2,
                            EntryTimeSlots: [
                                {
                                    Start: "2021-05-04T00:00:00",
                                    End: "2021-05-04T00:00:00",
                                },
                            ],
                        },
                    ],
                    CurrencyID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                    SendCommunicationsUponFulfillment: ["a", "b"],
                    Customer: {
                        ID: "xxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxxx",
                        CustomerType: "Contact",
                    },
                },
                CustomerEmailAddress: "",
            }),
        ).rejects.toThrowError(Error);
    });
});
