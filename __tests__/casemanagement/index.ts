import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("CaseManagement Class Test suite", () => {
    const output = {};
    test("getCaseTypes - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, [])
            .get((uri) => uri.includes(constants.ENDPOINT_CASE_MANAGEMENT_GET_CASE_TYPES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.casemanagement.getCaseTypes();
        expect(result).toEqual(output);
    });

    test("getCaseTypes - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_CASE_MANAGEMENT_GET_CASE_TYPES))
            .reply(httpStatus.BAD_REQUEST);
        await expect(dotcy.casemanagement.getCaseTypes()).rejects.toThrowError(Error);
    });

    test("searchCases - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, [])
            .post((uri) => uri.includes(constants.ENDPOINT_CASE_MANAGEMENT_SEARCH_CASES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.casemanagement.searchCases({
            CustomerID: "XXXXXXXXXXXXXXXXXXXX",
        });
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual({});
    });

    test("searchCases - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_CASE_MANAGEMENT_SEARCH_CASES))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.casemanagement.searchCases({
                CustomerID: "xxxxxxxxxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("createCase - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_CASE_MANAGEMENT_CREATE_CASE))
            .reply(httpStatus.OK, {});
        const result = await dotcy.casemanagement.createCase({
            CustomerID: "xxxxxxxxxxxxxxxxx",
        });
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual({});
    });

    test("createCase - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_CASE_MANAGEMENT_CREATE_CASE))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.casemanagement.createCase({
                CustomerID: "xxxxxxxxxxxxxxxxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("getCaseDetails - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("CaseManagement/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.casemanagement.getCaseDetails("xxxxxxxx");
        expect(result).toEqual({});
    });

    test("getCaseDetails - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("CaseManagement/"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(dotcy.casemanagement.getCaseDetails("xxxxxxxxxxxxxx")).rejects.toThrowError(
            Error,
        );
    });
});
