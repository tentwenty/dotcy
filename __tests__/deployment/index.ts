import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";
import Error from "../../src/helpers/Error";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Deployment methods", () => {
    test("getChannelInfo - Returns successfull response from dotcy", async () => {
        const output = { Values: { "00000000-0000-0000-0000-000000000000": [] } };
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_DEPLOYMENT_CHANNEL_INFO))
            .reply(httpStatus.OK, output);
        const result = await dotcy.deployment.getChannelInfo();
        expect(result).toEqual(output);
    });

    test("getChannelInfo - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_DEPLOYMENT_CHANNEL_INFO))
            .reply(httpStatus.BAD_REQUEST);

        expect(dotcy.deployment.getChannelInfo()).rejects.toThrow(Error);
    });
});
