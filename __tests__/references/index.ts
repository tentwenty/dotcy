import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "v2",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("References Class Test suite", () => {
    test("getCountries - Returns countries on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_COUNTRIES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getCountries();
        expect(result).toEqual({});
    });
    test("getCountries - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_COUNTRIES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getCountries();
        expect(result).toEqual([]);
    });
    test("getVenueAccessAreas - Returns venue areas on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_VENUE_ACCESS_AREAS))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getVenueAccessAreas("xxxx");

        expect(result).toEqual([]);
    });
    test("getVenueAccessAreas - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_VENUE_ACCESS_AREAS))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getVenueAccessAreas("xxxx");
        expect(result).toEqual([]);
    });

    test("getStates - Returns states on successfull response from dotcy pagkage", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_STATES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getStates();
        expect(result).toEqual({});
    });
    test("getStates - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_STATES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getStates();
        expect(result).toEqual([]);
    });
    test("getTargetGroups - Returns target groups on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_TARGET_GROUPS))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getTargetGroups();
        expect(result).toEqual({});
    });
    test("getTargetGroups - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_TARGET_GROUPS))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getTargetGroups();
        expect(result).toEqual([]);
    });

    test("getVenueAccessAreas - Returns event on successfull response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("References/VenueAccessAreas/"))
            .reply(httpStatus.OK, []);
        const result = await dotcy.references.getVenueAccessAreas("xxxxxxxxxxxx");
        expect(result).toEqual(output);
    });

    test("getVenueAccessProfiles - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("References/VenueAccessAreas/"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.references.getVenueAccessAreas("xxxxxxxxxxxx");
        expect(result).toEqual([]);
    });
    test("getEducationalLevels - Returns target groups on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_EDUCATIONAL_LEVELS))
            .reply(httpStatus.OK, []);
        const result = await dotcy.references.getEducationalLevels();
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual([]);
    });
    test("getEducationalLevels - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_EDUCATIONAL_LEVELS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.references.getEducationalLevels();
        expect(result).toEqual([]);
    });
    test("getTags - Returns target groups on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_TAGS))
            .reply(httpStatus.OK, []);
        const result = await dotcy.references.getTags();
        expect(result).toEqual([]);
    });
    test("getTags - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_TAGS))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getTags();
        expect(result).toEqual([]);
    });
    test("getAgeRanges - Returns age range on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_AGE_RANGE))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getAgeRanges();
        expect(result).toEqual({});
    });
    test("getAgeRanges - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_AGE_RANGE))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getAgeRanges();
        expect(result).toEqual([]);
    });
    test("getChannelCurrencies - Returns currencies on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_CH_CURRENCIES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getChannelCurrencies();
        expect(result).toEqual({});
    });
    test("getChannelCurrencies - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_CH_CURRENCIES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getChannelCurrencies();
        expect(result).toEqual([]);
    });
    test("getPersonalInterests - Returns personal interests on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_PER_INTERESTS))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getPersonalInterests();
        expect(result).toEqual({});
    });
    test("getPersonalInterests - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_PER_INTERESTS))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getPersonalInterests();
        expect(result).toEqual([]);
    });
    test("getLanguages - Returns languages on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_LANGUAGES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getLanguages();
        expect(result).toEqual({});
    });
    test("getLanguages - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_LANGUAGES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getLanguages();
        expect(result).toEqual([]);
    });
    test("getChannelPriceLists - Returns channel pricelist on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_CH_PRICELIST))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getChannelPriceLists();
        expect(result).toEqual({});
    });
    test("getLanguages - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_CH_PRICELIST))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getChannelPriceLists();
        expect(result).toEqual([]);
    });
    test("getTaxProfiles - Returns tax profiles on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_TAX_PROFILES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getTaxProfiles(
            ["aaaaaaaaaaaaaa", "bbbbbbbbbbbb"],
            "Default",
        );
        expect(result).toEqual({});
    });
    test("getTaxProfiles - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_TAX_PROFILES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getTaxProfiles();
        expect(result).toEqual([]);
    });

    test("getVisitorCategories - Returns tax profiles on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_VISITOR_CATEGORIES))
            .reply(httpStatus.OK, {});
        const result = await dotcy.references.getVisitorCategories();
        expect(result).toEqual({});
    });
    test("getVisitorCategories - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_REFERENCES_VISITOR_CATEGORIES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.references.getVisitorCategories();
        expect(result).toEqual([]);
    });
});
