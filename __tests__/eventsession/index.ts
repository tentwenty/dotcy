import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "v2",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});
const scope = nock("https://example.com", { allowUnmocked: true });

describe("Event Session Class Test suite", () => {
    test("getEventSessions - Returns session array on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SESSION_SEARCH_EVENT_SESSION))
            .reply(httpStatus.OK, []);
        const result = await dotcy.eventsession.getEventSessions({
            EventIDs: ["xxxxxxxxxxxxx"],
        });
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual([]);
    });

    test("getEventSessions - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SESSION_SEARCH_EVENT_SESSION))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.eventsession.getEventSessions({
            EventIDs: ["xxxxxxxxxxxxxx"],
        });
        expect(result).toEqual([]);
    });

    test("getSessionsSlots - Returns session array on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SESSION_SEARCH_SESSION_SLOTS))
            .reply(httpStatus.OK, []);
        const result = await dotcy.eventsession.getSessionsSlots({
            EventSessionID: "xxxxxxxxxxxx",
            caching: "Default",
        });
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual([]);
    });

    test("getSessionsSlots - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SESSION_SEARCH_SESSION_SLOTS))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.eventsession.getSessionsSlots({
            EventSessionID: "xxxxxxxxxxx",
        });
        expect(result).toEqual([]);
    });

    test("getRemainingCapacity - Returns session array on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("EventSession/RemainingCapacity"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.eventsession.getRemainingCapacity("xxxxxxxxxxxxxxxxx");
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual({});
    });

    test("getRemainingCapacity - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("EventSession/RemainingCapacity"))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.eventsession.getRemainingCapacity("xxxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getSessionProducts - Returns product sessions array on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SESSION_PRODUCTS))
            .reply(httpStatus.OK, []);
        const result = await dotcy.eventsession.getSessionProducts({
            EventID: "xxxxxxxxxxx",
        });
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual([]);
    });

    test("getSessionProducts - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SESSION_PRODUCTS))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.eventsession.getSessionProducts({
            EventID: "xxxxxxxxxxxxxx",
        });
        expect(result).toEqual([]);
    });
});
