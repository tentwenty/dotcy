import Router from "../../src/helpers/Router";

const router = new Router({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "v2",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

describe("Generic test suite", () => {
    test("Resolve normal urls", () => {
        const key = "Product/VariantTypeOptions";
        expect(router.getRoute(key)).toContain("api/v2");
        expect(router.getRoute(key)).toContain(key);
    });

    test("Resolve login urls", () => {
        const key = "token";
        expect(router.getRoute(key)).not.toContain("/v2");
        expect(router.getRoute(key)).toContain(key);
    });
});
