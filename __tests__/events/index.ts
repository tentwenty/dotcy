import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Events Class Test suite", () => {
    test("getEventById - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Event/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.events.getEventById("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual({});
    });

    test("getEventById - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Event/"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.events.getEventById("xxxxxxxxxxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getEventSetUp - Returns json array successful response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, [])
            .get((uri) => uri.includes(constants.ENDPOINT_EVENT_SETUP))
            .reply(httpStatus.OK, output);
        const result = await dotcy.events.getEventSetUp();
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual(output);
    });

    test("getEventSetUp - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, [])
            .get((uri) => uri.includes(constants.ENDPOINT_EVENT_SETUP))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.events.getEventSetUp();
        expect(result).toEqual([]);
    });

    test("searchEvents - Returns events", async () => {
        const output = [
            { ValidFrom: "2021-09-15T08:00:00+03:00", ValidTo: "2025-09-15T16:00:00+03:00" },
        ];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SEARCH))
            .reply(httpStatus.OK, output);
        const result = await dotcy.events.searchEvents({
            EventIDs: ["765ed0e2-a981-eb11-a819-00224899bbfd"],
        });
        expect(result).toEqual(output);
    });

    test("searchEvents - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_EVENT_SEARCH))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.events.searchEvents({
            EventIDs: ["765ed0e2-a981-eb11-a819-00224899bbfd"],
        });
        expect(result).toEqual([]);
    });
});
