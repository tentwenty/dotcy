import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Communications Class Test suite", () => {
    test("generateOTP - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/GenerateOTP"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.communications.generateOTP({
            profileID: "xxxxxxxxxx",
            OTPTypeCode: "xxxxxxx",
            profileType: "xxxxxxxx",
        });
        expect(result).toEqual({});
    });

    test("generateOTP - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/GenerateOTP"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.communications.generateOTP({
                profileID: "xxxxxxxxxx",
                OTPTypeCode: "xxxxxxxxx",
                profileType: "xxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("verifyOTP - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/VerifyOTP"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.communications.verifyOTP({
            profileID: "xxxxxx",
            OTPTypeCode: "xxxxxxxx",
            profileType: "xxxxxxxx",
            OTPCode: "xxxxxxxx",
        });
        expect(result).toEqual({});
    });

    test("verifyOTP - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/VerifyOTP"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.communications.verifyOTP({
                profileID: "xxxxxxxxxx",
                OTPTypeCode: "xxxxxxxx",
                profileType: "xxxxxxxxx",
                OTPCode: "xxxxxxxx",
            }),
        ).rejects.toThrowError(Error);
    });

    test("sendActivity - Returns response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_COMMUNICATIONS_SEND_ACTIVITY))
            .reply(httpStatus.OK, {});
        const result = await dotcy.communications.sendActivity({
            request: {
                AlwaysSend: true,
                Subject: "string",
                To: [
                    {
                        ID: "00000000-0000-0000-0000-000000000000",
                        Name: "string",
                        PartyType: "Contact",
                    },
                ],
                ActivityType: "Any",
                IsUnicode: true,
                LanguageID: "00000000-0000-0000-0000-000000000000",
                Priority: "Low",
                BodyContent: "string",
                BodyContentType: "string",
            },
        });
        expect(result).toEqual({});
    });

    test("sendActivity - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_COMMUNICATIONS_SEND_ACTIVITY))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.communications.sendActivity({
                request: {
                    AlwaysSend: true,
                    Subject: "string",
                    To: [
                        {
                            ID: "00000000-0000-0000-0000-000000000000",
                            Name: "string",
                            PartyType: "Contact",
                        },
                    ],
                    ActivityType: "Any",
                    IsUnicode: true,
                    LanguageID: "00000000-0000-0000-0000-000000000000",
                    Priority: "Low",
                    BodyContent: "string",
                    BodyContentType: "string",
                },
            }),
        ).rejects.toThrowError(Error);
    });

    test("getCustomNotifications - Returns successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_COMMUNICATIONS_GET_CUST_NOTIFICATIONS))
            .reply(httpStatus.OK, []);
        const result = await dotcy.communications.getCustomNotifications("2021-07-10 10:00:00 AM");
        expect(result).toEqual([]);
    });

    test("getCustomNotifications - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_COMMUNICATIONS_GET_CUST_NOTIFICATIONS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.communications.getCustomNotifications("2021-07-10 10:00:00 AM");
        expect(result).toEqual([]);
    });

    test("getBulkNotification - Returns successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Communications/BulkCustomNotification"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.communications.getBulkNotification("XXXXXXXXXXXXXXXXXXXXXXXXX");
        expect(result).toEqual({});
    });

    test("getBulkNotification - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Communications/BulkCustomNotification"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.communications.getBulkNotification("XXXXXXXXXXXXXXXXXXXXXXXXX");
        expect(result).toEqual({});
    });

    test("sendTicketsToTicketHolder - Returns successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/SendTicketToTicketHolder"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.communications.sendTicketsToTicketHolder(
            "xxxx-xxx-xxx",
            "xxxxx-xxxx-xxxx",
        );
        expect(result).toEqual({});
    });

    test("sendTicketsToTicketHolder - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/SendTicketToTicketHolder"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.communications.sendTicketsToTicketHolder(
            "xxxx-xxx-xxx",
            "xxxxx-xxxx-xxxx",
        );
        expect(result).toEqual({});
    });
});
