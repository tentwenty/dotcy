import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";
import Error from "../../src/helpers/Error";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Profile Class Test suite", () => {
    test("searchProfiles - Returns json array successfull response from dotcy", async () => {
        const output = ["a", "b"];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PROFILE_SEARCH_PROFILES))
            .reply(httpStatus.OK, output);
        const result = await dotcy.profile.searchProfiles({
            Email: "xxxx@xxxx.xx",
            IncludeInactiveRecords: false,
            CustomerType: "xxxxx",
            SelectColumns: {
                Contact: ["xxxxxxx", "xxxxxxx"],
            },
        });
        expect(result).toEqual(output);
    });

    test("searchProfiles - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PROFILE_SEARCH_PROFILES))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.profile.searchProfiles({
            Email: "xxxx@xxxxx.xx",
            IncludeInactiveRecords: false,
            CustomerType: "xxxxxx",
            SelectColumns: {
                Contact: ["xxxxxx", "xxxxxx"],
            },
        });
        expect(result).toEqual([]);
    });

    test("updateProfiles - Returns user object on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PROFILE_UPDATE_PROFILE))
            .reply(httpStatus.OK, {});
        const result = await dotcy.profile.updateProfile({
            ID: "xxxxxxxxxxxxxxxxxxxxxx",
            CustomerType: "xxxxxx",
            Values: {
                birthdate: {
                    ValueType: "xxxxxxx",
                    Value: "xxxx-xx-xx",
                },
            },
        });
        expect(result).toEqual({});
    });

    test("updateProfiles - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PROFILE_UPDATE_PROFILE))
            .reply(httpStatus.BAD_REQUEST, {});
        await expect(
            dotcy.profile.updateProfile({
                ID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                CustomerType: "xxxxx",
                Values: {
                    birthdate: {
                        ValueType: "xxxxxxxxxx",
                        Value: "xxxx-xx-xx",
                    },
                },
            }),
        ).rejects.toThrow(Error);
    });

    test("getProfileById - Returns profile on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Profile/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.profile.getProfileById("xxxxxxxxxxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getProfileById - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Profile/"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(dotcy.profile.getProfileById("xxxxxxxxxxxxxxxxxxxxxxx")).rejects.toThrow(
            Error,
        );
    });

    test("addOrUpdateTicketDeliveryAddress - Returns json object successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PROFILE_ADD_OR_UPDATE_TICKET_DETAILS))
            .reply(httpStatus.OK, {});
        const result = await dotcy.profile.addOrUpdateTicketDeliveryAddress({
            ID: "xxxxxxxxxxxxxxxx",
            Name: "xxxxxxxxxxxxxxxxx",
            Customer: {
                LogicalName: "xxxxxxxxxxx",
                ID: "xxxxxxxxxxxxxxxx",
                Name: "xxxxxxxxxxxxx",
            },
            Stree1: "xxxxxxxxx",
            Stree2: "xxxxxxxx",
            Stree3: "xxxxxxxxxxx",
            CityOtTown: "xxxxxxxx",
            ZipOrPostalCode: "xxxxxxxxx",
            Country: {
                ID: "xxxxxxxxxxxxx",
                Name: "xxxxxxxxx",
            },
            POBox: "xxxxxxxxx",
        });
        expect(result).toEqual({});
    });

    test("addOrUpdateTicketDeliveryAddress - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PROFILE_ADD_OR_UPDATE_TICKET_DETAILS))
            .reply(httpStatus.BAD_REQUEST, {});
        await expect(
            dotcy.profile.addOrUpdateTicketDeliveryAddress({
                ID: "xxxxxxxxxxxxxxxx",
                Name: "xxxxxxxxxxxxxxxxx",
                Customer: {
                    LogicalName: "xxxxxxxxxxx",
                    ID: "xxxxxxxxxxxxxxxx",
                    Name: "xxxxxxxxxxxxx",
                },
                Stree1: "xxxxxxxxx",
                Stree2: "xxxxxxxx",
                Stree3: "xxxxxxxxxxx",
                CityOtTown: "xxxxxxxx",
                ZipOrPostalCode: "xxxxxxxxx",
                Country: {
                    ID: "xxxxxxxxxxxxx",
                    Name: "xxxxxxxxx",
                },
                POBox: "xxxxxxxxx",
            }),
        ).rejects.toThrow(Error);
    });

    test("downloadData - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("/DownloadData"))
            .reply(httpStatus.OK, "");
        const result = await dotcy.profile.downloadData({
            profileID: "xxxxxxx",
            profileType: "xxxxxxxxxxx",
            userIPAddress: "xxxxxxxxxxxxxxx",
        });
        expect(result).toEqual("");
    });

    test("downloadData - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("/DownloadData"))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.profile.downloadData({
            profileID: "xxxxxxxxxx",
            profileType: "xxxxxxxxx",
            userIPAddress: "xxxxxxxxxx",
        });
        expect(result).toEqual("");
    });

    test("deleteProfile - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/DeleteProfile"))
            .reply(httpStatus.OK, "");
        const result = await dotcy.profile.deleteProfile({
            profileID: "xxxxxxxxxx",
            userIPAddress: "xxxxxxxxx",
        });
        expect(result).toEqual("");
    });

    test("deleteProfile - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .delete((uri) => uri.includes("/DeleteProfile"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.profile.deleteProfile({
                profileID: "xxxxxxxxxx",
                userIPAddress: "xxxxxxxxxx",
            }),
        ).rejects.toThrow(Error);
    });

    test("revokeDeleteProfile - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/RevokeDeleteRequest"))
            .reply(httpStatus.OK, "");
        const result = await dotcy.profile.revokeDeleteProfile({
            profileID: "xxxxxxxxxx",
            userIPAddress: "xxxxxxxxxxx",
        });
        expect(result).toEqual("");
    });

    test("revokeDeleteProfile - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes("/RevokeDeleteRequest"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.profile.revokeDeleteProfile({
                profileID: "xxxxxxxxxx",
                userIPAddress: "xxxxxxxxxx",
            }),
        ).rejects.toThrow(Error);
    });

    test("getPersonalInterest - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("/ProfileSelectedPersonalInterest"))
            .reply(httpStatus.OK, []);
        const result = await dotcy.profile.getPersonalInterest("xxxxxx");
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual([]);
    });

    test("getPersonalInterest - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("/ProfileSelectedPersonalInterest"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(dotcy.profile.getPersonalInterest("xxxxxx")).rejects.toThrow(Error);
    });

    test("updatePersonalInterest - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/ProfileSelectedPersonalInterest"))
            .reply(httpStatus.OK, []);
        const result = await dotcy.profile.updatePersonalInterest("xxxxxx", ["xxxxx"], ["xxxxx"]);
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual(true);
    });

    test("updatePersonalInterest - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .put((uri) => uri.includes("/ProfileSelectedPersonalInterest"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.profile.updatePersonalInterest("xxxxxx", ["xxxxx"], ["xxxxx"]),
        ).rejects.toThrow(Error);
    });
});
