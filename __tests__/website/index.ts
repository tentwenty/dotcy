import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Website Class Test suite", () => {
    test("loginExtAuthMethod - Returns event on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_WEBSITE_LOGIN_EXT_AUTH))
            .reply(httpStatus.OK, {});
        const result = await dotcy.website.loginExtAuthMethod({
            extAuthMethodID: "xxxxxxx",
            userNameOrCode: "xxxxxx",
            userIPAddress: "xxxxxxxxx",
        });
        expect(result).toEqual({});
    });

    test("loginExtAuthMethod - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_WEBSITE_LOGIN_EXT_AUTH))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.website.loginExtAuthMethod({
                extAuthMethodID: "xxxxxxxxxxx",
                userNameOrCode: "xxxxxxxxxx",
                userIPAddress: "xxxxxx",
            }),
        ).rejects.toThrow(Error);
    });
});
