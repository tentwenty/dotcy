import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Seating methods", () => {
    test("searchSeatingAreas - Returns seating map layout from dotcy", async () => {
        const output = [{ MaxCapacity: 200 }, { MaxCapacity: 20 }];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_SEATING_SEARCH_SEATING_AREAS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.seating.searchSeatingAreas({
            ProductID: "765ed0e2-a981-eb11-a819-00224899bbfd",
        });
        expect(result).toEqual(output);
    });

    test("searchSeatingAreas - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_SEATING_SEARCH_SEATING_AREAS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.seating.searchSeatingAreas({
            ProductID: "765ed0e2-a981-eb11-a819-00224899bbfd",
        });
        expect(result).toEqual([]);
    });

    test("searchSeats - Returns Reserved and booked seats", async () => {
        const output = [{ TicketNumber: "10004064" }, { TicketNumber: "10004065" }];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_SEATING_SEARCH_SEATS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.seating.searchSeats({
            ProductID: "765ed0e2-a981-eb11-a819-00224899bbfd",
        });
        expect(result).toEqual(output);
    });

    test("searchSeats - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_SEATING_SEARCH_SEATS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.seating.searchSeats({
            ProductID: "765ed0e2-a981-eb11-a819-00224899bbfd",
        });
        expect(result).toEqual([]);
    });

    test("getSeatingSection - Returns seating section on successfull response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Seating/SeatingSection/"))
            .reply(httpStatus.OK, {});
        const result = await dotcy.seating.getSeatingSection("xxxxxxxxxxxxxxxxxxxxxxx");
        expect(result).toEqual({});
    });

    test("getSeatingSection - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes("Seating/SeatingSection/"))
            .reply(httpStatus.BAD_REQUEST);
        await expect(dotcy.seating.getSeatingSection("xxxxxxxxxxxxxxxxxxxxxxx")).rejects.toThrow(
            Error,
        );
    });

    test("searchSeatingSections - Returns Reserved and booked seats", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_SEATING_SEARCH_SEATING_SECTIONS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.seating.searchSeatingSections({
            AreaID: "xxxxxx",
        });
        expect(result).toEqual(output);
    });

    test("searchSeatingSections - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_SEATING_SEARCH_SEATING_SECTIONS))
            .reply(httpStatus.BAD_REQUEST);
        const result = await dotcy.seating.searchSeatingSections({
            AreaID: "xxxxxxx",
        });
        expect(result).toEqual([]);
    });
});
