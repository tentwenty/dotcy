import Dotcy from "../../src";
import nock from "nock";
import * as constants from "../../src/constants";
import httpStatus from "http-status";

const dotcy = new Dotcy({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    apiUsername: "XXXXXXXXXX",
    apiPassword: "XXXXXXXXXXXXX",
    apiVersion: "XXXXXXXXXX",
    grantType: "XXXXXXXXXXXX",
    baseUrl: "https://example.com",
    channelId: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
});

const scope = nock("https://example.com", { allowUnmocked: true });

describe("Product Class Test suite", () => {
    test("getProductHierarchy - Returns variants on successfull response from dotcy", async () => {
        const output = [{ a: "test" }];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_HIERARCHY))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getProductHierarchy();
        expect(result).toEqual(output);
    });

    test("getProductHierarchy - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_HIERARCHY))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.products.getProductHierarchy();
        expect(result).toEqual([{}]);
    });

    test("getProductVariants - Returns variants on successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_VARIANTS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getProductVariants({ ProductIDs: [] });
        expect(result).toEqual(output);
    });

    test("getProductVariants - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_VARIANTS))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.products.getProductVariants({ ProductIDs: [] });
        expect(result).toEqual({});
    });

    test("getProductCapacity - Returns json array successful response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_PRODUCT_CAPACITY))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getProductCapacity({
            ProductID: "xxxxxx",
            LoadType: "xxxxx",
        });
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual(output);
    });

    test("getProductCapacity - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_PRODUCT_CAPACITY))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.products.getProductCapacity({
            ProductID: "xxxxxx",
            LoadType: "xxxxx",
        });
        expect(result).toEqual({});
    });

    test("getProductTimeBlocks - Returns json array successful response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, [])
            .get((uri) => uri.includes("/TimeBlocks"))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getProductTimeBlocks(
            "xxxxxxxxxxxxxx",
            "xxxxxxxxxxx",
            "xxxxxxxxxxxxx",
        );
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual(output);
    });

    test("getProductTimeBlocks - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, [])
            .get((uri) => uri.includes("/TimeBlocks"))
            .reply(httpStatus.BAD_REQUEST, []);
        const result = await dotcy.products.getProductTimeBlocks(
            "xxxxxxxxxxxxxx",
            "xxxxxxxxxxx",
            "xxxxxxxxxxxxx",
        );
        expect(result).toEqual([]);
    });

    test("getVariantTypeOptions - Returns variants on successfull response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_VARIANT_TYPE_OPTIONS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getVariantTypeOptions();
        expect(result).toEqual(output);
    });

    test("getVariantTypeOptions - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_VARIANT_TYPE_OPTIONS))
            .reply(httpStatus.BAD_REQUEST, {});
        const result = await dotcy.products.getVariantTypeOptions();
        expect(result).toEqual([]);
    });

    test("getUpsellConfiguration - Returns json array successfull response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getUpsellConfiguration({
            ProductIDs: ["xxxxxxxx"],
            FromDate: "xxx",
            ToDate: "xxx",
            GetForAllChannels: true,
            OnlyChannelAvailableProducts: true,
        });
        expect(result).toEqual(output);
    });

    test("getUpsellConfiguration - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.products.getUpsellConfiguration({
                ProductIDs: ["xxxxxxxx"],
                FromDate: "xxx",
                ToDate: "xxx",
                GetForAllChannels: true,
                OnlyChannelAvailableProducts: true,
            }),
        ).rejects.toThrowError(Error);
    });

    test("addToWaitingList - Returns variants on successfull response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_WAITING_LIST))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.addToWaitingList({
            EventID: "xxxxxxxxxxx",
            ProductID: "xxxxxxxxxxx",
            ProductVariantID: "xxxxxxxxxxx",
            ProfileID: "xxxxxxxxxxx",
            ProfileType: "xxxxxxxxxxx",
            RequestForDate: "xxxxxxxxxxx",
            NoOfTickets: 0,
        });
        expect(result).toEqual(output);
    });

    test("addToWaitingList - Handles bad request error from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) => uri.includes(constants.ENDPOINT_WAITING_LIST))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.products.addToWaitingList({
                EventID: "xxxxxxxxxxx",
                ProductID: "xxxxxxxxxxx",
                ProductVariantID: "xxxxxxxxxxx",
                ProfileID: "xxxxxxxxxxx",
                ProfileType: "xxxxxxxxxxx",
                RequestForDate: "xxxxxxxxxxx",
                NoOfTickets: 0,
            }),
        ).rejects.toThrowError(Error);
    });

    test("getUpSellConfigurationDetails - Returns json array successfull response from dotcy", async () => {
        const output = [];
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) =>
                uri.includes(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG_DETAILS),
            )
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getUpSellConfigurationDetails(["xxxxxxx", "yyyyyyy"]);
        expect(result).toEqual(output);
    });

    test("getUpSellConfigurationDetails - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .post((uri) =>
                uri.includes(constants.ENDPOINT_PRODUCT_GET_UPSELL_PRODUCT_CONFIG_DETAILS),
            )
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.products.getUpSellConfigurationDetails(["xxxxxxx", "yyyyyyy"]),
        ).rejects.toThrowError(Error);
    });

    test("getProductImageUrls - Returns response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_IMAGE_URLS))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getProductImageUrls(["xxxxxxx", "yyyyyyy"]);
        expect(result).toEqual(output);
    });

    test("getProductImageUrls - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_IMAGE_URLS))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.products.getProductImageUrls(["xxxxxxx", "yyyyyyy"]),
        ).rejects.toThrowError(Error);
    });

    test("getproductLiterature - Returns response from dotcy", async () => {
        const output = {};
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_PRODUCT_LITERATURE))
            .reply(httpStatus.OK, output);
        const result = await dotcy.products.getproductLiterature(
            "01f07b60-4882-ec11-a83f-000d3adb3ad7",
        );
        // console.warn(result);
        // expect(result).toBeTruthy();
        expect(result).toEqual(output);
    });

    test("getproductLiterature - Handles bad request response from dotcy", async () => {
        scope
            .post((uri) => uri.includes(constants.ENDPOINT_AUTHORIZE))
            .reply(httpStatus.OK, {})
            .get((uri) => uri.includes(constants.ENDPOINT_PRODUCT_GET_PRODUCT_LITERATURE))
            .reply(httpStatus.BAD_REQUEST);
        await expect(
            dotcy.products.getproductLiterature("01f07b60-4882-ec11-a83f-000d3adb3ad7"),
        ).rejects.toThrowError(Error);
    });
});
